unit Singleton;

{$mode ObjFPC}{$H+}

interface

uses
  sysutils;

type

  { TcnocSingleton }

  generic TSingleton<T: TObject> = class
  private
    class var FIsDestroyed: Boolean;
    class var FInstance: T;
    class function GetInstance: T; static;
  public
    class destructor Destroy;
    class property Instance: T read GetInstance;
  end;


implementation

{ TcnocSingleton }

class destructor TSingleton.Destroy;
begin
  FInstance.Free;
  FIsDestroyed:=True;
end;

class function TSingleton.GetInstance: T;
begin
  if FIsDestroyed then
    Exit(nil);
  if not Assigned(FInstance) then
    FInstance := T.Create();

  Result := FInstance;
end;

end.

