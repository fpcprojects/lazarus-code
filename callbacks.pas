unit Callbacks;

{$mode ObjFPC}{$H+}
{$interfaces corba}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections;

type
  TCallback = procedure(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean) of object;
  TCallbackDefinition = record
    CallerData: PtrInt;
    Callback: TCallback;
  end;

  TCallbackList = specialize TList<TCallbackDefinition>;

  ICallbackDispatcher = interface
    procedure RegisterCallback(ACallback: TCallback; ACallerData: PtrInt);
  end;
  TCallbackDispatcherList = specialize TList<ICallbackDispatcher>;

  ICallbackListener = interface ['{4E26455D-F73A-472D-AF5E-B9D76DA766D9}']
    procedure AttachCallbackDispatcher(ADispatcher: ICallbackDispatcher);
  end;

  { TCallbackDispatcher }

  TCallbackDispatcher = class(ICallbackDispatcher)
  private
    FCallbackList: TCallbackList;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure RegisterCallback(ACallback: TCallback; ACallerData: PtrInt);
    procedure TriggerCallbacks(Sender: TObject);
  end;

  { TCallbackListener }

  TCallbackListener = class(ICallbackListener)
  private
    FCallbackDispatcherList: TCallbackDispatcherList;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure AttachCallbackDispatcher(ADispatcher: ICallbackDispatcher);
  end;


implementation

{ TCallbackDispatcher }

constructor TCallbackDispatcher.Create;
begin
  FCallbackList := TCallbackList.Create;
end;

destructor TCallbackDispatcher.Destroy;
begin
  FCallbackList.Free;
  FCallbackList := nil;
  inherited;
end;

procedure TCallbackDispatcher.RegisterCallback(ACallback: TCallback; ACallerData: PtrInt);
var
  Method: TMethod;
  Listener: ICallbackListener;
  CallbackDefinition: TCallbackDefinition;
begin
  if Assigned(ACallback) then
    begin
    Method := TMethod(ACallback);
    if Supports(TObject(Method.Data), ICallbackListener, Listener) then
      begin
      Listener.AttachCallbackDispatcher(Self);
      end;
    CallbackDefinition.Callback:=ACallback;
    CallbackDefinition.CallerData:=ACallerData;
    FCallbackList.Add(CallbackDefinition);
    end;
end;

procedure TCallbackDispatcher.TriggerCallbacks(Sender: TObject);
var
  i: SizeInt;
  ForgetMe: Boolean;
begin
  for i := FCallbackList.Count-1 downto 0 do
    begin
    FCallbackList[i].Callback(Sender,FCallbackList[i].CallerData, ForgetMe);
    if ForgetMe then
      FCallbackList.Delete(i);
      //FCallbackList.ExtractIndex(i);
    end;
end;

{ TCallbackListener }

constructor TCallbackListener.Create;
begin
  inherited Create;
  FCallbackDispatcherList := TCallbackDispatcherList.Create();
end;

destructor TCallbackListener.Destroy;
begin
  FCallbackDispatcherList.Free;
  inherited Destroy;
end;

procedure TCallbackListener.AttachCallbackDispatcher(ADispatcher: ICallbackDispatcher);
begin
  FCallbackDispatcherList.Add(ADispatcher);
end;

end.

