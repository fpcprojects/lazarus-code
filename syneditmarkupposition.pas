unit SynEditMarkupPosition;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Graphics,
  Controls,
  SynEditMarkup,
  SynEditMiscClasses,
  SynEditPointClasses,
  SynEditTypes;

type

  { TSynEditMarkupPosition }

  TSynEditMarkupPosition = class(TSynEditMarkup)
  private
    FLine: Integer;
    FFrom: Integer;
    FTill: Integer;
    FActiveLine: Boolean;
  public
    constructor Create(ASynEdit : TSynEditBase; ALine, AFrom, ATill: Integer);

    procedure PrepareMarkupForRow(aRow : Integer); override;
    function GetMarkupAttributeAtRowCol(const aRow: Integer;
                                        const aStartCol: TLazSynDisplayTokenBound;
                                        const AnRtlInfo: TLazSynDisplayRtlInfo): TSynSelectedColor; override;
    procedure GetNextMarkupColAfterRowCol(const aRow: Integer;
                                         const aStartCol: TLazSynDisplayTokenBound;
                                         const AnRtlInfo: TLazSynDisplayRtlInfo;
                                         out   ANextPhys, ANextLog: Integer); override;
    function GetMarkupAttributeAtWrapEnd(const aRow: Integer;
      const aWrapCol: TLazSynDisplayTokenBound): TSynSelectedColor; override;
  end;


implementation

{ TSynEditMarkupPosition }

constructor TSynEditMarkupPosition.Create(ASynEdit: TSynEditBase; ALine, AFrom, ATill: Integer);
begin
  inherited Create(ASynEdit);
  FLine:=ALine;
  FFrom:=AFrom;
  FTill:=ATill;
end;

procedure TSynEditMarkupPosition.PrepareMarkupForRow(aRow: Integer);
begin
  FActiveLine := aRow = FLine;
  MarkupInfo.SetFrameBoundsPhys(FFrom, FTill);
end;

function TSynEditMarkupPosition.GetMarkupAttributeAtRowCol(const aRow: Integer;
  const aStartCol: TLazSynDisplayTokenBound;
  const AnRtlInfo: TLazSynDisplayRtlInfo): TSynSelectedColor;
begin
  result := nil;

  if FActiveLine and (FFrom <= aStartCol.Logical) and (FTill > aStartCol.Logical) then
    Result := MarkupInfo;
end;

procedure TSynEditMarkupPosition.GetNextMarkupColAfterRowCol(
  const aRow: Integer; const aStartCol: TLazSynDisplayTokenBound;
  const AnRtlInfo: TLazSynDisplayRtlInfo; out ANextPhys, ANextLog: Integer);
begin
  ANextLog := -1;
  ANextPhys := -1;

  if FFrom > aStartCol.Logical then
    ANextLog := FFrom
  else
  if FTill > aStartCol.Logical then
    ANextLog := FTill;
end;

function TSynEditMarkupPosition.GetMarkupAttributeAtWrapEnd(const aRow: Integer; const aWrapCol: TLazSynDisplayTokenBound): TSynSelectedColor;
begin
  result := nil;

  if FActiveLine and (FFrom <= aWrapCol.Logical) and (FTill > aWrapCol.Logical) then
    Result := MarkupInfo;
end;

end.

