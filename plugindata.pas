unit PluginData;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  generics.Collections;

type

  { TPluginDataDictionary }

  generic TPluginDataDictionary<T: TObject> = class
  private type
    TIntDictionary = specialize TObjectDictionary<string, T>;

  private var
    FDictionary: TIntDictionary;
  public
    constructor Create;
    destructor Destroy; override;
    function TryGetData(AnUniqueId: string; out AValue: T): Boolean;
    // Get plugin-data, if there is no data, create it.
    function GetData(AnUniqueId: string): T;
    procedure Remove(AnUniqueId: string);
    procedure AddOrSetData(AnUniqueId: string; AValue: T);
  end;

implementation

{ TPluginDataDictionary }

constructor TPluginDataDictionary.Create;
begin
  FDictionary := TIntDictionary.Create([doOwnsValues]);
end;

destructor TPluginDataDictionary.Destroy;
begin
  FDictionary.Free;
  inherited Destroy;
end;

function TPluginDataDictionary.TryGetData(AnUniqueId: string; out AValue: T): Boolean;
begin
  Result := FDictionary.TryGetValue(AnUniqueId, AValue);
end;

function TPluginDataDictionary.GetData(AnUniqueId: string): T;
begin
  if not FDictionary.TryGetValue(AnUniqueId, Result) then
    begin
    Result := T.Create;
    FDictionary.Add(AnUniqueId, Result);
    end;
end;

procedure TPluginDataDictionary.Remove(AnUniqueId: string);
begin
  FDictionary.Remove(AnUniqueId);
end;

procedure TPluginDataDictionary.AddOrSetData(AnUniqueId: string; AValue: T);
begin
  FDictionary.AddOrSetValue(AnUniqueId, AValue);
end;

end.

