unit ChangesSyneditPlugin;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  SynEdit,
  LazSynEditText,
  Events,
  OpenFile,
  plugins;

type

  { TChangesSyneditPlugin }

  TChangesSyneditPlugin = class(TLazSynEditPlugin)
  private
    FFileUniqueId: string;
    procedure DoLinesEdited(Sender: TSynEditStrings; aLinePos, aBytePos, aCount, aLineBrkCnt: Integer; aText: String);
  protected
    procedure DoEditorRemoving(AValue: TCustomSynEdit); override;
    procedure DoEditorAdded(AValue: TCustomSynEdit); override;
  public
    constructor Create(AnUniqueFileId: string; AOwner: TComponent);
  end;

  { TChangesPlugin }

  TChangesPlugin = class(TPlugin)
  protected
    procedure HandleNewEditor(AnEvent: TEvent; out Finished: Boolean);
  public
    constructor Create; override;
  end;

  { TLinesEditedData }

  TLinesEditedData = class
  private
    FBytePos: Integer;
    FCount: Integer;
    FLineBrkCnt: Integer;
    FLinePos: Integer;
    FText: string;
  public
    constructor Create(ALinePos, ABytePos, ACount, ALineBrkCnt: Integer; AText: string);
  published
    property LinePos: Integer read FLinePos write FLinePos;
    property BytePos: Integer read FBytePos write FBytePos;
    property Count: Integer read FCount write FCount;
    property LineBrkCnt: Integer read FLineBrkCnt write FLineBrkCnt;
    property Text: string read FText write FText;
  end;

implementation

{ TChangesSyneditPlugin }

procedure TChangesSyneditPlugin.DoLinesEdited(Sender: TSynEditStrings; aLinePos, aBytePos, aCount, aLineBrkCnt: Integer; aText: String);
begin
  TGlobalEventDispatcher.Instance.SendEvent('LinesEdited', FFileUniqueId, TLinesEditedData.Create(aLinePos, aBytePos, aCount, aLineBrkCnt, aText));
end;

procedure TChangesSyneditPlugin.DoEditorRemoving(AValue: TCustomSynEdit);
begin
  inherited DoEditorRemoving(AValue);
  ViewedTextBuffer.RemoveEditHandler(@DoLinesEdited);
end;

procedure TChangesSyneditPlugin.DoEditorAdded(AValue: TCustomSynEdit);
begin
  inherited DoEditorAdded(AValue);
  ViewedTextBuffer.AddEditHandler(@DoLinesEdited);
end;

constructor TChangesSyneditPlugin.Create(AnUniqueFileId: string; AOwner: TComponent);
begin
  FFileUniqueId:=AnUniqueFileId;
  inherited Create(AOwner);
end;

{ TChangesPlugin }

procedure TChangesPlugin.HandleNewEditor(AnEvent: TEvent; out Finished: Boolean);
var
  OpnFile: TOpenFile;
begin
  Finished:=False;
  OpnFile := TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
  if Assigned(OpnFile) then
    begin
    TChangesSyneditPlugin.Create(OpnFile.UniqueId, OpnFile.SynEdit);
    end;
end;

constructor TChangesPlugin.Create;
begin
  inherited Create;
  TGlobalEventDispatcher.Instance.AddEventHandler('NewEditor', @HandleNewEditor);
end;

{ TLinesEditedData }

constructor TLinesEditedData.Create(ALinePos, ABytePos, ACount,ALineBrkCnt: Integer; AText: string);
begin
  FLinePos:=ALinePos;
  FBytePos:=ABytePos;
  FCount:=ACount;
  FLineBrkCnt:=ALineBrkCnt;
  FText:=AText;
end;

initialization
  TGlobalPluginList.Instance.Add(TChangesPlugin.Create);
end.

