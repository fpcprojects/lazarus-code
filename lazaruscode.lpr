program LazarusCode;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  TLoggerUnit,
  teventlogappenderunit,
  cnocQueue,
  Interfaces, // this includes the LCL widgetset
  Forms, anchordockpkg, lazcontrols, MainForm, OpenFile, Actions, Singleton,
  Events, OpenFileAction, Requests, FilesExplorer, FilesFrame, OpenFolder,
  OpenFiles, Shortcuts, OpenFolderAction, SaveFileAction, CloseFileAction,
  CloseAllAction, Callbacks, plugins, sessions, Shell, OpenTerminalAction, Tabs,
  LangServer, LanguageServerAction, LangServerModel, HoverToolTip, annotations,
  SynEditMarkupPosition, changessyneditplugin, languageserver_pylsp,
  LanguageDetectionPlugin, PluginData, completionplugin, hoverjumptoplugin,
  RunTaskAction, ShowLog, ShowLogAction, logging;

{$R *.res}

begin
  TLoggerUnit.initialize();

  TcnocMonitor.DefaultSpinCount:=6;

  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TMainFrm, MainFrm);
  Application.CreateForm(TFileExplorer, FileExplorer);
  Application.CreateForm(TOpenFileForm, OpenFileForm);
  Application.Run;
  TLoggerUnit.TLogger.FreeInstances();
end.

