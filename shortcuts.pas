unit Shortcuts;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Singleton,
  LCLType,
  Generics.Collections,
  Actions;

type
  TShortcutRegistration = record
    ActionName: string;
    Shortcut: TShortCut;
  end;
  TShortcutRegistrationList = specialize TList<TShortcutRegistration>;

  { TShortcuts }

  TShortcuts = class
  private
    FRegistrationList: TShortcutRegistrationList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure RegisterShortcut(AShortcut: TShortCut; AnActionName: string);
    function ShortcutForAction(AnActionName: string): TShortcut;

  end;
  TGlobalShortcuts = specialize TSingleton<TShortcuts>;


implementation

{ TShortcuts }

constructor TShortcuts.Create;
begin
  FRegistrationList := TShortcutRegistrationList.Create;
end;

destructor TShortcuts.Destroy;
begin
  FRegistrationList.Free;
  inherited Destroy;
end;

procedure TShortcuts.RegisterShortcut(AShortcut: TShortCut; AnActionName: string);
var
  Registration: TShortcutRegistration;
begin
  Registration.Shortcut:=AShortcut;
  Registration.ActionName:=AnActionName;
  FRegistrationList.Add(Registration);
end;

function TShortcuts.ShortcutForAction(AnActionName: string): TShortcut;
var
  i: Integer;
begin
  for i := 0 to FRegistrationList.Count -1 do
    if FRegistrationList[i].ActionName=AnActionName then
      begin
      Result := FRegistrationList[i].Shortcut;
      Exit;
      end;
  Result := VK_UNKNOWN;
end;

end.

