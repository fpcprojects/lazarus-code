unit Requests;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Singleton,
  Callbacks,
  Events;

type

  { TRequest }

  TRequest = class(TCustomEvent, ICallbackDispatcher)
  private
    FDispatcher: TCallbackDispatcher;
    FHandled: Boolean;
    FRefCount: Integer;
  protected
    procedure IncRef;
    procedure DecRefAndFree;
    procedure TriggerDoneCallbacks;
    property Dispatcher: TCallbackDispatcher read FDispatcher implements ICallbackDispatcher;
  public
    constructor Create(AName, ASender: string; AData: TObject = nil);
    destructor Destroy; override;
    procedure Handle; virtual;
    procedure SetHandled; virtual;
    property Handled: Boolean read FHandled;
  end;

  { TRequestDispatcher }

  TRequestHandler = procedure (ARequest: TRequest; out Finished: Boolean) of object;

  TRequestDispatcher = class(TCustomEventDispatcher)
  public
    function CreateRequest(AName, ASender: string; AData: TObject = nil; ACallback: TCallback = nil): TRequest;
    procedure SendRequest(AName, ASender: String; AData: TObject = nil; ACallback: TCallback = nil); overload;
    procedure SendRequest(ARequest: TRequest); overload;

    procedure AddRequestHandler(ARequestName: string; ARequestHandler: TRequestHandler);
    procedure RemoveRequestHandler(ARequestName: string; ARequestHandler: TRequestHandler);
  end;
  TGlobalRequestDispatcher = specialize TSingleton<TRequestDispatcher>;

implementation

uses
  OpenFile,
  Actions,
  plugins,
  MainForm;

{ TRequest }

procedure TRequest.IncRef;
begin
  Inc(FRefCount);
end;

procedure TRequest.DecRefAndFree;
begin
  Dec(FRefCount);
  if FRefCount < 0 then
    begin
    TriggerDoneCallbacks;
    Free;
    end;
end;

procedure TRequest.TriggerDoneCallbacks;
begin
  FDispatcher.TriggerCallbacks(Self);
end;

constructor TRequest.Create(AName, ASender: string; AData: TObject);
begin
  FDispatcher := TCallbackDispatcher.Create;
  inherited Create(AName, ASender, AData);
end;

destructor TRequest.Destroy;
begin
  FDispatcher.Free;
  FDispatcher := nil;
  inherited Destroy;
end;

procedure TRequest.Handle;
begin
  Inc(FRefCount)
end;

procedure TRequest.SetHandled;
begin
  FHandled:=true;
  DecRefAndFree;
end;

{ TRequestDispatcher }

function TRequestDispatcher.CreateRequest(AName, ASender: string; AData: TObject; ACallback: TCallback = nil): TRequest;
begin
  Result := TRequest.Create(AName, ASender, AData);
  (Result as ICallbackDispatcher).RegisterCallback(ACallback, 0);
end;

procedure TRequestDispatcher.SendRequest(AName, ASender: String; AData: TObject; ACallback: TCallback = nil);
begin
  SendRequest(CreateRequest(AName, ASender, AData, ACallback));
end;

procedure TRequestDispatcher.SendRequest(ARequest: TRequest);
var
  GlobalFileList: TOpenFileList;
  IsFinished: Boolean;
  i: Integer;
  Handler: TCustomEventCaller;
begin
  IsFinished:=False;
  try
    if FHandlerDictionary.TryGetValue(ARequest.Name, Handler) then
      begin
      Handler.CallEvent(ARequest, IsFinished);
      end;

    if not IsFinished then
      begin
      GlobalFileList := TGlobalFileList.Instance;
      for i := 0 to GlobalFileList.Count -1 do
        begin
        GlobalFileList.Items[i].HandleRequest(ARequest, IsFinished);
        if IsFinished then
          Break;
        end;
      end;

    if not IsFinished then
      begin
      for i := 0 to TGlobalPluginList.Instance.Count -1 do
        begin
        TGlobalPluginList.Instance.Items[i].HandleRequest(ARequest, IsFinished);
        if IsFinished then
          Break;
        end;
      end;

    if not IsFinished then
      MainFrm.HandleRequest(ARequest, IsFinished);
  finally
    ARequest.DecRefAndFree;
  end;
end;

procedure TRequestDispatcher.AddRequestHandler(ARequestName: string; ARequestHandler: TRequestHandler);
begin
  inherited AddHandler(ARequestName, TCustomEventHandler(ARequestHandler));
end;

procedure TRequestDispatcher.RemoveRequestHandler(ARequestName: string; ARequestHandler: TRequestHandler);
begin
  // In some cases the dispatcher is already freed (order of class-destructors is
  // unpredictable)
  if Assigned(self) then
    inherited RemoveHandler(ARequestName, TCustomEventHandler(ARequestHandler));
end;

end.

