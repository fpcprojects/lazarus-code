unit OpenFile;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  SynEdit,
  Singleton,
  Requests,
  Events,
  Generics.Collections;

type

  { TOpenFile }

  TOpenFile = class
  private
    FFileName: string;
    FSynEdit: TSynEdit;
    FUniqueId: string;
    FUniqueInt: Integer;
    FModified: Boolean;
    FBLockUpdates: Boolean;
    FVersion: Integer;
    FLanguageId: string;
    function GetCaption: string;
    function GetIsShallow: Boolean;
    function GetLanguageId: string;
    procedure SetLanguageId(AValue: string);
    procedure SetSynEdit(AValue: TSynEdit);
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromFile(AFileName: string);
    procedure SaveToFile(AFileName: string = '');
    procedure HandleRequest(ARequest: TRequest; out Finished: Boolean);
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean);
    procedure SetModified;
    property UniqueId: string read FUniqueId;
    property Filename: string read FFileName;
    property LanguageId: string read GetLanguageId write SetLanguageId;
    property Caption: string read GetCaption;
    property UniqueInt: Integer read FUniqueInt write FUniqueInt;
    property Modified: Boolean read FModified;
    property SynEdit: TSynEdit read FSynEdit write SetSynEdit;
    property IsShallow: Boolean read GetIsShallow;
    property Version: Integer read FVersion write FVersion;
  end;
  TCustOpenFileList = specialize TObjectList<TOpenFile>;

  { TOpenFileList }

  TOpenFileList = class(TCustOpenFileList)
  protected
    FFocusedOpenFile: TOpenFile;
    FShallowFile: TOpenFile;
  public
    function NewFile: TOpenFile;
    function FindByUniqueId(AnUniqueId: string): TOpenFile;
    function FindByUniqueInt(AnUniqueInt: Integer): TOpenFile;
    function FindByFilename(AFilename: string): TOpenFile;
    function ObtainShallowFile: TOpenFile;
    procedure ResetShallow;
    function GetFocusedFile: TOpenFile;
    procedure SetFocusedFile(AnUniqueId: string);
  end;
  TGlobalFileList = specialize TSingleton<TOpenFileList>;

  { TLanguageIdChangedData }

  TLanguageIdChangedData = class
  private
    FNewLanguageId: string;
    FOldLanguageId: string;
  public
    constructor Create(AnOldLanguageId, ANewLanguageId: string);
  published
    property OldLanguageId: string read FOldLanguageId write FOldLanguageId;
    property NewLanguageId: string read FNewLanguageId write FNewLanguageId;
  end;

implementation

{ TOpenFile }

function TOpenFile.GetCaption: string;
begin
  if FileName='' then
    Result := 'New file'
  else if Length(Filename)>25 then
    Result := ExtractFileName(FileName)
  else
    Result := FileName;
end;

function TOpenFile.GetIsShallow: Boolean;
begin
  Result := TGlobalFileList.Instance.FShallowFile = Self;
end;

function TOpenFile.GetLanguageId: string;
begin
  Result := FLanguageId;
end;

procedure TOpenFile.SetLanguageId(AValue: string);
begin
  if AValue = FLanguageId then
    Exit;
  TGlobalEventDispatcher.Instance.SendEvent('LanguageIdChanged', UniqueId, TLanguageIdChangedData.Create(FLanguageId, AValue));
  FLanguageId := AValue;
end;

procedure TOpenFile.SetSynEdit(AValue: TSynEdit);
begin
  if FSynEdit=AValue then Exit;
  FSynEdit:=AValue;
end;

constructor TOpenFile.Create;
begin
  FUniqueInt:=Random(9999999);
  FUniqueId:='file'+IntToStr(FUniqueInt);
end;

destructor TOpenFile.Destroy;
begin
  inherited Destroy;
end;

procedure TOpenFile.LoadFromFile(AFileName: string);
begin
  FFileName:=AFileName;
  FBLockUpdates := True;
  try
    SynEdit.Lines.LoadFromFile(AFileName);
  finally
    FBLockUpdates := False;
  end;
  TEvent.Create('LoadedFile', UniqueId).Send;
end;

procedure TOpenFile.SaveToFile(AFileName: string);
begin
  if AFileName='' then
    AFileName:=FFileName;
  SynEdit.Lines.SaveToFile(AFileName);
end;

procedure TOpenFile.HandleRequest(ARequest: TRequest; out Finished: Boolean);
begin
  Finished:=False;
end;

procedure TOpenFile.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
begin
  Finished:=False;
end;

procedure TOpenFile.SetModified;
begin
  if not FBLockUpdates then
    begin
    FModified:=True;
    if IsShallow then
      TGlobalFileList.Instance.ResetShallow;
    end;
end;

{ TOpenFileList }

function TOpenFileList.NewFile: TOpenFile;
begin
  Result := TOpenFile.Create;
  Add(Result);
  TEvent.Create('NewFile', Result.UniqueId).Send;
end;

function TOpenFileList.FindByUniqueId(AnUniqueId: string): TOpenFile;
var
  i: Integer;
begin
  for i := 0 to Count-1 do
    if Items[i].UniqueId=AnUniqueId then
      Exit(Items[i]);
  Result := nil;
end;

function TOpenFileList.FindByUniqueInt(AnUniqueInt: Integer): TOpenFile;
var
  i: Integer;
begin
  for i := 0 to Count-1 do
    if Items[i].UniqueInt=AnUniqueInt then
      Exit(Items[i]);
  Result := nil;
end;

function TOpenFileList.FindByFilename(AFilename: string): TOpenFile;
var
  i: Integer;
begin
  for i := 0 to Count-1 do
    if SameFileName(Items[i].Filename, AFilename) then
      Exit(Items[i]);
  Result := nil;
end;

function TOpenFileList.ObtainShallowFile: TOpenFile;
begin
  if not Assigned(FShallowFile) then
    FShallowFile:=NewFile;
  Result := FShallowFile;
end;

procedure TOpenFileList.ResetShallow;
var
  SFile: TOpenFile;
begin
  SFile := FShallowFile;
  FShallowFile:=nil;
  if Assigned(SFile) then
    TGlobalEventDispatcher.Instance.SendEvent('Unshallow', SFile.UniqueId);
end;

function TOpenFileList.GetFocusedFile: TOpenFile;
begin
  Result := FFocusedOpenFile;
end;

procedure TOpenFileList.SetFocusedFile(AnUniqueId: string);
begin
  FFocusedOpenFile := FindByUniqueId(AnUniqueId);
  TGlobalEventDispatcher.Instance.SendEvent('ReceivedFocus', AnUniqueId);
end;

{ TLanguageIdChangedData }

constructor TLanguageIdChangedData.Create(AnOldLanguageId, ANewLanguageId: string);
begin
  FOldLanguageId := AnOldLanguageId;
  FNewLanguageId := ANewLanguageId;
end;

end.

