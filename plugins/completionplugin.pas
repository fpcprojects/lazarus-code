unit CompletionPlugin;

{$mode ObjFPC}{$H+}

interface

uses
  // rtl
  Classes,
  SysUtils,
  Generics.Collections,
  // Lazutils
  LazUTF8,
  // LCL
  LCLType,
  // SynEdit
  SynEdit,
  SynCompletion,
  // LazarusCode
  Callbacks,
  plugins,
  PluginData,
  Events,
  OpenFile,
  Requests;

type

  { TCompletionDataResponse }

  TCompletionDataResponse = class
  private
    FResponseLabel: string;
  public
    procedure Assign(ASource: TCompletionDataResponse);
  published
    property ResponseLabel: string read FResponseLabel write FResponseLabel;
  end;
  TCustCompletionDataResponseList = specialize TObjectList<TCompletionDataResponse>;

  { TCompletionDataResponseList }

  TCompletionDataResponseList = class(TCustCompletionDataResponseList)
  public
    procedure Assign(ASource: TCompletionDataResponseList);
  end;

  { TCompletionPluginData }

  TCompletionPluginData = class
  private
    FSynCompletion: TSynCompletion;
    FSynEdit: TSynEdit;
    FUniqueFileId: string;
    FCompletionDataResponseList: TCompletionDataResponseList;

    procedure DoSearchPosition(var APosition: integer);
    procedure HandleCodeCompletionResponse(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
  public
    constructor Create;
    destructor Destroy; override;
    procedure DoBeforeExecute(ASender: TSynBaseCompletion; var ACurrentString: String; var APosition: Integer; var AnX, AnY: Integer; var AnResult: TOnBeforeExeucteFlags);
    procedure DoExecute(Sender: TObject);
    procedure DoCodeCompletion(var Value: string; SourceValue: string; var SourceStart, SourceEnd: TPoint; KeyChar: TUTF8Char; Shift: TShiftState);
    procedure DoKeyNextChar(Sender: TObject);
    procedure DoKeyPrevChar(Sender: TObject);
    procedure UpdateDropdown;
    property UniqueFileId: string read FUniqueFileId write FUniqueFileId;
    property SynEdit: TSynEdit read FSynEdit write FSynEdit;
    property SynCompletion: TSynCompletion read FSynCompletion write FSynCompletion;
  end;

  TCompletionPluginDataDictionary = specialize TPluginDataDictionary<TCompletionPluginData>;

  { TCompletionPlugin }

  TCompletionPlugin = class(TPlugin)
  private
    FPluginData: TCompletionPluginDataDictionary;
    //procedure DoCodeCompletion(var Value: string; SourceValue: string; var SourceStart, SourceEnd: TPoint; KeyChar: TUTF8Char; Shift: TShiftState);
    //procedure DoSearchPosition(var APosition: integer);
    procedure HandleNewEditor(AnEvent: TEvent; out Finished: Boolean);
  protected

  public
    constructor Create; override;
    destructor Destroy; override;
  end;

  { TCompletionData }

  TCompletionData = class
  private
    FResponse: TCompletionDataResponseList;
    FSourceStart: TPoint;
    FUniqueFileId: string;
  public
    constructor Create(AnUniqueFileId: string; ASourceStart: TPoint);
    destructor Destroy; override;
    property Response: TCompletionDataResponseList read FResponse;
    property SourceStart: TPoint read FSourceStart;
  published
    property UniqueFileId: string read FUniqueFileId;
  end;

{ TCompletionPlugin }

type
  //TCompletionRequest = specialize TResponseRequest<TObject, Boolean>;
  TCompletionRequest = TRequest;

implementation

{ TCompletionData }

constructor TCompletionData.Create(AnUniqueFileId: string; ASourceStart: TPoint);
begin
  FSourceStart:=ASourceStart;
  FUniqueFileId:=AnUniqueFileId;
  FResponse := TCompletionDataResponseList.Create;
end;

destructor TCompletionData.Destroy;
begin
  FResponse.Free;
  inherited Destroy;
end;

{ TCompletionDataResponse }

procedure TCompletionDataResponse.Assign(ASource: TCompletionDataResponse);
begin
  FResponseLabel := ASource.ResponseLabel;
end;

{ TCompletionDataResponseList }

procedure TCompletionDataResponseList.Assign(ASource: TCompletionDataResponseList);
var
  Item: TCompletionDataResponse;
  i: Integer;
begin
  Clear;
  for i := 0 to ASource.Count -1 do
    begin
    Item := TCompletionDataResponse.Create;
    Item.Assign(ASource.Items[i]);
    Add(Item);
    end;
end;

//procedure OnCompletionRequestDone(AParam: Boolean);
//begin
//
//end;

procedure TCompletionPluginData.DoCodeCompletion(
  var Value: string;
  SourceValue: string;
  var SourceStart, SourceEnd: TPoint;
  KeyChar: TUTF8Char;
  Shift: TShiftState);
begin
end;

procedure TCompletionPluginData.UpdateDropdown;
var
  i: Integer;
  Item: TCompletionDataResponse;
  CurrStr: String;
begin
  SynCompletion.ItemList.Clear;
  for i := 0 to FCompletionDataResponseList.Count -1 do
    begin
    Item := FCompletionDataResponseList[i];
    CurrStr := SynCompletion.CurrentString;
    if (CurrStr='') or (Item.ResponseLabel.StartsWith(CurrStr)) then
      SynCompletion.ItemList.Add(Item.ResponseLabel);
    end;
end;

procedure TCompletionPluginData.DoSearchPosition(var APosition: integer);
begin
  // ToDo: This is also called before an OnExecute. In that case this function-call
  // is useless...
  UpdateDropdown;
end;

procedure TCompletionPluginData.HandleCodeCompletionResponse(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
var
  Req: TRequest;
  Data: TCompletionData;
begin
  Req := Sender as TRequest;
  Data := Req.Data as TCompletionData;

  FCompletionDataResponseList.Assign(Data.Response);

  UpdateDropdown;
end;

constructor TCompletionPluginData.Create;
begin
  FCompletionDataResponseList := TCompletionDataResponseList.Create;
end;

destructor TCompletionPluginData.Destroy;
begin
  FCompletionDataResponseList.Free;
  inherited Destroy;
end;

procedure TCompletionPluginData.DoBeforeExecute(ASender: TSynBaseCompletion;
  var ACurrentString: String; var APosition: Integer; var AnX, AnY: Integer;
  var AnResult: TOnBeforeExeucteFlags);
begin
end;

procedure TCompletionPluginData.DoExecute(Sender: TObject);
var
  Req: TRequest;
begin
  assert(sender=SynCompletion);

  Req := TRequest.Create('Completion', 'ComplPlugin', TCompletionData.Create(FUniqueFileId, FSynEdit.CaretXY));
  (Req as ICallbackDispatcher).RegisterCallback(@HandleCodeCompletionResponse, PtrInt( @SynCompletion));
  TGlobalRequestDispatcher.Instance.SendRequest(Req);

  SynCompletion.ItemList.Clear;
  SynCompletion.ItemList.Add('loading...');

  FCompletionDataResponseList.Clear;
end;

procedure TCompletionPlugin.HandleNewEditor(AnEvent: TEvent; out Finished: Boolean);
var
  OpnFile: TOpenFile;
  AutoCompl: TSynAutoComplete;
  CompletionPluginData: TCompletionPluginData;
  SynCompletion: TSynCompletion;
begin
  Finished:=False;
  OpnFile := TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
  if Assigned(OpnFile) then
    begin
    CompletionPluginData := FPluginData.GetData(AnEvent.Sender);
    CompletionPluginData.UniqueFileId:=AnEvent.Sender;
    CompletionPluginData.SynEdit:=OpnFile.SynEdit;

    SynCompletion := TSynCompletion.Create(nil);
    SynCompletion.ShortCut:=KeyToShortCut(VK_SPACE, [ssCtrl]);
    SynCompletion.OnSearchPosition:=@CompletionPluginData.DoSearchPosition;
    SynCompletion.OnCodeCompletion:=@CompletionPluginData.DoCodeCompletion;
    //SynCompletion.OnBeforeExecute:=@CompletionPluginData.DoBeforeExecute;
    SynCompletion.OnExecute:=@CompletionPluginData.DoExecute;
    SynCompletion.OnKeyNextChar:=@CompletionPluginData.DoKeyNextChar;
    SynCompletion.OnKeyPrevChar:=@CompletionPluginData.DoKeyPrevChar;
    SynCompletion.ExecCommandID:=ecSynAutoCompletionExecute;
    SynCompletion.Editor := OpnFile.SynEdit;
    SynCompletion.AutoUseSingleIdent := False;

    CompletionPluginData.SynCompletion:=SynCompletion;

    //AutoCompl := TSynAutoComplete.Create(OpnFile.SynEdit);
    //AutoCompl.AddEditor(OpnFile.SynEdit);
    //AutoCompl.AutoCompleteList.Values['Anno'] := 'hoi';
    //AutoCompl.ShortCut:=KeyToShortCut(VK_SPACE, [ssCtrl]);
    //AutoCompl.ExecCommandID:=ecSynAutoCompletionExecute;
    //AutoCompl.AutoCompleteList.Values['Anno'] := 'hoi';
    //AutoCompl.AutoCompleteList.Add('anno=fsdf');
    ////AutoCompl.AutoCompleteList.Add('anno');
    end;
end;

procedure TCompletionPluginData.DoKeyNextChar(Sender: TObject);
var
  NewPrefix: String;
  Line: String;
  LogCaret: TPoint;
  CharLen: LongInt;
  AddPrefix: String;
begin
  // Copied from the Lazarus-ide sourceeditor.pas
  LogCaret:=SynEdit.LogicalCaretXY;
  if LogCaret.Y>SynEdit.Lines.Count then exit; //* LogCaret.Y One Based
  Line:=SynEdit.Lines[LogCaret.Y-1];
  if LogCaret.X>length(Line) then exit;
  CharLen:=UTF8CodepointSize(@Line[LogCaret.X]);
  AddPrefix:=copy(Line,LogCaret.X,CharLen);
  if not SynEdit.IsIdentChar(AddPrefix) then exit;
  NewPrefix:=SynCompletion.CurrentString+AddPrefix;
  inc(LogCaret.X);
  SynEdit.LogicalCaretXY:=LogCaret;
  SynCompletion.CurrentString:=NewPrefix;

  UpdateDropdown;
end;

procedure TCompletionPluginData.DoKeyPrevChar(Sender: TObject);
var
  NewPrefix: String;
  NewLen: LongInt;
begin
  // Copied from the Lazarus-ide sourceeditor.pas
  NewPrefix:=SynCompletion.CurrentString;
  if NewPrefix='' then exit;
  SynEdit.CaretX:=SynEdit.CaretX-1;
  NewLen:=UTF8FindNearestCharStart(PChar(NewPrefix),length(NewPrefix),
                                   length(NewPrefix)-1);
  NewPrefix:=copy(NewPrefix,1,NewLen);
  SynCompletion.CurrentString:=NewPrefix;

  UpdateDropdown;
end;

constructor TCompletionPlugin.Create;
begin
  inherited Create;
  FPluginData := TCompletionPluginDataDictionary.Create;
  TGlobalEventDispatcher.Instance.AddEventHandler('NewEditor', @HandleNewEditor);
end;

destructor TCompletionPlugin.Destroy;
begin
  TGlobalEventDispatcher.Instance.RemoveEventHandler('NewEditor', @HandleNewEditor);
  FPluginData.Free;
  inherited Destroy;
end;

initialization
  TGlobalPluginList.Instance.Add(TCompletionPlugin.Create);
end.


