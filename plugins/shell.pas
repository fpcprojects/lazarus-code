unit Shell;

{$mode ObjFPC}{$H+}

interface

uses
  // FPC
  // RTL
  Classes,
  SysUtils,
  unix,
  process,
  Pipes,
  StrUtils,
  BaseUnix,
  Termio,
  Math,
  Contnrs,
  syncobjs,
  // fcl-web
  fpTemplate,
  // LCL
  Forms,
  ComCtrls,
  LMessages,
  LCLType,
  Dialogs,
  LCLIntf,
  Graphics,
  Controls,
  SynEdit,
  SynEditTypes,
  SynEditKeyCmds,
  SynEditPointClasses,
  SynEditMarkupSelection,
  SynEditMarkup,
  SynEditMiscClasses,
  SynEditHighlighter,
  // log4fpc
  TLoggerUnit,
  // cnocThreadingTools
  cnocqueue,
  // LazarusCode
  tabs,
  Logging,
  plugins,
  Requests,
  OpenFolder,
  PluginData,
  Events;

const
  WM_SHELL_OUTPUT = LM_USER + 2014;
  WM_SHELL_INPUT_INITIALIZED = LM_USER + 2015;
  WM_SHELL_FINISHED = LM_USER + 2017;
//  WM_SHELL_STDERR_OUTPUT = LM_USER + 2016;

type
  TCharacterEditingMode = (hemFollowing, hemPreceding);
  TApplicationKeyMode = (ckmCursor, ckmApplication);

type
  TOutputBuffer = record
    Buf: PByte;
    Length: Word;
  end;

  TOutputQueue = specialize TcnocThreadedQueue<TOutputBuffer>;
  TShellSynedit = class;

  { TShellThread }

  TShellThread = class(TThread)
  strict private
    FProcess     : TProcess;
    FOutputStream: TInputPipeStream;
  private
    FSynEdit: TShellSynEdit;
    FSynEditHandle: HWND;
    FOutputQueue: TOutputQueue;
    FExecutable: TProcessString;
    FParameters: TProcessStrings;
    FCwd: string;
    procedure ForkEvent(Sender: TObject);
  protected
    procedure Execute; override;
    procedure TerminatedSet; override;
  public
    constructor Create(ASynEdit: TShellSynEdit; ASynEditHandle: HWND; AnOutputQueue: TOutputQueue; AnExecutable: TProcessString; AParameters: TProcessStrings; ACwd: string);
    destructor Destroy; override;
  end;

  { TShellSynEdit }

  TShellOutputState = (sosText, sosEscape, sosEscapeSecCharacter, sosCSI, sosOSC);
  TShellSynEdit = class(TSynEdit)
  private
    FUniqueId: string;
    // In/output handle of the pseudoterminal
    FMasterPty: HWND;
    // Input stream, bound to the FMasterPty handle
    FInputStream: TOutputPipeStream;

    // Information to parse incoming data from the pseudoterminal, stored
    // within the class so that it can survives escape sequences split between
    // two buffered reads...
    FOutputState: TShellOutputState;
    FOutpuTerminalCache: string;
    FPriorMark: SizeInt;

    // Used to set graphic effects (colors, underline etc).
    // This is done by creating a selection and apply a markup on these
    // selections.
    // These fields store the current selection and corresponding markup.
    FCurrentCursorSelection: TSynEditSelection;
    FCurrentMarkup: TSynEditMarkupSelection;
    FUpFront: Boolean;

    FCursorSelectionList: TObjectList;

    // Some fields to handle certain graphic effects
    FNegative: Boolean;
    FConcealed: Boolean;
    FForeground: TColor;
    FBackground: TColor;
    FFontStyles: TFontStyles;
    FFrameEdges: TSynFrameEdges;
    FFaint: Boolean;

    // Shell application has been terminated
    FShellFinished: Boolean;

    // Terminal (vt200, ecma) modes
    FCharacterEditingMode: TCharacterEditingMode;
    FBracketedPasteMode: Boolean;
    FCursorKeysApplicationMode: TApplicationKeyMode;
    FKeypadApplicationMode: TApplicationKeyMode;

    // To avoid that every incoming character is logged, the they are
    // kept in a cache.
    FLogCache: string;
    FLogInstance: TLogger;

    // The actual position of the caret (that is: where the running process
    // thinks the caret is currently at) This could be different from the
    // SynEdit caret, when the caret has been set to another location using the
    // mouse, or due to a selection being made.
    FTerminalCaret: TPoint;
    procedure FlushLog();
    procedure MarkupInvalidateLines(FirstLine, LastLine: integer);
  protected
    FOutputQueue: TOutputQueue;
    // Used for logging terminal-data
    function FormatRawDataAsString(AData: string): string;
    procedure LogReceivedData(AData: string; DataType: TShellOutputState);
    procedure LogSendData(AData: string);
    procedure LogSynSelectionChange(ASelection: TSynEditSelection; Msg: string);

    // Map Lazarus syn-edit commands to the proper characters that have to be
    // send to the pseudoterminal.
    function MapKeys(ACommand: TSynEditorCommand; CursorApplicationMode, KeypadApplicationMode: TApplicationKeyMode): string;

    // Prepare SGR effects at the current caret-location
    procedure ApplySGR(Force: Boolean);
    procedure ApplySGRAfterCursorJump;
    function GetSynSelection(ARow, ACol: Integer): TSynSelectedColorMergeResult;
    procedure PrepareSGRMode;

    procedure ConsoleSizeChanged(AMasterPtyFd: HWND);
    procedure UTF8KeyPress(var Key: TUTF8Char); override;
    procedure DoOnProcessCommand(var Command: TSynEditorCommand; var AChar: TUTF8Char; Data: pointer); override;

    procedure ProcessCSIEscapeSequences(T: string);
    procedure ProcessOSCEscapeSequences(T: string);
    procedure ProcessEscControlSequence(T: string);
    procedure ParseTerminalOutput(T: string);
    procedure HandleShellOutput(Data: PtrInt);
    procedure HandleShellInputInitialized(var Msg: TLMessage); message WM_SHELL_INPUT_INITIALIZED;
    procedure HandleShellFinished(var Msg: TLMessage); message WM_SHELL_FINISHED;
    procedure DoOnResize; override;
  public
    constructor Create(AOwner: TComponent; AnUniqueId: string);
    destructor Destroy; override;
  end;

  { TShellPluginData }

  TShellPluginData = class
  private
    FCloseAfterProcessEnd: Boolean;
    FPage: TTabSheet;
    FSynEdit: TShellSynEdit;
    FThread: TShellThread;
  public
    property Page: TTabSheet read FPage write FPage;
    property SynEdit: TShellSynEdit read FSynEdit write FSynEdit;
    property Thread: TShellThread read FThread write FThread;
    property CloseAfterProcessEnd: Boolean read FCloseAfterProcessEnd write FCloseAfterProcessEnd;
  end;

  TShellPluginDataDictionary = specialize TPluginDataDictionary<TShellPluginData>;

  { TShellPlugin }

  TShellPlugin = class(TPlugin)
  private
    FDataDictionary: TShellPluginDataDictionary;
    procedure HandleCloseTabRequestedEvent(AnEvent: TEvent; out Finished: Boolean);
    procedure HandleRunProcessRequest(ARequest: TRequest; out Finished: Boolean);
    procedure HandleShellFinishedEvent(AnEvent: TEvent; out Finished: Boolean);
    procedure InitializeSynEdit(PluginData: TShellPluginData; AnUniqueId: string);
    function ParseTemplate(ATemplate: string): string;
  protected
    procedure RunApp(PluginData: TShellPluginData; AnExecutable: TProcessString; AParameters: TProcessStrings; ACwd: string);
  public
    constructor Create();
    destructor Destroy; override;
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean); override;
  end;

  { TRunProcessData }

  TRunProcessData = class
  private
    FCwd: string;
    FExecutable: TProcessString;
    FParameters: TProcessStrings;
  public
    constructor Create(AnExcutable: TProcessString);
    destructor Destroy; override;
    property Executable: TProcessString read FExecutable;
    property Parameters: TProcessStrings read FParameters;
    property Cwd: string read FCwd write FCwd;
  end;

implementation

function login_tty(__fd:longint):longint;cdecl;external 'c' name 'login_tty';
function openpty(__amaster:Plongint; __aslave:Plongint; __name:Pchar; __termp:pointer{Ptermios}; __winp:pointer{Pwinsize}):longint;cdecl;external 'util' name 'openpty';

{ TShellThread }

procedure StatusReportHandler(Sender: TThread; AData: Pointer;
  const status: String);
begin

end;

var
  GSlavePTyFd: cint;
  GQuickLock: Cardinal;

procedure TShellThread.ForkEvent(Sender: TObject);
begin
  if login_tty(GSlavePTyFd) <> 0 then
    writeln('Failed to login to tty. Errcode: '+inttostr(fpgeterrno)+' - '+inttostr(GSlavePTyFd));
end;

procedure TShellThread.Execute;
const
  BUF_SIZE = 4096; // Buffer size for reading the output in chunks

var
  BytesRead    : longint;
  Buffer       : TOutputBuffer;
  AMasterPtyFd: cint;
  InputStream:TOutputPipeStream;
  ExitStatus, ExtCode: Integer;

begin
  FProcess := TProcess.Create(nil);

  FProcess.Executable := FExecutable;
  FProcess.Parameters.Assign(FParameters);
  FParameters.Free;

  // Process option poUsePipes has to be used so the output can be captured.
  // Process option poWaitOnExit can not be used because that would block
  // this program, preventing it from reading the output data of the process.
  AMasterPtyFd:=-1;
  GSlavePTyFd:=-1;
  if openpty(@AMasterPtyFd, @GSlavePTyFd, nil, nil, nil) <> 0 then
    raise Exception.Create('Failed to open pseudo-tty. Errcode: '+inttostr(fpgeterrno));
  FProcess.OnForkEvent:=@ForkEvent;
  FProcess.CurrentDirectory:=FCwd;
  //FProcess.Options := [poUsePipes];

  FProcess.Execute;

  FOutputStream:=TInputPipeStream.Create(AMasterPtyFd);
  InputStream:=TOutputPipeStream.Create(AMasterPtyFd);
  //StderrStream:=TInputPipeStream.Create(AMasterPtyFd+2);
  //FOutputStream:=FProcess.Output;
  //InputStream:=FProcess.Input;
  //StderrStream:=FProcess.Stderr;

  PostMessage(FSynEditHandle, WM_SHELL_INPUT_INITIALIZED, PtrInt(InputStream), AMasterPtyFd);

  //  FInputStream:=TOutputPipeStream.Create (InHandle);
  //FOutputStream:=TInputPipeStream.Create (OutHandle);
  //if Not (poStderrToOutput in FProcessOptions) then
  //  FStderrStream:=TInputPipeStream.Create(ErrHandle);


  // All generated output from FProcess is read in a loop until no more data is available
  repeat
    // Get the new data from the process to a maximum of the buffer size that was allocated.
    // Note that all read(...) calls will block except for the last one, which returns 0 (zero).
    Buffer.Buf := GetMem(BUF_SIZE);
    BytesRead := FOutputStream.Read(Buffer.Buf[0], BUF_SIZE);


    if BytesRead>0 then
      begin
      Buffer.Length:=BytesRead;
      FOutputQueue.PushItem(Buffer);
      if InterlockedExchange(GQuickLock, 1) = 0 then
        Application.QueueAsyncCall(@FSynEdit.HandleShellOutput, 0);
      end
    else
      Freemem(Buffer.Buf);
  until (BytesRead = 0) or not FProcess.Running;  // Stop if no more data is available
  //FProcess.WaitOnExit();
  //            werkt niet, omdat de pty altijd blijft bestaan....
  // The process has finished so it can be cleaned up
  ExtCode := FProcess.ExitCode;
  ExitStatus := FProcess.ExitStatus;
  //FreeAndNil(FProcess);
  //InputStream.Free;
  PostMessage(FSynEditHandle, WM_SHELL_FINISHED, ExitCode, ExitStatus)
end;

procedure TShellSynEdit.ConsoleSizeChanged(AMasterPtyFd: HWND);
var
  WinSize: TWinSize;
begin
  if integer(AMasterPtyFd) >= 0 then
    begin
    with WinSize do
      begin
      // Calculated guess, assume monospace:
      ws_xpixel := CharsInWindow*(Font.GetTextWidth('WW') div 2);
      ws_ypixel := LinesInWindow*Font.GetTextHeight('W');
      // The -2 is actually wrong, but it is a hack to avoid the screen to
      // scroll horizontally when the cursor is placed to the most right position
      // in the linux-command 'top'.
      ws_col := CharsInWindow-2;
      ws_row := LinesInWindow;
      end;

(* Note that when the Linux kernel (or appropriate driver etc.) gets TIOCSWINSZ *)
(* it takes it upon itself to raise a SIGWINCH, I've not tested whether other   *)
(* unix implementations do the same. Because this is an implicit action, and    *)
(* because by and large the process receiving the signal can identify the       *)
(* sender and would be entitled to be unhappy if the sender appeared to vary,   *)
(* I've not attempted to defer signal sending in cases where the process being  *)
(* debugged is in a paused state or is otherwise suspected to not be able to    *)
(* handle it immediately. MarkMLl (so you know who to kick).                    *)

    if fpioctl(AMasterPtyFd, TIOCSWINSZ, @WinSize) < 0 then
      begin
      //fileclose(ttyHandle);
      //DebugLn(DBG_WARNINGS, ['TPseudoConsoleDlg.AddOutput Write failed, closed handle']);
      //ttyHandle := System.THandle(-1)    (* Attempted ioctl() failed          *)
      end
    end
end;

constructor TShellThread.Create(
  ASynEdit: TShellSynEdit;
  ASynEditHandle: HWND;
  AnOutputQueue: TOutputQueue;
  AnExecutable: TProcessString;
  AParameters: TProcessStrings;
  ACwd: string);
begin
  FSynEdit:=ASynEdit;
  FSynEditHandle:=ASynEditHandle;
  FOutputQueue:=AnOutputQueue;
  FExecutable:=AnExecutable;
  FParameters:=TStringList.Create;
  FParameters.Assign(AParameters);
  FCwd:=ACwd;
  inherited Create(False);
end;

destructor TShellThread.Destroy;
begin
  FreeAndNil(FProcess);
  FOutputStream.Free;
  inherited Destroy;
end;

procedure TShellThread.TerminatedSet;
begin
  if Assigned(FProcess) then
    begin
    FProcess.Terminate(1);
    // By closing this handle the blocking read-call in the thread is closed so
    // that the thread can terminate itself.
    FileClose(GSlavePTyFd);
    end;
end;

{ TShellSynEdit }

procedure TShellSynEdit.MarkupInvalidateLines(FirstLine, LastLine: integer);
begin
  //
end;

function TShellSynEdit.FormatRawDataAsString(AData: string): string;
var
  InEscape: Boolean;
  i: Integer;
begin
  Result:='';
  for i := 1 to Length(AData) do
    begin
    if (ord(AData[i])<$20) or (AData[i]='''') then
      begin
      if InEscape then
        Result:=Result+'''';
      InEscape:=false;
      Result:=Result+'#$'+HexStr(Ord(AData[i]), 2);
      end
    else
      begin
      if not InEscape then
        Result:=Result+'''';
      Result:=Result+AData[i];
      InEscape:=true;
      end;
    end;
  if InEscape then
    Result:=Result+'''';
end;

procedure TShellSynEdit.FlushLog();
begin
  if FLogCache<>'' then
    FLogInstance.Trace('Received data              < '+FormatRawDataAsString(FLogCache));
  FLogCache:='';
end;

procedure TShellSynEdit.LogReceivedData(AData: string; DataType: TShellOutputState);

begin
  if (DataType in [sosOSC, sosCSI]) and (FLogCache<>'') then
    FlushLog;

  case DataType of
    sosOSC:
      FLogInstance.Trace('Received esc sequence [OSC]< '+AData);
    sosCSI:
      FLogInstance.Trace('Received esc sequence [CSI]< '+AData);
    sosText:
      begin
      FLogCache:=FLogCache+AData;
      if AData = #$10 then
        FlushLog;
      end;
    sosEscape, sosEscapeSecCharacter:
      FLogInstance.Trace('Received esc control       < '+AData);
  end;
end;

procedure TShellSynEdit.LogSendData(AData: string);
begin
  // To keep the log consistent, log any pending received data
  FlushLog();
  FLogInstance.Trace('Send (raw) data            > '+FormatRawDataAsString(AData));
end;

procedure TShellSynEdit.LogSynSelectionChange(ASelection: TSynEditSelection; Msg: string);
var
  s: string;
  Ind: Integer;
begin
  Ind := FCursorSelectionList.IndexOf(ASelection);

  s := Format('[%d] %s ([%d,%d]x[%d,%d])', [Ind, Msg, ASelection.StartLinePos, ASelection.StartBytePos, ASelection.EndLinePos, ASelection.EndBytePos]);
  FLogInstance.Info(s);
end;

procedure TShellSynEdit.UTF8KeyPress(var Key: TUTF8Char);
begin
  if FShellFinished then
    begin      // kan nog niet omdat key nog verder afgehanderld word
    Key := #0;
    Parent.Free;
    Exit;
    end;

  // The inputstream is only available after the shell-application has been
  // started.
  if Assigned(FInputStream) then
    begin
    LogSendData(Key);
    FInputStream.WriteByte(Ord(Key[1]));
    Key:='';
    inherited UTF8KeyPress(Key);
    end
  else
    begin
    // ToDo: cache the key, so it can be send after the shell-application has
    // started?
    end;
end;

function TShellSynEdit.MapKeys(ACommand: TSynEditorCommand; CursorApplicationMode, KeypadApplicationMode: TApplicationKeyMode): string;
begin
  case ACommand of
    ecUp:
      if CursorApplicationMode=ckmApplication then
        Result := #$1B'OA'
      else
        Result := #$1B'[A';
    ecDown:
      if CursorApplicationMode=ckmApplication then
        Result := #$1B'OB'
      else
        Result := #$1B'[B';
    ecRight:
      if CursorApplicationMode=ckmApplication then
        Result := #$1B'OC'
      else
        Result := #$1B'[C';
    ecLeft:
      if CursorApplicationMode=ckmApplication then
        Result := #$1B'OD'
      else
        Result := #$1B'[D';
    ecTab:
      if CursorApplicationMode=ckmApplication then
        Result := #$1B'OI'
      else
        Result := #$9;
    ecLineStart:
      Result := #$1B'[1~';
    ecInsertMode:
      Result := #$1B'[2~';
    ecDeleteCharNoCrLf:
      Result := #$1B'[3~';
    ecLineEnd:
      Result := #$1B'[4~';
    ecPageUp:
      Result := #$1B'[5~';
    ecPageDown:
      Result := #$1B'[6~';
    else
      begin
      FlushLog;
      FLogInstance.Warn('Requested a command for which the escape-sequence is not known.');
      Result := '';
      end;
  end;
end;

procedure TShellSynEdit.ApplySGR(Force: Boolean);

  function Lighter(MyColor:TColor; Percent:Byte):TColor;
  var r,g,b:Byte;
  begin
    MyColor:=ColorToRGB(MyColor);
    r:=GetRValue(MyColor);
    g:=GetGValue(MyColor);
    b:=GetBValue(MyColor);
    r:=r+muldiv(255-r,Percent,100); //Percent% closer to white
    g:=g+muldiv(255-g,Percent,100);
    b:=b+muldiv(255-b,Percent,100);
    result:=RGB(r,g,b);
  end;

var
  RtlInfo: TLazSynDisplayRtlInfo;
  NextPhys, NextLog, i, CursId: Integer;
  StartCol: TLazSynDisplayTokenBound;
  SelectionMarkup : TSynEditMarkup;
begin
  //if not force then
  //  begin
  //  StartCol.Logical:=-1;//CaretX-1;
  //  StartCol.Physical:=-1;//CaretX-1;
  //  StartCol.Offset:=0;
  //
  //  RtlInfo.IsRtl:=False;
  //  RtlInfo.LogFirst:=1;
  //  RtlInfo.LogLast:=255;
  //  RtlInfo.PhysLeft:=1;
  //  RtlInfo.PhysRight:=255;
  //
  //  //for i := 0 to MarkupCount -1 do
  //  //  if Markup[i].
  //  //
  //  //
  //  //SelectionMarkup := MarkupManager.MarkupByClass[TSynEditMarkupSelection];
  //  //SelectionMarkup.PrepareMarkupForRow(CaretY);
  //  //{MarkupManager}SelectionMarkup.GetNextMarkupColAfterRowCol(CaretY, StartCol, RtlInfo, NextPhys, NextLog);
  //  //if NextLog > -1 then
  //  //  Force:=True;
  //  MarkupManager.BeginMarkup;
  //  MarkupManager.PrepareMarkupForRow(CaretY);
  //  MarkupManager.GetNextMarkupColAfterRowCol(CaretY, StartCol, RtlInfo, NextPhys, NextLog);
  //  MarkupManager.FinishMarkupForRow(CaretY);
  //  MarkupManager.EndMarkup;
  //  if NextLog > -1 then
  //    Force:=True;
  //
  //  end;
  // Only create a new selection to apply the SGR onto, when there is already
  // a selection (or force is true). After all, there is no need to create a
  // selection when the defaults are used.
  // Force is true when a new (non default) SGR is applied
  if Assigned(FCurrentCursorSelection) or Force then
    begin
    // ToDo: mem-leak
    FCurrentCursorSelection := TSynEditSelection.Create(ViewedTextBuffer, False);
    FCursorSelectionList.Add(FCurrentCursorSelection);
    // Has to be assigned, or else it will cause an AV cause it will be called,
    // even if not assigned.
    FCurrentCursorSelection.InvalidateLinesMethod:=@MarkupInvalidateLines;
    FCurrentMarkup := TSynEditMarkupSelection.Create(Self, FCurrentCursorSelection);
    MarkupManager.AddMarkUp(FCurrentMarkup);
    FCurrentCursorSelection.StartLineBytePos:=CaretXY;
    FCurrentCursorSelection.EndLineBytePos:=CaretXY;

    LogSynSelectionChange(FCurrentCursorSelection, 'New SynEditSelection created');

    if FNegative then
      begin
      FCurrentMarkup.MarkupInfo.Foreground:=FBackground;
      FCurrentMarkup.MarkupInfo.Background:=FForeground;
      end
    else
      begin
      FCurrentMarkup.MarkupInfo.Foreground:=FForeground;
      FCurrentMarkup.MarkupInfo.Background:=FBackground;
      end;
    if FConcealed then
      FCurrentMarkup.MarkupInfo.Foreground:=FCurrentMarkup.MarkupInfo.Background
    else if FFaint then
      FCurrentMarkup.MarkupInfo.Foreground:=Lighter(FCurrentMarkup.MarkupInfo.Foreground, 50);
    FCurrentMarkup.MarkupInfo.FrameEdges:=FFrameEdges;
    end;
end;

procedure TShellSynEdit.ApplySGRAfterCursorJump;
var
  RtlInfo: TLazSynDisplayRtlInfo;
  NextPhys, NextLog, i: Integer;
  StartCol: TLazSynDisplayTokenBound;
  SelectionMarkup : TSynEditMarkup;
begin
  exit;
  StartCol.Logical:=0;//CaretX-1;
  StartCol.Physical:=0;//CaretX-1;
  StartCol.Offset:=0;

  RtlInfo.IsRtl:=False;
  RtlInfo.LogFirst:=1;
  RtlInfo.LogLast:=255;
  RtlInfo.PhysLeft:=1;
  RtlInfo.PhysRight:=255;

  //for i := 0 to MarkupCount -1 do
  //  if Markup[i].
  //
  //
  //SelectionMarkup := MarkupManager.MarkupByClass[TSynEditMarkupSelection];
  //SelectionMarkup.PrepareMarkupForRow(CaretY);
  //{MarkupManager}SelectionMarkup.GetNextMarkupColAfterRowCol(CaretY, StartCol, RtlInfo, NextPhys, NextLog);
  //if NextLog > -1 then
  //  Force:=True;
  MarkupManager.BeginMarkup;
  MarkupManager.PrepareMarkupForRow(CaretY);


  NextLog := -1;
  NextPhys := -1;
  //if fMarkUpList.Count = 0
  //then exit;
  //TSynEditMarkup(fMarkUpList[0]).GetNextMarkupColAfterRowCol(aRow, aStartCol, AnRtlInfo, ANextPhys, ANextLog);
  for i := 1 to MarkupCount-1 do
    if Markup[i] is TSynEditMarkupSelection then
      Markup[i].GetNextMarkupColAfterRowCol(CaretY, StartCol, RtlInfo, NextPhys, NextLog);



  //MarkupManager.GetNextMarkupColAfterRowCol(CaretY, StartCol, RtlInfo, NextPhys, NextLog);
  MarkupManager.FinishMarkupForRow(CaretY);
  MarkupManager.EndMarkup;

  //if (NextLog > 0) or (NextPhys>-1) then
  //  ApplySGR(true)
  //else
  //  NextPhys:=5;
end;

function TShellSynEdit.GetSynSelection(ARow, ACol: Integer): TSynSelectedColorMergeResult;
var
  RtlInfo: TLazSynDisplayRtlInfo;
  NextPhys, NextLog, i: Integer;
  StartCol: TLazSynDisplayTokenBound;
  SelectionMarkup : TSynEditMarkup;
  BaseColors: TSynHighlighterAttributesModifier;
begin
  StartCol.Logical:=ACol;
  StartCol.Physical:=ACol;
  StartCol.Offset:=0;

  RtlInfo.IsRtl:=False;
  RtlInfo.LogFirst:=1;
  RtlInfo.LogLast:=255;
  RtlInfo.PhysLeft:=1;
  RtlInfo.PhysRight:=255;

  MarkupManager.BeginMarkup;
  MarkupManager.PrepareMarkupForRow(ARow);
  NextLog := -1;
  NextPhys := -1;

  Result := TSynSelectedColorMergeResult.Create();
  Result.InitMergeInfo;


  BaseColors := TSynHighlighterAttributesModifier.Create;
  try
    BaseColors.Foreground:=Font.Color;
    BaseColors.Background:=Color;
    Result.Merge(BaseColors);
  finally
    BaseColors.Free;
  end;

  for i := 1 to MarkupCount-1 do
    if Markup[i] is TSynEditMarkupSelection then
      Markup[i].MergeMarkupAttributeAtRowCol(ARow, StartCol, StartCol, RtlInfo, Result);
  Result.ProcessMergeInfo;
  MarkupManager.FinishMarkupForRow(CaretY);
  MarkupManager.EndMarkup;
end;

procedure TShellSynEdit.PrepareSGRMode;
var
  LocalMarkup: TSynSelectedColorMergeResult;
begin
  LocalMarkup := GetSynSelection(CaretY, CaretX);
  try
    if LocalMarkup.Foreground<>FForeground then
      begin
      FLogInstance.Info(Format('Foreground does not match at [%d,%d]',[CaretY, CaretX]));
      ApplySGR(True);
      end;
  finally
    LocalMarkup.Free;
  end;
end;

procedure TShellSynEdit.DoOnProcessCommand(var Command: TSynEditorCommand; var AChar: TUTF8Char; Data: pointer);
var
  Buffer: string;
begin
  case Command of
    ecLineBreak:
      begin
      LogSendData(#$A);
      FInputStream.WriteByte($A);
      Command:=ecNone;
      end;
    ecUp, ecDown, ecRight, ecLeft, ecTab, ecLineStart, ecLineEnd, ecDeleteCharNoCrLf, ecPageUp, ecPageDown, ecInsertMode:
      begin
      Buffer := MapKeys(Command, FCursorKeysApplicationMode, FKeypadApplicationMode);
      LogSendData(Buffer);
      FInputStream.WriteData(PChar(Buffer), Length(Buffer));
      Command:=ecNone;
      end;
    //ecGotoXY:
    //  begin
    //  // It is not possible to set the cursor to another place
    //  Command:=ecNone;
    //  end
  else
    inherited DoOnProcessCommand(Command, AChar, Data);
  end;
end;

procedure TShellSynEdit.ProcessCSIEscapeSequences(T: string);

var
  ControlFunctionStr: string;
  ParamStr: string;
  Params: TStringArray;

  // Erase in page
  procedure Ecma48ControlFunctionED();
  var
    i: Integer;
    p: TPoint;
  begin
    case ParamStr of
      '1': // Erase above, todo: untested
        begin
        // Set cursor to highest visible line
        SetCaretY(TopView-1);
        for i := 0 to CaretY - TopView do
          ExecuteCommand(ecDeleteLine, #0, nil);
        // Insert a new line (the new, empty, current line)
        ExecuteCommand(ecInsertLine, #0, nil);
        end;
      '2': // Erase all
        begin
        // ToDo: for some reason, when the window is not completely scrolled down,
        // one line is removed too many.

        // Set cursor to highest visible line
        SetCaretY(TopView-1);
        // Remove current line and all lines below
        for i := 0 to LinesInWindow do
          ExecuteCommand(ecDeleteLine, #0, nil);
        // Insert a new line (the new, empty, current line)
        ExecuteCommand(ecInsertLine, #0, nil);
        end;
      '3': // Erase saved lines (xterm addition)
        begin
        // Store current caret position, relative to view
        p := TextXYToScreenXY(CaretXY);
        SetBlockBegin(TPoint.Create(1,1));
        SetBlockEnd(TPoint.Create(1,TopView));
        SelText:='';
        // Restore caret position
        CaretXY:=p;
        end
    else // Erase below, todo: untested
      begin
      // Remove current line and all lines below
      for i := 0 to Lines.Count - CaretY do
        ExecuteCommand(ecDeleteLine, #0, nil);
      // Insert a new line (the new, empty, current line)
      ExecuteCommand(ecInsertLine, #0, nil);
      end
    end;
  end;

  // EL
  procedure Ecma48ControlFunctionEraseInLine();
  begin
    case ParamStr of
      '1': begin
           FLogInstance.Debug('Erase Line, from start of line till cursor');
           ExecuteCommand(ecDeleteBOL, #0, nil);
           end;
      '2': begin
           FLogInstance.Debug('Erase Line, complete line');
           ExecuteCommand(ecDeleteLine, #0, nil);
           end
    else
      begin
      FLogInstance.Debug('Erase Line, from cursor till end of line');
      ExecuteCommand(ecDeleteEOL, #0, nil);
      end;
    end;
  end;

  // SM
  procedure Ecma48ControlFunctionSetMode(SetMode: Boolean);
  var
    i: Integer;
    OrdValue: Integer;
    BoolValue: Boolean;
  begin
    if SetMode then
      begin
      OrdValue := 1;
      BoolValue:=True;
      end
    else
      begin
      OrdValue := 0;
      BoolValue:=False;
      end;

    for i := 0 to High(Params) do
      begin
      case Params[i] of
        '10':
          FCharacterEditingMode:=TCharacterEditingMode(OrdValue);
        '?1':
          FCursorKeysApplicationMode:=TApplicationKeyMode(OrdValue);
        '?2004':
          FBracketedPasteMode:=BoolValue;
        '?25': // Show cursor (DECTCEM), VT220.
          if BoolValue then
            Options:=Options-[eoNoCaret]
          else
            Options:=Options+[eoNoCaret]
      else
        begin
        FlushLog;
        FLogInstance.Warn('Received request to set unsupported mode ['+Params[i]+']');
        end;
      end;
      end;
  end;

  // Delete character
  procedure Ecma48ControlFunctionDCH;
  var
    n, i: Integer;
  begin
    n := StrToIntDef(ParamStr, 1);
    for i := 0 to n-1 do
      begin
      case FCharacterEditingMode of
        hemFollowing: ExecuteCommand(ecDeleteCharNoCrLf, #0, nil);
        hemPreceding: ExecuteCommand(ecDeleteLastChar, #0, nil);
      end;

      end;
  end;

  // CUP
  procedure Ecma48ControlFunctionCursorPosition;
  var
    P: TPoint;
  begin
    if Length(Params) < 2 then
      P := TPoint.Create(1,1)
    else
      P := TPoint.Create(StrToIntDef(Params[1], 1), StrToIntDef(Params[0], 1));
    P := ScreenXYToTextXY(P, True);
    ExecuteCommand(ecGotoXY, #0, @P);
    //ApplySGRAfterCursorJump;
    FCurrentCursorSelection:=nil;
    FUpFront:=False;
  end;

  // CUF
  procedure Ecma48ControlFunctionCursorRight;
  var
    n: LongInt;
    i: Integer;
  begin
    n := StrToIntDef(ParamStr, 1);
    for i := 0 to n-1 do
      ExecuteCommand(ecRight, #0, nil);
  end;

  // Select graphic rendition
  procedure Ecma48ControlFunctionSGR;
  var
    Clear, NewMarkupCreated: Boolean;
    i: Integer;
    NewCursorSelection: TSynEditSelection;
    NewMarkup: TSynEditMarkupSelection;
  begin
    NewMarkupCreated:=False;
    for i := 0 to High(Params) do
      begin
      //// When the parameter starts with 0, clear any earlier markups...
      //if (Length(Params[i]) > 1) and (Params[i][1]='0') then
      //  begin
      //  Clear := True;
      //  NewMarkupCreated:=False;
      //  Delete(Params[i],1, 1);
      //  end;

      // SGR should be cleared in case of an empty param, the param being 0. But
      // there is also the case where '0'X is send, then first do a clear (the 0)
      // and then handle the X.
      if (Params[i]='0') or (Params[i]='') or ((Length(Params[i]) > 1) and (Params[i][1]='0')) then
        begin
        if ((Length(Params[i]) > 1) and (Params[i][1]='0')) then
          Delete(Params[i],1, 1);

        // ToDo: mem-leak!
        FCurrentMarkup:=nil;
        FCurrentCursorSelection:=nil;
        FUpFront:=False;
        FNegative:=False;
        FFaint:=False;
        FConcealed:=False;
        FFontStyles:=Font.Style;
        FFrameEdges:=sfeNone;
        FForeground:=Font.Color;
        FBackground:=Color;
        Clear:=true;
        end;

      if not ((Params[i]='0') or (Params[i]='')) then
        begin
        //if not NewMarkupCreated then
        //  begin
        //  NewCursorSelection := TSynEditSelection.Create(ViewedTextBuffer, False);
        //  // Has to be assigned, or else it will cause an AV cause it will be called,
        //  // even if not assigned.
        //  NewCursorSelection.InvalidateLinesMethod:=@MarkupInvalidateLines;
        //  NewMarkup := TSynEditMarkupSelection.Create(Self, NewCursorSelection);
        //  MarkupManager.AddMarkUp(NewMarkup);
        //  NewCursorSelection.StartLineBytePos:=CaretXY;
        //  NewCursorSelection.EndLineBytePos:=CaretXY;
        //  NewMarkup.MarkupInfo.FrameEdges:=sfeNone;
        //
        //  if Assigned(FCurrentMarkup) and not Clear then
        //    begin
        //    // Copy the values of the current markup
        //    NewMarkup.MarkupInfo.Style:=FCurrentMarkup.MarkupInfo.Style;
        //    end
        //  else
        //    begin
        //    //NewMarkup.MarkupInfo.Background:=Color;
        //    //NewMarkup.MarkupInfo.Foreground:=Font.Color;
        //    // Use default colors as base (There was no markup, so the
        //    // default colors were still active)
        //    FNegative:=False;
        //    FFaint:=False;
        //    FConcealed:=False;
        //    FForeground:=Font.Color;
        //    FBackground:=Color;
        //    end;
        //
        //  FCurrentCursorSelection:=NewCursorSelection;
        //  FCurrentMarkup:=NewMarkup;
        //  NewMarkupCreated := True;
        //  end;
        Clear:=False;

        case Params[i] of
          '1': // bold or increased intensity
            FFontStyles:=FFontStyles+[fsBold];
          '2': // faint, decreased intensity or second colour
            FFaint:=True;
          '3':  // italicized
            FFontStyles:=FFontStyles+[fsItalic];
          '4': // single underlined
            begin
            FFontStyles:=FFontStyles+[fsUnderline];
            FFrameEdges:=sfeNone;
            end;
          '5': // slowly blinking (less then 150 per minute)
            begin
            end;
          '6': // rapidly blinking (150 per minute or more)
            begin
            end;
          '7': // negative image
            FNegative:=True;
          '8': // concealed characters
            FConcealed:=True;
          '9': // crossed-out (characters still legible but marked as to be deleted)
            FFontStyles:=FFontStyles+[fsStrikeOut];
          '20': // doubly underlined
            begin
            FFontStyles:=FFontStyles+[fsUnderline];
            FFrameEdges:=sfeBottom;
            end;
          '22': // normal colour or normal intensity (neither bold nor faint)
            begin
            FFontStyles:=FFontStyles-[fsBold];
            FFaint:=False;
            end;
          '23': // not italicized, not fraktur
            FFontStyles:=FFontStyles-[fsItalic];
          '24': // not underlined (neither singly nor doubly)
            begin
            FFontStyles:=FFontStyles-[fsUnderline];
            FFrameEdges:=sfeNone;
            end;
          '25': // steady (not blinking)
            begin
            end;
          '27': // positive image
            FNegative:=False;
          '30': // black display
            FForeground:=clBlack;
          '31': // red display
            FForeground:=clRed;
          '32': // green display
            FForeground:=clGreen;
          '33': // yellow display
            FForeground:=clYellow;
          '34': // blue display
            FForeground:=clBlue;
          '35': // magenta display
            FForeground:=clFuchsia;
          '36': // cyan display
            FForeground:=clAqua;
          '37': // white display
            FForeground:=clWhite;
          '39': // default display colour (implementation-defined)
            FForeground:=Font.Color;
          '49': // default background colour (implementation-defined)
            FBackground:=Color;
        else
          begin
          FlushLog;
          FLogInstance.Warn('Received unsupported SGR-flag ['+Params[i]+']');
          end;
        end;
      end;
      end;
    //ApplySGR(not Clear);
  end;

begin
  LogReceivedData(T, sosCSI);

  ControlFunctionStr := RightStr(T, 1);
  ParamStr := LeftStr(T, Length(T) -1);
  Params := ParamStr.Split(';');

  case ControlFunctionStr of
    'h': Ecma48ControlFunctionSetMode(True);
    'l': Ecma48ControlFunctionSetMode(False);
    'm': Ecma48ControlFunctionSGR;
    'C': Ecma48ControlFunctionCursorRight;
    'H': Ecma48ControlFunctionCursorPosition;
    'J': Ecma48ControlFunctionED;
    'K': Ecma48ControlFunctionEraseInLine;
    'P': Ecma48ControlFunctionDCH;
  else
    begin
    FlushLog;
    FLogInstance.Warn('Received unsupported CSI-function ['+ControlFunctionStr+']');
    end;
  end;
end;

procedure TShellSynEdit.ProcessOSCEscapeSequences(T: string);
begin
  LogReceivedData(T, sosOSC);
end;

procedure TShellSynEdit.ProcessEscControlSequence(T: string);
begin
  LogReceivedData(T, FOutputState);
  FLogInstance.Warn('Received unsupported escape control sequence ['+T+']');
end;

procedure TShellSynEdit.ParseTerminalOutput(T: string);
var
  p1, p2, StrCount: SizeInt;
  Part, Str: string;
begin
  BeginUpdate();
  try
    T := FOutpuTerminalCache + T;
    p1 := Length(FOutpuTerminalCache) +1;
    while (p1 > 0) and (p1 <= Length(T))  do
      begin
      case FOutputState of
        sosText:
          begin
          case T[p1] of
            #$07:
              begin
              LogReceivedData(T[p1], FOutputState);
              FLogInstance.Debug('Beep');
              Beep;
              end;
            #$08:
              begin
              LogReceivedData(T[p1], FOutputState);
              FLogInstance.Debug('Backspace, move cursor one position to the left');
              // In principle this should be a backspace. But bash seems to expect
              // a left here?
              ExecuteCommand(ecLeft, #0, nil);
              end;
            #$1B:
              begin
              FPriorMark:=p1+1;
              FOutputState:=sosEscape;
              end;
            #$0A:
              begin
              LogReceivedData(T[p1], FOutputState);
              FLogInstance.Debug('Line Feed, move cursor one position down');
              //Lines.Insert(CaretXY.Y,'');
              if CaretY = Lines.Count then
                begin
                FUpFront:=True;
                Lines.Add('');
                end;
              ExecuteCommand(ecDown, #0, nil);
              //ApplySGRAfterCursorJump;
              FCurrentCursorSelection:=nil;
              end;
            #$0D:
              begin
              LogReceivedData(T[p1], FOutputState);
              FLogInstance.Debug('Carriage return, move cursor to most left position');
              ExecuteCommand(ecLineStart, #0, nil);
              FCurrentCursorSelection:=nil;
              FUpFront:=False;
              //ApplySGRAfterCursorJump;
              end;
            #$09:
              begin
              LogReceivedData(T[p1], FOutputState);
              if not Assigned(FCurrentCursorSelection) and not FUpFront then
                PrepareSGRMode();
              ExecuteCommand(ecChar, T[p1], nil);
              if Assigned(FCurrentCursorSelection) then
                begin
                FCurrentCursorSelection.EndLineBytePos:=CaretXY;
                LogSynSelectionChange(FCurrentCursorSelection, 'SynEditSelection extended');
                end;
              end
            else
              begin
              StrCount := PosSetEx([#$07, #$08, #$09, #$1B, #$0A, #$0D], T, p1);
              if StrCount=0 then
                StrCount:=Length(T)+1;
              StrCount := StrCount - (p1 -1);
              Str := Copy(T, p1, StrCount-1);

              LogReceivedData(Str, FOutputState);
              if not Assigned(FCurrentCursorSelection) and not FUpFront then
                PrepareSGRMode();

              TextView.EditReplace(CaretX, CaretY, Length(str),(*Temp +*) Str);
              CaretX := CaretX + length(Str);

              if Assigned(FCurrentCursorSelection) then
                begin
                FCurrentCursorSelection.EndLineBytePos:=CaretXY;
                LogSynSelectionChange(FCurrentCursorSelection, 'SynEditSelection extended');
                end;
              inc(p1, StrCount-2);
              end
          end;
          inc(p1);
          end;
        sosEscape:
          begin
          case T[p1] of
            '[': // CSI (ESC [), ends with a byte in the range 0x40 through 0x7E
              begin
              FOutputState:=sosCSI;
              Inc(FPriorMark);
              end;
            ']': // OSC (ESC ]), ends with ST (ESC \)
              begin
              FOutputState:=sosOSC;
              Inc(FPriorMark);
              end;
            '=': // Enter alternate keypad mode
              begin
              FLogInstance.Debug('Enter alternate keypad mode');
              FKeypadApplicationMode:=ckmApplication;
              LogReceivedData(T[p1], FOutputState);
              FOutputState:=sosText;
              end;
            '>': // Exit alternate keypad mode
              begin
              FLogInstance.Debug('Exit alternate keypad mode');
              FKeypadApplicationMode:=ckmCursor;
              LogReceivedData(T[p1], FOutputState);
              FOutputState:=sosText;
              end;
            'M': // Reverse line feed
              begin
              FLogInstance.Debug('Reverse line feed. Move cursor one position upwards');
              if CaretY = TopView then
                begin
                // This is probably incorrect, it should probably only insert a
                // line at the top when the cursor is positioned at the top.
                // But the 'less' command puts the terminal in alternative
                // screen buffer mode (1049) which is not supported (yet).
                // Scrolling on top of the view makes it possible to scroll up.
                // So that's why this is done.
                ExecuteCommand(ecScrollUp, #0, nil);
                end;
              ExecuteCommand(ecUp, #0, nil);
              LogReceivedData(T[p1], FOutputState);

              // Is this correct?
              FCurrentCursorSelection:=nil;

              FOutputState:=sosText;
              end;
            '(': // Designate G0 Character Set
              begin
              FOutputState:=sosEscapeSecCharacter;
              end;
            else
              begin
              FlushLog;
              FLogInstance.Warn('Received unsupported escape-character ['+T[p1]+']');
              FOutputState:=sosText;
              end;
          end;
          inc(p1);
          end;
        sosEscapeSecCharacter:
          begin
          Inc(p1);
          Part:=Copy(T, FPriorMark, p1 - FPriorMark);
          ProcessEscControlSequence(Part);
          FOutputState:=sosText;
          end;
        sosCSI:
          begin
          if T[p1] in [#$40..#$7e] then
            begin
            Part:=Copy(T, FPriorMark, p1 - FPriorMark +1);
            ProcessCSIEscapeSequences(Part);
            FOutputState:=sosText;
            end;
          inc(p1);
          end;
        sosOSC:
          begin
          p2 := PosEx(#$1B+'\', T, p1);
          if p2 > 0 then
            begin
            p1 := p2;
            Part:=Copy(T, FPriorMark, p1 - FPriorMark);
            ProcessOSCEscapeSequences(Part);
            FOutputState:=sosText;
            Inc(p1, 2);
            end
          else
            // Wait for the next buffered output
            break;
          end
      end;
      end;

    if FOutputState<>sosText then
      begin
      FOutpuTerminalCache:=T;
      end
    else
      begin
      FOutpuTerminalCache:='';
      FPriorMark:=1;
      end;
  finally
    EndUpdate;
  end;
end;

// This function is called through QueueAsyncCall. In an earlier attempt a message
// was used. But LCL-messages takes precedence over GTK-messages. So that GTK
// messages never came through. With a freeze as a result then processing
// large quantities of data.
procedure TShellSynEdit.HandleShellOutput(Data: PtrInt);
var
  Buffer: TOutputBuffer;
  B: TBytes;
  T: string;
  i: integer;
  HadData: Boolean;
begin
  HadData:=False;

  i := 0;
  if FOutputQueue.PopItem(Buffer) = wrSignaled then
    begin
    HadData:=True;
    // Wherever the caret may have been placed (mouse events or whatever)
    // Replace it where the terminal thinks it is.
    CaretXY := FTerminalCaret;

    repeat
    SetLength(B, Buffer.Length);
    Move(Buffer.Buf^, B[0], Buffer.Length);
    Freemem(Buffer.Buf);
    T := TEncoding.UTF8.GetAnsiString(B, 0, Buffer.Length);

    ParseTerminalOutput(T);

    FTerminalCaret := CaretXY;

    FlushLog;
    inc(i);
    if i > 5 then
      begin
      // Stop processing the input for now, and schedule this function again to
      // continue the process. This to give the UI (widgetset and LCL-events)
      // the ability to process some events. So that the application doesn't
      // 'freeze'. (The 5 is completely arbitary)
      Application.QueueAsyncCall(@HandleShellOutput, 0);
      exit;
      end;
    until FOutputQueue.PopItem(Buffer) <> wrSignaled;
    end;
  InterlockedExchange(GQuickLock, 0);
  if HadData then
    // Between PopItem and InterlockedExchange a new item could have been pushed.
    // So we have to call HandleShellOutput once more. Even, though in most cases
    // there will be no messages pending in FOutputQueue.
    Application.QueueAsyncCall(@HandleShellOutput, 0);
end;

procedure TShellSynEdit.HandleShellInputInitialized(var Msg: TLMessage);
begin
  FInputStream:=TObject(Msg.WParam) as TOutputPipeStream;
  FMasterPty:=Msg.LParam;
  ConsoleSizeChanged(FMasterPty)
end;

procedure TShellSynEdit.HandleShellFinished(var Msg: TLMessage);
begin
  Options := Options + [eoNoCaret];
  Options2 := Options2 - [eoAlwaysVisibleCaret];
  ReadOnly:=True;
  FShellFinished := True;
  TGlobalEventDispatcher.Instance.SendEvent('ShellFinished', FUniqueId);
end;

procedure TShellSynEdit.DoOnResize;
begin
  inherited DoOnResize;
  // In theory the pty could be 0, but that would be the stdin handle of this
  // process itself... That shouldn't happen.
  if FMasterPty>0 then
    ConsoleSizeChanged(FMasterPty);
end;

constructor TShellSynEdit.Create(AOwner: TComponent; AnUniqueId: string);
begin
  inherited Create(AOwner);
  FUniqueId:=AnUniqueId;
  FOutputQueue:=TOutputQueue.create(10, INFINITE, 0);
  FLogInstance := TGlobalLogFactory.Instance.InitializeLogger('pseudoterminal');
  FTerminalCaret:=TPoint.Create(1,1);
  FPriorMark:=1;
  FCursorSelectionList:=TObjectList.Create;
  FForeground:=Font.Color;
end;

destructor TShellSynEdit.Destroy;
begin
  FOutputQueue.Free;
  FCursorSelectionList.Free;
  inherited Destroy;
end;

{ TShellPlugin }

procedure TShellPlugin.RunApp(PluginData: TShellPluginData; AnExecutable: TProcessString; AParameters: TProcessStrings; ACwd: string);
begin
  PluginData.Thread := TShellThread.Create(PluginData.SynEdit, PluginData.FSynEdit.Handle, PluginData.SynEdit.FOutputQueue, AnExecutable, AParameters, ACwd);
end;

constructor TShellPlugin.Create();
begin
  FDataDictionary := TShellPluginDataDictionary.Create;
  TGlobalRequestDispatcher.Instance.AddRequestHandler('RunProcess', @HandleRunProcessRequest);
  TGlobalEventDispatcher.Instance.AddEventHandler('ShellFinished', @HandleShellFinishedEvent);
  TGlobalEventDispatcher.Instance.AddEventHandler('CloseTabRequested', @HandleCloseTabRequestedEvent);

end;

destructor TShellPlugin.Destroy;
begin
  FDataDictionary.Free;
  inherited Destroy;
end;

procedure TShellPlugin.InitializeSynEdit(PluginData: TShellPluginData; AnUniqueId: string);

var
  SynEdit: TShellSynEdit;

  procedure AddKey(ACommand: TSynEditorCommand; AShortCut: TShortCut);
  begin
    with SynEdit.Keystrokes.Add do
      begin
      Command:=ACommand;
      ShortCut:=AShortCut;
      end;
  end;

begin
  SynEdit:=TShellSynEdit.Create(PluginData.Page, AnUniqueId);
  SynEdit.Align:=alClient;
  SynEdit.Gutter.Parts.Clear;
  SynEdit.Options := SynEdit.Options - [eoAutoIndent, eoNoCaret] + [eoScrollPastEof]; // + [eoPersistentCaret]; //+ [eoAlwaysVisibleCaret];
  SynEdit.Options2 := SynEdit.Options2 + [eoAlwaysVisibleCaret];
  SynEdit.OverwriteCaret:=ctHorizontalLine;
  SynEdit.InsertMode:=false;
  SynEdit.MaxUndo:=0;

  SynEdit.Keystrokes.Clear;
  AddKey(ecUp, VK_UP);
  AddKey(ecDown, VK_DOWN);
  AddKey(ecLeft, VK_LEFT);
  AddKey(ecRight, VK_RIGHT);
  AddKey(ecTab, VK_TAB);
  AddKey(ecLineStart, VK_HOME);
  AddKey(ecLineEnd, VK_END);
  AddKey(ecDeleteCharNoCrLf, VK_DELETE);
  AddKey(ecInsertMode, VK_INSERT);
  AddKey(ecPageUp, VK_PRIOR);
  AddKey(ecPageDown, VK_NEXT);

  PluginData.SynEdit:=SynEdit;
  PluginData.Page.InsertControl(SynEdit);
end;

function TShellPlugin.ParseTemplate(ATemplate: string): string;
var
  TemplateParser: TTemplateParser;
begin
  TemplateParser:=TTemplateParser.Create;
  try
    TemplateParser.StartDelimiter:='${';
    TemplateParser.EndDelimiter:='}';
    TemplateParser.values['workspaceFolder'] := TGlobalOpenFolder.Instance.OpenFolderName;
    Result := TemplateParser.ParseString(ATemplate);
  finally
    TemplateParser.Free;
  end;
end;

procedure TShellPlugin.HandleRunProcessRequest(ARequest: TRequest; out Finished: Boolean);
var
  TabSheet: TTabSheet;
  RequestData: TRunProcessData;
  PluginData: TShellPluginData;
  UniqueId: String;
begin
  TabSheet := TGlobalTabFactory.Instance.GetTab;
  TabSheet.Caption := 'Terminal';

  UniqueId := IntToStr(TabSheet.Tag);
  PluginData := FDataDictionary.GetData(UniqueId);
  PluginData.Page:=TabSheet;
  InitializeSynEdit(PluginData, UniqueId);
  RequestData := ARequest.Data as TRunProcessData;
  RunApp(PluginData, ParseTemplate(RequestData.Executable), RequestData.Parameters, ParseTemplate(RequestData.Cwd));
end;

procedure TShellPlugin.HandleCloseTabRequestedEvent(AnEvent: TEvent; out Finished: Boolean);
var
  PluginData: TShellPluginData;
begin
  Finished:=False;
  if FDataDictionary.TryGetData(AnEvent.Sender, PluginData) then
    begin
    Finished:=True;
    PluginData.CloseAfterProcessEnd:=True;
    PluginData.Thread.Terminate;
    end;
end;

procedure TShellPlugin.HandleShellFinishedEvent(AnEvent: TEvent; out Finished: Boolean);
var
  PluginData: TShellPluginData;
begin
  Finished:=True;
  PluginData := FDataDictionary.GetData(AnEvent.Sender);
  PluginData.Thread.Free;
  if PluginData.CloseAfterProcessEnd then
    PluginData.FPage.Free;
  FDataDictionary.Remove(AnEvent.Sender);
end;

procedure TShellPlugin.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
var
  RunProcessData: TRunProcessData;
begin
  Finished:=False;
  if AnEvent.Name='Startup' then
    begin
    // Could be used to initialize the shell when it is persistent over a restart
    // or something...

    RunProcessData := TRunProcessData.Create('/bin/bash');
    RunProcessData.Parameters.Add('-i');
    TGlobalRequestDispatcher.Instance.SendRequest('RunProcess', 'OpenTerminalAction', RunProcessData);
    end;
end;

{ TRunProcessData }

constructor TRunProcessData.Create(AnExcutable: TProcessString);
begin
  FExecutable:=AnExcutable;
  FParameters := TStringList.Create;
end;

destructor TRunProcessData.Destroy;
begin
  FParameters.Free;
  inherited Destroy;
end;

initialization
  TGlobalPluginList.Instance.Add(TShellPlugin.Create);
end.

