unit annotations;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  OpenFile,
  Events,
  Graphics,
  plugins,
  PluginData,
  synedittypes,
  SynEdit,
  SynEditMarkupGutterMark,
  SynEditMarkupPosition,
  SynEditPointClasses,
  SynEditMarkupSelection,
  Requests;

type
  TAnnotationLevel = (alInfo, alWarning, alError);

  { TAnnotation }

  TAnnotation = class
  private
    FLevel: TAnnotationLevel;
    FLineNr: Integer;
    FPosFrom: Integer;
    FPosTill: Integer;
    FText: string;
  public
    procedure Assign(ASourceAnnotation: TAnnotation);
  published
    property Level: TAnnotationLevel read FLevel write FLevel;
    property LineNr: Integer read FLineNr write FLineNr;
    property PosFrom: Integer read FPosFrom write FPosFrom;
    property PosTill: Integer read FPosTill write FPosTill;
    property Text: string read FText write FText;
  end;
  TAnnotationList = specialize TList<TAnnotation>;

  { TFileAnnotations }

  TFileAnnotations = class
  private
    FAnnotationList: TAnnotationList;
    FUniqueFileId: string;
  public
    constructor Create;
    destructor Destroy; override;
    //procedure Assign(ASourceFileAnnotations: TFileAnnotations);
  published
    property UniqueFileId: string read FUniqueFileId write FUniqueFileId;
    property AnnotationList: TAnnotationList read FAnnotationList;
  end;
  //TCustFileAnnotationsList = specialize TObjectList<TFileAnnotations>;

  { TFileAnnotationsList }

  //TFileAnnotationsList = class(TCustFileAnnotationsList)
  //public
  //  function FindbyUniqueFileId(AnUniqueFileId: string): TFileAnnotations;
  //end;

  TSynEditMarkupPositionList = specialize TObjectList<TSynEditMarkupPosition>;

  { TAnnotationsData }

  TAnnotationsData = class
  private
    FAnnotationList: TAnnotationList;
    FMarkupList: TSynEditMarkupPositionList;
  public
    constructor Create;
    destructor Destroy; override;
    property AnnotationList: TAnnotationList read FAnnotationList;
    property MarkupList: TSynEditMarkupPositionList read FMarkupList;
  end;
  TAnnotationsDataDictionary = specialize TPluginDataDictionary<TAnnotationsData>;

  { TAnnotations }

  TAnnotations = class(TPlugin)
  private
    // Keep a list of markups, so they can be removed when new annotations are
    // applied.
    //FAnnotationMarkupList: TSynEditMarkupPositionList;
    //FFileAnnotationsList: TFileAnnotationsList;
    FAnnotationData: TAnnotationsDataDictionary;
  protected
    procedure ApplyAnnotations(Annotations: TFileAnnotations);
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean); override;
    procedure HandleRequest(ARequest: TRequest; out Finished: Boolean); override;
  end;

implementation

uses
  LangServer,
  HoverToolTip;

{ TAnnotation }

procedure TAnnotation.Assign(ASourceAnnotation: TAnnotation);
begin
  FLevel := ASourceAnnotation.Level;
  FLineNr := ASourceAnnotation.LineNr;
  FPosFrom := ASourceAnnotation.PosFrom;
  FPosTill := ASourceAnnotation.PosTill;
  FText := ASourceAnnotation.Text;
end;

{ TFileAnnotations }

constructor TFileAnnotations.Create;
begin
  FAnnotationList := TAnnotationList.Create;
end;

destructor TFileAnnotations.Destroy;
begin
  FAnnotationList.Free;
  inherited Destroy;
end;

//procedure TFileAnnotations.Assign(ASourceFileAnnotations: TFileAnnotations);
//var
//  Annotation: TAnnotation;
//  i: Integer;
//begin
//  UniqueFileId:=ASourceFileAnnotations.UniqueFileId;
//  for i := 0 to ASourceFileAnnotations.AnnotationList.Count -1 do
//    begin
//    Annotation := TAnnotation.Create;
//    Annotation.Assign(ASourceFileAnnotations.AnnotationList.Items[i]);
//    AnnotationList.Add(Annotation);
//    end;
//end;

{ TAnnotationsData }

constructor TAnnotationsData.Create;
begin
  FMarkupList := TSynEditMarkupPositionList.Create;
  FAnnotationList := TAnnotationList.Create;
end;

destructor TAnnotationsData.Destroy;
begin
  FMarkupList.Free;
  FAnnotationList.Free;
  inherited Destroy;
end;

{ TFileAnnotationsList }

//function TFileAnnotationsList.FindbyUniqueFileId(AnUniqueFileId: string): TFileAnnotations;
//var
//  i: Integer;
//begin
//  for i := 0 to Count -1 do
//    if Items[i].UniqueFileId=AnUniqueFileId then
//      Exit(Items[i]);
//  result := nil;
//end;

{ TAnnotations }

procedure TAnnotations.ApplyAnnotations(Annotations: TFileAnnotations);
var
  OpnFile: TOpenFile;
  SynEdit: TSynEdit;
  i: Integer;
  Annotation: TAnnotation;
  Markup: TSynEditMarkupPosition;
  a: TSynEdit;
  Ann: TFileAnnotations;
  AnnotationData: TAnnotationsData;
begin
  AnnotationData := FAnnotationData.GetData(Annotations.UniqueFileId);

  AnnotationData.AnnotationList.Clear;

  for i := 0 to Annotations.AnnotationList.Count -1 do
    begin
    Annotation := TAnnotation.Create;
    Annotation.Assign(Annotations.AnnotationList[i]);
    AnnotationData.AnnotationList.Add(Annotation);
    end;

  OpnFile := TGlobalFileList.Instance.FindByUniqueId(Annotations.UniqueFileId);
  if Assigned(OpnFile) then
    begin
    SynEdit := OpnFile.SynEdit;
    SynEdit.BeginUpdate(False);
    try
      // Remove prior markups
      for i := AnnotationData.MarkupList.Count -1 downto 0 do
        begin
        Markup := AnnotationData.MarkupList[i];
        SynEdit.MarkupManager.RemoveMarkUp(Markup);
        AnnotationData.MarkupList.Delete(i);
        end;

      for i := 0 to Annotations.AnnotationList.Count -1 do
        begin
        Annotation := Annotations.AnnotationList.Items[i];

        Markup := TSynEditMarkupPosition.Create(SynEdit, Annotation.LineNr +1, Annotation.PosFrom -1, Annotation.PosTill -1);
        markup.MarkupInfo.Background:=SynEdit.Color;
        markup.MarkupInfo.Foreground:=SynEdit.Font.Color;
        markup.MarkupInfo.FrameStyle:=slsWaved;
        case Annotation.Level of
          alInfo:    markup.MarkupInfo.FrameColor:=TColor($FFA050);
          alWarning: markup.MarkupInfo.FrameColor:=clOlive;
          alError:   markup.MarkupInfo.FrameColor:=clRed;
        end;

        markup.MarkupInfo.FrameEdges:=sfeBottom;
        SynEdit.MarkupManager.AddMarkUp(Markup);
        AnnotationData.MarkupList.Add(Markup);
        end;
    finally
      SynEdit.Invalidate;
      SynEdit.EndUpdate;
    end;
    end;
end;

constructor TAnnotations.Create;
begin
  FAnnotationData := TAnnotationsDataDictionary.Create;
end;

destructor TAnnotations.Destroy;
begin
  FAnnotationData.Free;
  inherited Destroy;
end;

procedure TAnnotations.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
var
  Annotations: TFileAnnotations;
begin
  inherited HandleEvent(AnEvent, Finished);
  if AnEvent.Name = 'NewAnnotations' then
    begin
    Annotations := AnEvent.Data as TFileAnnotations;
    ApplyAnnotations(Annotations);
    end;
end;

procedure TAnnotations.HandleRequest(ARequest: TRequest; out Finished: Boolean);
var
  ReqHoverInfoParams: THoverInfoParams;
  Annotation: TAnnotation;
  i: Integer;
  AnnotationData: TAnnotationsData;
begin
  inherited HandleRequest(ARequest, Finished);
  if ARequest.Name='RequestTooltipInfo' then
    begin
    ReqHoverInfoParams := ARequest.Data as THoverInfoParams;

    AnnotationData := FAnnotationData.GetData(ReqHoverInfoParams.FileId);

    for i := 0 to AnnotationData.AnnotationList.Count -1 do
      begin
      Annotation := AnnotationData.AnnotationList[i];
      if (Annotation.LineNr=ReqHoverInfoParams.Position.Y -1) and (ReqHoverInfoParams.Position.X >= Annotation.PosFrom) and (ReqHoverInfoParams.Position.X < Annotation.PosTill) then
        TGlobalEventDispatcher.Instance.SendEvent('RequestTooltipInfoResponse', 'LanguageServer', THoverInfoRequestResponse.Create(Annotation.Text, ReqHoverInfoParams.Position));
      end;
    end;
end;

initialization
  TGlobalPluginList.Instance.Add(TAnnotations.Create);
end.

