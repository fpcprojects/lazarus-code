unit LanguageDetectionPlugin;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  // LazarusCode
  OpenFile,
  Events,
  plugins;

type

  { TLanguageDetectionPlugin }

  TLanguageDetectionPlugin = class(TPlugin)
  private
    procedure HandleLoadedFile(AnEvent: TEvent; out Finished: Boolean);
    procedure HandleClosedFile(AnEvent: TEvent; out Finished: Boolean);
  protected
    function ObtainLanguageId(AFileName: string; AFileContents: TStrings): string;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

implementation

{ TLanguageDetectionPlugin }

procedure TLanguageDetectionPlugin.HandleLoadedFile(AnEvent: TEvent; out Finished: Boolean);
var
  OpnFile: TOpenFile;
  DetectedLanguageId: String;
begin
  OpnFile := TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
  if Assigned(OpnFile) then
    begin
    DetectedLanguageId := ObtainLanguageId(OpnFile.Filename, OpnFile.SynEdit.Lines);
    OpnFile.LanguageId := DetectedLanguageId;
    end;
  Finished:=False;
end;

procedure TLanguageDetectionPlugin.HandleClosedFile(AnEvent: TEvent; out Finished: Boolean);
var
  OpnFile: TOpenFile;
begin
  OpnFile := TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
  if Assigned(OpnFile) then
    OpnFile.LanguageId := '';
end;

function TLanguageDetectionPlugin.ObtainLanguageId(AFileName: string; AFileContents: TStrings): string;
begin
  Result := '';
  case LowerCase(ExtractFileExt(AFileName)) of
    'pp',
    'lpr',
    'pas':
      Result := 'pascal';
    '.py':
      Result := 'python';
    'md':
      Result := 'markdown';
    'json':
      Result := 'json';
    'js':
      Result := 'javascript';
    'html':
      Result := 'html';
    'txt':
      Result := 'plaintext';
    'yaml',
    'yml':
      Result := 'yaml';
    'sql':
      Result := 'sql';
    'ts':
      Result := 'typescript';
    '':
      begin
      case AFileName of
        'Dockerfile':
          Result := 'dockerfile';
        'Makefile':
          Result := 'makefile';
      end; {case}
      end;
  end; {case}
end;

constructor TLanguageDetectionPlugin.Create;
begin
  inherited Create;
  TGlobalEventDispatcher.Instance.AddEventHandler('LoadedFile', @HandleLoadedFile);
  TGlobalEventDispatcher.Instance.AddEventHandler('ClosedFile', @HandleClosedFile);
end;

destructor TLanguageDetectionPlugin.Destroy;
begin
  TGlobalEventDispatcher.Instance.RemoveEventHandler('LoadedFile', @HandleLoadedFile);
  TGlobalEventDispatcher.Instance.RemoveEventHandler('ClosedFile', @HandleClosedFile);
  inherited Destroy;
end;

initialization
  TGlobalPluginList.Instance.Add(TLanguageDetectionPlugin.Create);
end.

