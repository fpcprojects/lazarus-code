unit languageserver_pylsp;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  plugins,
  LangServer;

type

  { TLangServerPylsp }

  TLangServerPylsp = class(TLanguageServer)
  public
    function DoesSupportLanguageId(ALanguageId: string): Boolean; override;
  end;

implementation

{ TLangServerPylsp }

function TLangServerPylsp.DoesSupportLanguageId(ALanguageId: string): Boolean;
begin
  Result := ALanguageId='python';
end;

initialization
  TGlobalPluginList.Instance.Add(TLangServerPylsp.Create);
end.

