unit sessions;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  fpjson,
  csModel,
  csGenericCollectionsDescriber,
  csJSONRttiStreamHelper,
  csRegisterCommonClassDescribers,
  plugins,
  OpenFolderAction,
  Actions,
  Events,
  OpenFolder,
  OpenFileAction,
  OpenFile, LangServerModel;

type

  { TSessionsPlugin }

  TSessionsPlugin = class(TPlugin)

  private
    type

      { TSessionFile }

      TSessionFile = class
      private
        FFileName: string;
        FShallow: Boolean;
      public
        constructor Create();
        constructor Create(AFileName: string; AShallow: Boolean);
      published
        property FileName: string read FFileName write FFileName;
        property Shallow: Boolean read FShallow write FShallow;
      end;
      TSessionFileList = specialize TObjectList<TSessionFile>;

      { TSession }

      TSession = class
      private
        FFolderName: string;
        FOpenFiles: TSessionFileList;
      public
        constructor Create;
        destructor Destroy; override;
      published
        property FolderName: string read FFolderName write FFolderName;
        property OpenFiles: TSessionFileList read FOpenFiles write FOpenFiles;
      end;
      TSessionList = specialize TObjectList<TSession>;

      { TSessions }

      TSessions = class
      private
        FSessions: TSessionList;
      public
        constructor Create;
        destructor Destroy; override;
      published
        property Sessions: TSessionList read FSessions write FSessions;

      end;
  protected
    function GetSessionInfoFilename: string;
    function CollectSessionInfo: TSession;
    procedure ApplySessionInfo(ASession: TSession);
    procedure LoadSessionInfo;
    procedure WriteSessionInfo;
  public
    constructor Create;
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean); override;
  end;

implementation

constructor TSessionsPlugin.TSessions.Create;
begin
  FSessions := TSessionList.Create();
end;

destructor TSessionsPlugin.TSessions.Destroy;
begin
  FSessions.Free;
  inherited Destroy;
end;

{ TSessionsPlugin }

function TSessionsPlugin.GetSessionInfoFilename: string;
begin
  Result := GetAppConfigFile(False, True);
end;

function TSessionsPlugin.CollectSessionInfo: TSession;
var
  Files: TOpenFileList;
  i: Integer;
begin
  Result := TSession.Create;
  Result.FolderName:=TGlobalOpenFolder.Instance.OpenFolderName;
  Files := TGlobalFileList.Instance;
  for i := 0 to Files.Count -1 do
    begin
    Result.OpenFiles.Add(TSessionFile.Create(Files[i].Filename, Files[i].IsShallow));
    end;
end;

procedure TSessionsPlugin.ApplySessionInfo(ASession: TSession);
var
  i: Integer;
begin
  if ASession.FolderName<>'' then
    TGlobalActionFactory.Instance.RunAction('OpenFolder', TOpenFolderParams.Create(ASession.FolderName));
      for i := 0 to ASession.OpenFiles.Count -1 do
    TGlobalActionFactory.Instance.RunAction('OpenFile', TOpenFileParams.Create(ASession.OpenFiles[i].FileName, ASession.OpenFiles[i].Shallow));
end;

procedure TSessionsPlugin.LoadSessionInfo;
var
  Sessions: TSessions;
  FN: String;
  JSon: string;
  FS: TStringStream;
begin
  FN:=GetSessionInfoFilename;
  if FileExists(FN) then
    begin
    FS := TStringStream.Create();
    try
      FS.LoadFromFile(FN);
      JSon:=FS.DataString;
    finally
      FS.Free;
    end;
    end;

  Sessions := TSessions.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject(JSon, Sessions, nil, [tcsdfCreateClassInstances, tcsdfDynamicClassDescriptions]);
    if Sessions.Sessions.Count > 0 then
      ApplySessionInfo(Sessions.Sessions[0]);
  finally
    Sessions.Free;
  end;
end;

procedure TSessionsPlugin.WriteSessionInfo;
var
  Sessions: TSessions;
  FN: String;
  JSon: string;
  FS: TStringStream;
begin
  Sessions := TSessions.Create;
  try
    Sessions.Sessions.Add(CollectSessionInfo);
    FN:=GetSessionInfoFilename;
    ForceDirectories(ExtractFilePath(FN));
    JSon := TJSONRttiStreamHelper.ObjectToJSONString(Sessions);
    FS := TStringStream.Create(JSon);
    try
      FS.SaveToFile(FN);
    finally
      FS.Free;
    end;
  finally
    Sessions.Free;
  end;
end;

constructor TSessionsPlugin.Create;
begin
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(specialize TcsGenericCollectionTListObjectDescriber<TSession>, 100);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(specialize TcsGenericCollectionTListObjectDescriber<TSessionFile>, 100);
end;

procedure TSessionsPlugin.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
begin
  Finished:=False;
  if (AnEvent.Name='LoadedFile') or (AnEvent.Name='OpenFolderChanged') or (AnEvent.Name='Unshallow') then
    begin
    WriteSessionInfo;
    end
  else if AnEvent.Name='Startup' then
    begin
    LoadSessionInfo;
    end;
end;

{ TSessionsPlugin.TSessionFile }

constructor TSessionsPlugin.TSessionFile.Create();
begin
  inherited Create;
end;

constructor TSessionsPlugin.TSessionFile.Create(AFileName: string; AShallow: Boolean);
begin
  Create();
  FFileName:=AFileName;
  FShallow:=AShallow;
end;

{ TSessionsPlugin.TSession }

constructor TSessionsPlugin.TSession.Create;
begin
  FOpenFiles := TSessionFileList.Create();
end;

destructor TSessionsPlugin.TSession.Destroy;
begin
  FOpenFiles.Free;
  inherited Destroy;
end;

initialization
  TGlobalPluginList.Instance.Add(TSessionsPlugin.Create);
end.

