unit HoverJumpToPlugin;

{$mode ObjFPC}{$H+}

interface

uses
  // rtl
  Classes,
  SysUtils,
  Generics.Collections,
  // LCL
  Graphics,
  Controls,
  // SynEdit
  SynEdit,
  SynEditTypes,
  // LazarusCode
  Actions,
  SynEditMarkupPosition,
  Callbacks,
  Plugins,
  Events,
  OpenFile,
  Requests,
  OpenFileAction;

type

  { THoverJumpToPlugin }

  THoverJumpToPlugin = class(TPlugin)
  private
    FHoverMarkup: TSynEditMarkupPosition;
    FCurrentSymbolLine: Integer;
    FCurrentSymbolStart: Integer;
    FCurrentSymbolEnd: Integer;
    FCurrentOpnFile: TOpenFile;
    FCurrentUniqueFileId: string;
    procedure HandleMouseClick(AnEvent: TEvent; out Finished: Boolean);
    procedure HandleNewEditor(AnEvent: TEvent; out Finished: Boolean);
    procedure HandleWindowClosed(AnEvent: TEvent; out Finished: Boolean);
    procedure SetUnderline(OpnFile:TOpenFile; Line, SymbolStart, SymbolEnd: Integer);
    procedure ClearUnderline;
    procedure HandleDeclarationInfoResponse(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
    procedure HandleMouseMove(AnEvent: TEvent; out Finished: Boolean);
    procedure UpdateSynCursor(Sender: TObject; const AMouseLocation: TSynMouseLocationInfo; var AnCursor: TCursor; var APriority: Integer; var AChangedBy: TObject);
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

  { TReference }

  TReference = class
  private
    FEndPos: TPoint;
    FFilename: string;
    FStartPos: TPoint;
  public
    property StartPos: TPoint read FStartPos write FStartPos;
    property EndPos: TPoint read FEndPos write FEndPos;
    property Filename: string read FFilename write FFilename;
  end;
  TReferenceList = specialize TObjectList<TReference>;

  { TPositionInfoData }

  TPositionInfoData = class
  private
    FPosition: TPoint;
    FReferenceList: TReferenceList;
    FUniqueFileId: string;
  public
    constructor Create(AnUniqueFileId: string; APosition: TPoint);
    destructor Destroy; override;
    property Position: TPoint read FPosition write FPosition;
    property Response: TReferenceList read FReferenceList;
  published
    property UniqueFileId: string read FUniqueFileId write FUniqueFileId;
  end;

  { TJumpToDefinitionParams }

  TJumpToDefinitionParams = class
  private
    FPosition: TPoint;
    FUniqueFileId: string;
  public
    constructor Create(AnUniqueFileId: string; APosition: TPoint);
    property UniqueFileId: string read FUniqueFileId write FUniqueFileId;
    property Position: TPoint read FPosition write FPosition;
  end;

  { TJumpToDefinitionAction }

  TJumpToDefinitionAction = class(TActionExecutor)
  private
    FJumpLocation: TPoint;
    procedure CallbackAfterFileFocused(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
    procedure HandleDeclarationInfoResponse(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
  public
    procedure Execute; override;
  end;

implementation


{ THoverJumpToPlugin }

procedure THoverJumpToPlugin.SetUnderline(OpnFile: TOpenFile; Line, SymbolStart, SymbolEnd: Integer);
begin
  ClearUnderline;

  FCurrentSymbolLine:=Line;
  FCurrentSymbolStart:=SymbolStart;
  FCurrentSymbolEnd:=SymbolEnd;
  FCurrentOpnFile:=OpnFile;

  FHoverMarkup := TSynEditMarkupPosition.Create(OpnFile.SynEdit, FCurrentSymbolLine, FCurrentSymbolStart, FCurrentSymbolEnd);
  FHoverMarkup.MarkupInfo.Background:=OpnFile.SynEdit.Color;
  FHoverMarkup.MarkupInfo.Foreground:=clBlue;
  FHoverMarkup.MarkupInfo.FrameStyle:=slsWaved;
  FHoverMarkup.MarkupInfo.FrameColor:=clblue;
  FHoverMarkup.MarkupInfo.FrameEdges:=sfeBottom;
  OpnFile.SynEdit.MarkupManager.AddMarkUp(FHoverMarkup);
  OpnFile.SynEdit.Invalidate;
  OpnFile.SynEdit.UpdateCursorOverride;
end;

procedure THoverJumpToPlugin.HandleMouseClick(AnEvent: TEvent; out Finished: Boolean);
begin
  if Assigned(FHoverMarkup) then
    TGlobalActionFactory.Instance.RunAction('JumpToDefinition', TJumpToDefinitionParams.Create(FCurrentOpnFile.UniqueId, TPoint.Create(FCurrentSymbolStart, FCurrentSymbolLine)));
end;

procedure THoverJumpToPlugin.HandleNewEditor(AnEvent: TEvent; out Finished: Boolean);
var
  OpnFile: TOpenFile;
begin
  Finished:=False;
  OpnFile := TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
  if Assigned(OpnFile) then
    begin
    OpnFile.SynEdit.RegisterQueryMouseCursorHandler(@UpdateSynCursor);
    end;
end;

procedure THoverJumpToPlugin.HandleWindowClosed(AnEvent: TEvent; out Finished: Boolean);
var
  OpnFile: TOpenFile;
begin
  Finished:=False;
  OpnFile := TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
  // ToDo, werkt niet, omdat OpnFile al niet meer bestaat (ook niet echt nodig, trouwens...)
  if Assigned(OpnFile) then
    begin
    OpnFile.SynEdit.UnRegisterQueryMouseCursorHandler(@UpdateSynCursor);
    end;
end;

procedure THoverJumpToPlugin.ClearUnderline;
begin
  if Assigned(FHoverMarkup) then
    begin
    FCurrentOpnFile.SynEdit.MarkupManager.RemoveMarkUp(FHoverMarkup);
    FCurrentOpnFile.SynEdit.Invalidate;
    FreeAndNil(FHoverMarkup);
    FCurrentOpnFile.SynEdit.UpdateCursorOverride;
    end;
end;

procedure THoverJumpToPlugin.HandleDeclarationInfoResponse(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
var
  i: String;
  Request: TRequest;
  PositionInfoData: TPositionInfoData;
  Reference: TReference;
  OpnFile: TOpenFile;
  EndX, StartX: integer;
begin
  i := (sender as trequest).data.ClassName;
  Request := sender as TRequest;
  if Request.Handled then
    begin
    PositionInfoData := Request.Data as TPositionInfoData;
    if Assigned(PositionInfoData.Response) and (PositionInfoData.Response.Count > 0) then
      begin
      OpnFile := TGlobalFileList.Instance.FindByUniqueId(PositionInfoData.UniqueFileId);
      if Assigned(OpnFile) then
        begin
        Reference := PositionInfoData.Response.Items[0];
        Assert(Reference.StartPos.y=Reference.EndPos.Y);

        OpnFile.SynEdit.GetWordBoundsAtRowCol(PositionInfoData.Position, StartX, EndX);
        SetUnderline(OpnFile, PositionInfoData.Position.Y, StartX, EndX);
        end;
      end;
    end;
end;

procedure THoverJumpToPlugin.HandleMouseMove(AnEvent: TEvent; out Finished: Boolean);
var
  OpnFile: TOpenFile;
  SynEdt: TSynEdit;
  Ps: TPoint;
  MoveEvent: TMouseMoveEvent;
  Req: TRequest;
begin
  MoveEvent := TMouseMoveEvent(AnEvent);
  if Assigned(FHoverMarkup) then
    begin
    Ps := FCurrentOpnFile.SynEdit.PixelsToLogicalPos(MoveEvent.CursorPos);
    if (PS.Y <> FCurrentSymbolLine) or (PS.X < FCurrentSymbolStart) or (PS.X > FCurrentSymbolEnd) then
      ClearUnderline;
    end;

  if not Assigned(FHoverMarkup) and (ssCtrl in MoveEvent.Shift) then
    begin
    OpnFile := TGlobalFileList.Instance.GetFocusedFile;
    SynEdt := OpnFile.SynEdit;
    Ps := SynEdt.PixelsToLogicalPos(MoveEvent.CursorPos);

    Req := TRequest.Create('DeclarationInfo', 'HoverJumpToPlugin', TPositionInfoData.Create(OpnFile.UniqueId, Ps));
    (Req as ICallbackDispatcher).RegisterCallback(@HandleDeclarationInfoResponse, 0);
    TGlobalRequestDispatcher.Instance.SendRequest(Req);

    Req := TRequest.Create('DefinitionInfo', 'HoverJumpToPlugin', TPositionInfoData.Create(OpnFile.UniqueId, Ps));
    (Req as ICallbackDispatcher).RegisterCallback(@HandleDeclarationInfoResponse, 0);
    TGlobalRequestDispatcher.Instance.SendRequest(Req);
    end;
end;
procedure THoverJumpToPlugin.UpdateSynCursor(Sender: TObject; const AMouseLocation: TSynMouseLocationInfo; var AnCursor: TCursor; var APriority: Integer; var AChangedBy: TObject);
const
  LINK_CURSOR_PRIORITY = 1;
begin
  if (APriority > LINK_CURSOR_PRIORITY) then exit;
  if Assigned(FHoverMarkup) then
    AnCursor:=crHandPoint
  else
    AnCursor:=crDefault;
  APriority := LINK_CURSOR_PRIORITY;
  AChangedBy := Self;
end;

constructor THoverJumpToPlugin.Create;
begin
  inherited Create;
  TGlobalEventDispatcher.Instance.AddEventHandler('MouseMove', @HandleMouseMove);
  TGlobalEventDispatcher.Instance.AddEventHandler('MouseClick', @HandleMouseClick);
  TGlobalEventDispatcher.Instance.AddEventHandler('NewEditor', @HandleNewEditor);
  TGlobalEventDispatcher.Instance.AddEventHandler('WindowClosed', @HandleWindowClosed);
end;

destructor THoverJumpToPlugin.Destroy;
begin
  ClearUnderline;
  inherited Destroy;
end;

{ TPositionInfoData }

constructor TPositionInfoData.Create(AnUniqueFileId: string; APosition: TPoint);
begin
  FReferenceList:=TReferenceList.Create;
  FUniqueFileId:=AnUniqueFileId;
  FPosition:=APosition;
end;

destructor TPositionInfoData.Destroy;
begin
  FReferenceList.Free;
  inherited Destroy;
end;

{ TJumpToDefinitionParams }

constructor TJumpToDefinitionParams.Create(AnUniqueFileId: string; APosition: TPoint);
begin
  FUniqueFileId:=AnUniqueFileId;
  FPosition:=APosition;
end;

{ TJumpToActionAction }

procedure TJumpToDefinitionAction.CallbackAfterFileFocused(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
var
  OpnFile: TOpenFile;
begin
  OpnFile := TGlobalFileList.Instance.GetFocusedFile;
  OpnFile.SynEdit.CaretXY := FJumpLocation;

  ForgetMe:=True;
end;

procedure TJumpToDefinitionAction.HandleDeclarationInfoResponse(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
var
  i: String;
  Request: TRequest;
  PositionInfoData: TPositionInfoData;
  Reference: TReference;
  OpnFile: TOpenFile;
  EndX, StartX: integer;
  Data: TOpenFileParams;
begin
  i := (sender as trequest).data.ClassName;
  Request := sender as TRequest;
  if Request.Handled then
    begin
    PositionInfoData := Request.Data as TPositionInfoData;
    if Assigned(PositionInfoData.Response) and (PositionInfoData.Response.Count > 0) then
      begin
      Reference:=PositionInfoData.Response[0];
      OpnFile := TGlobalFileList.Instance.FindByFilename(Reference.Filename);
      FJumpLocation := Reference.StartPos;
      if Assigned(OpnFile) then
        begin
        TGlobalRequestDispatcher.Instance.SendRequest('SetFocus', OpnFile.UniqueId, nil, @CallbackAfterFileFocused);
        end
      else
        begin
        Data := TOpenFileParams.Create(Reference.Filename, True);
        TGlobalActionFactory.Instance.RunAction('OpenFile', Data, @CallbackAfterFileFocused);
        end;
      end;
    end;
end;

procedure TJumpToDefinitionAction.Execute;
var
  //HoverInfoParams: THoverInfoParams;
  //HoverParams: THoverParams;
  OpnFile: TOpenFile;
  //SynEdt: TSynEdit;
  Ps: TPoint;
  Req: TRequest;
  JumpToParams: TJumpToDefinitionParams;
begin
  JumpToParams := Params as TJumpToDefinitionParams;
  //FPosition := HoverInfoParams.Position;
  OpnFile:=TGlobalFileList.Instance.FindByUniqueId(JumpToParams.UniqueFileId);
  if Assigned(OpnFile) then
    begin
    //SynEdt := OpnFile.SynEdit;

    Req := TRequest.Create('DefinitionInfo', 'JumpToDefinitionAction', TPositionInfoData.Create(OpnFile.UniqueId, JumpToParams.Position));
    (Req as ICallbackDispatcher).RegisterCallback(@HandleDeclarationInfoResponse, 0);
    TGlobalRequestDispatcher.Instance.SendRequest(Req);

    //HoverParams := THoverParams.Create;
    //HoverParams.TextDocument.Uri:= TLanguageServer.FilenameToUri(OpnFile.Filename);
    //
    //HoverParams.Position.Line:=Max(HoverInfoParams.Position.Y,1)-1;
    //HoverParams.Position.Character:=Max(HoverInfoParams.Position.X,1)-1;
    //
    //FRequestId := 'HIA'+IntToStr(Random(999999));
    //
    //TGlobalEventDispatcher.Instance.SendEvent('LangServerRequest', 'HoverInfoAction', TLangServerRequestData.Create('textDocument/hover', OpnFile.LanguageId, FRequestId, HoverParams));
    end;
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('JumpToDefinition', 'Jump to definition', 'Language server', TJumpToDefinitionAction);
  TGlobalPluginList.Instance.Add(THoverJumpToPlugin.Create);
end.

