unit ShowLog;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  // LCL
  Controls,
  // SynEdit
  SynEdit,
  SynEditTypes,
  // log4fpc
  TAppenderUnit,
  TLayoutUnit,
  TLoggingEventUnit,
  TSimpleLayoutUnit,
  // LazarusCode
  Requests,
  Plugins,
  Events,
  Tabs,
  Logging,
  // LCL
  ComCtrls,
  ExtCtrls;

type

  { TLogFilter }

  TLogFilter = class
  private
    FOnFilterChanged: TNotifyEvent;
    FShowLogCategories: TStringList;
    function GetShowCategories: TStrings;
    procedure LogCategoriesChanged(Sender: TObject);
  public
    constructor Create;
    destructor Destroy; override;
    property ShowCategories: TStrings read GetShowCategories;
    property OnFilterChanged: TNotifyEvent read FOnFilterChanged write FOnFilterChanged;
  end;

  { TShowLogPlugin }

  TShowLogPlugin = class(TPlugin)
  private
    FSynEdit: TSynEdit;
    FTabSheet: TTabSheet;
    FAppender: TAppender;
    FLogInstanceFilter: TCheckGroup;
    FLogFilter: TLogFilter;

    procedure InitializeSynEdit;
    procedure HandleShowLogsRequest(ARequest: TRequest; out Finished: Boolean);
    procedure HandleCloseTabRequestedEvent(AnEvent: TEvent; out Finished: Boolean);
    procedure OnCheckgroupItemClicked(Sender: TObject; Index: integer);
    procedure OnLogFrameChanged(Sender: TObject);
  protected
  public
    constructor Create();
    destructor Destroy; override;
  end;


implementation

type

  { TSynEditLogAppender }

  TSynEditLogAppender = class(TAppender)
  private
    procedure DoFilterChanged(Sender: TObject);
  protected type
    TLoggingEventList = specialize TObjectList<TLoggingEvent>;

    { TSELayout }

    TSELayout = class (TLayout)
    private
    public
       function Format(AEvent : TLoggingEvent) : String; Override;
    end;
  protected var
    FSynEdit: TSynEdit;
    FLogCache: TLoggingEventList;
    FLogFilter: TLogFilter;
    procedure AddEventToSynedit(AEvent : TLoggingEvent);
  public
    constructor Create(ASynEdit: TSynEdit; ALogFilter:TLogFilter; ALayout: TLayout);
    destructor Destroy; Override;
    procedure Append(AEvent : TLoggingEvent); override;
    function RequiresLayout() : Boolean; override;
  end;

{ TSynEditLogAppender.TSELayout }

function TSynEditLogAppender.TSELayout.Format(AEvent: TLoggingEvent): String;
begin
  Result := FormatDateTime('yyyy-MM-dd hh:mm:ss.zzz', AEvent.GetStartTime) + ' [' + AEvent.GetLevel.ToString + '] - ' + AEvent.GetMessage;
end;

{ TSynEditLogAppender }

procedure TSynEditLogAppender.DoFilterChanged(Sender: TObject);
var
  i: Integer;
begin
  FSynEdit.BeginUpdate(False);
  try
    FSynEdit.Clear;
    for i := 0 to FLogCache.Count -1 do
      AddEventToSynedit(FLogCache[i]);
  finally
    FSynEdit.EndUpdate;
  end;
end;

procedure TSynEditLogAppender.AddEventToSynedit(AEvent: TLoggingEvent);
begin
  if FLogFilter.ShowCategories.IndexOf(AEvent.GetLogger) > -1 then
    FSynEdit.Lines.Add(AEvent.GetMessage());
end;

constructor TSynEditLogAppender.Create(ASynEdit: TSynEdit; ALogFilter: TLogFilter; ALayout: TLayout);
begin
  inherited Create;
  FSynEdit:=ASynEdit;
  if not Assigned(ALayout) then
    ALayout := TSELayout.Create;
  FLayout := ALayout;
  FLogCache := TLoggingEventList.Create(True);
  FLogFilter := ALogFilter;
  FLogFilter.OnFilterChanged:=@DoFilterChanged;
end;

destructor TSynEditLogAppender.Destroy;
begin
  FLogCache.Free;
  inherited Destroy;
end;

procedure TSynEditLogAppender.Append(AEvent: TLoggingEvent);
var
  CachedEvent: TLoggingEvent;
begin
   if (Self.FClosed) then begin
      if (Self.FErrorHandler <> Nil) then
         Self.FErrorHandler.Error(
            'This appender is closed and cannot be written to.');
      Exit;
   end;

   CachedEvent := TLoggingEvent.Create(AEvent.GetLevel, Self.Flayout.Format(AEvent), AEvent.GetLogger);
   FLogCache.Add(CachedEvent);

   AddEventToSynedit(CachedEvent);
end;

function TSynEditLogAppender.RequiresLayout: Boolean;
begin
  Result:=true;
end;

{ TLogFilter }

function TLogFilter.GetShowCategories: TStrings;
begin
  Result := FShowLogCategories;
end;

procedure TLogFilter.LogCategoriesChanged(Sender: TObject);
begin
  if Assigned(FOnFilterChanged) then
    FOnFilterChanged(Sender);
end;

constructor TLogFilter.Create;
begin
  FShowLogCategories := TStringList.Create;
  FShowLogCategories.OnChange:=@LogCategoriesChanged;
end;

destructor TLogFilter.Destroy;
begin
  FShowLogCategories.Free;
  inherited Destroy;
end;

{ TShowLogPlugin }

procedure TShowLogPlugin.InitializeSynEdit;
var
  SynEditPanel: TPanel;
  AdditionalPanel: TPanel;
  Splitter: TSplitter;
  LogFactory: TLogFactory;
  i: Integer;
begin
  SynEditPanel := TPanel.Create(FTabSheet);
  SynEditPanel.Align:=alClient;
  FTabSheet.InsertControl(SynEditPanel);

  Splitter := TSplitter.Create(FTabSheet);
  Splitter.Align:=alRight;
  FTabSheet.InsertControl(Splitter);

  AdditionalPanel := TPanel.Create(FTabSheet);
  AdditionalPanel.Align:=alRight;
  FTabSheet.InsertControl(AdditionalPanel);

  FLogInstanceFilter := TCheckGroup.Create(FTabSheet);
  FLogInstanceFilter.Align:=alTop;
  FLogInstanceFilter.Caption:='Filter';
  FLogInstanceFilter.OnClick:=@OnLogFrameChanged;
  FLogInstanceFilter.OnItemClick:=@OnCheckgroupItemClicked;
  AdditionalPanel.InsertControl(FLogInstanceFilter);

  LogFactory := TGlobalLogFactory.Instance;
  for i := 0 to LogFactory.Count -1 do
    FLogInstanceFilter.Items.Add(LogFactory.LoggerItems[i].GetName());

  FSynEdit:=TSynEdit.Create(SynEditPanel);
  FSynEdit.Align:=alClient;
  FSynEdit.Gutter.Parts.Clear;
  FSynEdit.Options := FSynEdit.Options - [eoAutoIndent, eoNoCaret] + [eoScrollPastEof];
  fSynEdit.InsertMode:=false;
  FSynEdit.MaxUndo:=0;

  SynEditPanel.InsertControl(FSynEdit);

  FAppender := TSynEditLogAppender.Create(FSynEdit, FLogFilter, nil);
  TGlobalLogFactory.Instance.AddAppender(FAppender);

  OnLogFrameChanged(Self);
end;

procedure TShowLogPlugin.HandleShowLogsRequest(ARequest: TRequest; out Finished: Boolean);
begin
  if not Assigned(FTabSheet) then
    begin
    FTabSheet := TGlobalTabFactory.Instance.GetTab;
    FTabSheet.Caption := 'Logs';
    InitializeSynEdit;
    end;
end;

procedure TShowLogPlugin.HandleCloseTabRequestedEvent(AnEvent: TEvent; out Finished: Boolean);
begin
  Finished:=False;
  if AnEvent.Sender=IntToStr(FTabSheet.Tag) then
    begin
    Finished:=True;
    FreeAndNil(FTabSheet);
    TGlobalLogFactory.Instance.RemoveAppender(FAppender);
    end;
end;

procedure TShowLogPlugin.OnCheckgroupItemClicked(Sender: TObject; Index: integer);
begin
  OnLogFrameChanged(Sender);
end;

procedure TShowLogPlugin.OnLogFrameChanged(Sender: TObject);
var
  i: Integer;
begin
  FLogFilter.ShowCategories.Clear;

  FLogFilter.ShowCategories.BeginUpdate;
  try
    for i := 0 to FLogInstanceFilter.Items.Count -1 do
      if FLogInstanceFilter.Checked[i] then
        FLogFilter.ShowCategories.add(FLogInstanceFilter.Items[i]);
  finally
    FLogFilter.ShowCategories.EndUpdate;
  end;
end;

constructor TShowLogPlugin.Create();
begin
  TGlobalRequestDispatcher.Instance.AddRequestHandler('ShowLogs', @HandleShowLogsRequest);
  TGlobalEventDispatcher.Instance.AddEventHandler('CloseTabRequested', @HandleCloseTabRequestedEvent);
  FLogFilter := TLogFilter.Create;
end;

destructor TShowLogPlugin.Destroy;
begin
  FLogFilter.Free;
  inherited Destroy;
end;

initialization
  TGlobalPluginList.Instance.Add(TShowLogPlugin.Create);
end.

