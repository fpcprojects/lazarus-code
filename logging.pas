unit Logging;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  // Log4Fpc
  TLoggerUnit,
  TEventLogAppenderUnit,
  TAppenderUnit,
  // LazarusCode
  Singleton;

type

  { TLogFactory }

  TLogFactory = class(TObject)
  private
  private type
    TLoggerList = specialize TObjectList<TLogger>;
  private var
    FLoggerList: TLoggerList;
    FAppenderList: TAppendersCollection;
    FEventLogAppender: TEventLogAppender;
    function GetCount: integer;
    function GetLoggerItem(const i: integer): TLogger;
  public
    constructor Create;
    destructor Destroy; override;

    function InitializeLogger(const AName : string): TLogger;
    procedure AddAppender(const AnAppender: TAppender);
    procedure RemoveAppender(const AnAppender: TAppender);

    property LoggerItems[i: integer]: TLogger read GetLoggerItem;
    property Count: integer read GetCount;
  end;
  TGlobalLogFactory = specialize TSingleton<TLogFactory>;

implementation

{ TLogFactory }

function TLogFactory.GetLoggerItem(const i: integer): TLogger;
begin
  Result := FLoggerList.Items[i];
end;

function TLogFactory.GetCount: integer;
begin
  Result := FLoggerList.Count;
end;

constructor TLogFactory.Create;
begin
  FLoggerList := TLoggerList.Create(false);
  FAppenderList := TAppendersCollection.Create;

  FEventLogAppender := TEventLogAppender.Create;
  AddAppender(FEventLogAppender);
end;

destructor TLogFactory.Destroy;
begin
  FAppenderList.Delete(FEventLogAppender.GetName());
  FEventLogAppender.Free;
  FAppenderList.Free;
  FLoggerList.Free;
  inherited Destroy;
end;

function TLogFactory.InitializeLogger(const AName: string): TLogger;
var
  i: Integer;
begin
  Result := TLogger.GetInstance(AName);

  for i := 0 to FAppenderList.Count -1 do
    Result.AddAppender(FAppenderList.Items[i]);

  FLoggerList.Add(Result);
end;

procedure TLogFactory.AddAppender(const AnAppender: TAppender);
var
  i: Integer;
begin
  for i := 0 to FLoggerList.Count -1 do
    FLoggerList.Items[i].AddAppender(AnAppender);
end;

procedure TLogFactory.RemoveAppender(const AnAppender: TAppender);
var
  i: Integer;
begin
  for i := 0 to FLoggerList.Count -1 do
    FLoggerList.Items[i].RemoveAppender(AnAppender.GetName());
end;

end.

