unit HoverToolTip;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils, Types,
  plugins,
  Forms,
  InterfaceBase,
  Events,
  // SynEdit
  SynEdit,
  // Log4Fpc
  TLoggerUnit,
  TLevelUnit,
  // LazarusCode
  Actions,
  Requests,
  Logging,
  Singleton,
  OpenFile, LMessages,
  LCLIntf, Graphics, Controls, ExtCtrls,
  Themes, ComCtrls, LResources;

type

  { THoverInfoRequestResponse }

  THoverInfoRequestResponse = class
  private
    FHint: string;
    FPosition: TPoint;
  public
    constructor Create(AHint: string; APosition: TPoint);
    property Hint: string read FHint write FHint;
    property Position: TPoint read FPosition;
  end;

  { THoverTooltipPlugin }

  THoverTooltipPlugin = class(TPlugin)
  private
    FPreviousRowColumn: TPoint;
    FIdleTimer: TIdleTimer;
    FSendId: string;
    FHintWindow: THintWindow;
    FInfoRequested: Boolean;
    FLogInstance: TLogger;
    FHint: string;
    procedure CancelTimer;
    procedure DoOnUserInput(Sender: TObject; Msg: Cardinal);
    procedure DoOnUserInputCancelTimer(Sender: TObject; Msg: Cardinal);
    procedure IdleTimer(Sender: TObject);
    procedure OnHoverInfoCompleted(Response: THoverInfoRequestResponse);
    procedure StartTimer;
    procedure RequestHintInfo(CursorPos: TPoint);
    procedure StartTimer(Sender: TObject);
    procedure StopTimer;
    procedure HideHint;

    function MousePixelsToCursor(APixelPoint: TPoint): TPoint;
  public
    constructor Create();
    destructor Destroy; override;
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean); override;
  end;


implementation

uses
  LangServer;


{ THoverInfoRequestResponse }

constructor THoverInfoRequestResponse.Create(AHint: string; APosition: TPoint);
begin
  FHint:=AHint;
  FPosition:=APosition;
end;

{ THoverTooltipPlugin }

procedure THoverTooltipPlugin.CancelTimer;
begin
  FIdleTimer.Enabled:=False;
end;

procedure THoverTooltipPlugin.DoOnUserInput(Sender: TObject; Msg: Cardinal);
var
  CursorPos: TPoint;
begin
  if not (Application.MouseControl is TSynEdit) then
    begin
    FLogInstance.Warn('Moused moved outside of text-editor');
    HideHint;
    end
  else if (Msg <> LM_MOUSEMOVE) then
    begin
    FLogInstance.Warn('User input reported');
    HideHint;
    end
  else
    begin
    GetCursorPos(CursorPos);
    CursorPos:=MousePixelsToCursor(CursorPos);
    if CursorPos.X < 1 then
      begin
      FLogInstance.Warn('Mouse moved inside gutter');
      HideHint;
      end;
    if CursorPos<>FPreviousRowColumn then
      begin
      RequestHintInfo(CursorPos);
      FPreviousRowColumn:=CursorPos;
      end;
    end;
end;

procedure THoverTooltipPlugin.DoOnUserInputCancelTimer(Sender: TObject; Msg: Cardinal);
begin
  if Msg <> LM_MOUSEMOVE then
    begin
    FLogInstance.Warn('User input detected (mouse excluded)');
    FIdleTimer.Enabled:=False;
    FInfoRequested:=False;
    Application.RemoveOnUserInputHandler(@DoOnUserInputCancelTimer);
    end;
end;

procedure THoverTooltipPlugin.IdleTimer(Sender: TObject);
var
  CursorPos: TPoint;
  RowColumn: TPoint;
begin
  FLogInstance.Warn('IdleTimer fired');
  FIdleTimer.AutoEnabled:=False;
  FIdleTimer.Enabled:=False;
  if (Application.MouseControl is TSynEdit) then
    begin
    GetCursorPos(CursorPos);
    RowColumn:=MousePixelsToCursor(CursorPos);
    FLogInstance.Warn('Idle mouse position: ' + IntToStr(RowColumn.X) + ':' + IntToStr(RowColumn.Y));
    if (RowColumn=FPreviousRowColumn) and (RowColumn.X > 0) then
      begin
      RequestHintInfo(RowColumn);
      end;
    end;
end;

procedure THoverTooltipPlugin.OnHoverInfoCompleted(Response: THoverInfoRequestResponse);
var
  OpnFile: TOpenFile;
  SynEdt: TSynEdit;
  ClientXY: TPoint;
  NewBounds, r: TRect;
begin
  if not FInfoRequested then
    // In the mean time cancelled due to user-input
    begin
    FLogInstance.Warn('Hint-info received, but ignored because of user-input.');
    Exit;
    end;

  if Response.Hint = '' then
    begin
    FLogInstance.Warn('The language server does not has a hint for the given location');
    HideHint;
    Exit;
    end;

  if FHintWindow = nil then
    FHintWindow := THintWindow.Create(nil);
  OpnFile := TGlobalFileList.Instance.GetFocusedFile;
  SynEdt := OpnFile.SynEdit;

  ClientXY:=Response.Position;
  ClientXY.y:=ClientXY.y+1;
  ClientXY:=SynEdt.ScreenXYToPixels(SynEdt.TextXYToScreenXY(ClientXY));
  ClientXY:=SynEdt.ClientToScreen(ClientXY);

  ClientXY.Y:=ClientXY.Y+5;
  ClientXY.X:=ClientXY.X-20;

  Application.AddOnUserInputHandler(@DoOnUserInput);

  if FHint<>'' then
    FHint:=FHint+LineEnding;
  FHint:=FHint+Response.Hint;

  // Why?
  FHintWindow.DisableAlign;
  NewBounds := FHintWindow.CalcHintRect(0, FHint, nil);
  Types.OffsetRect(NewBounds, ClientXY.x,ClientXY.y-4);
  FHintWindow.ActivateHint(NewBounds, FHint);
  FHintWindow.EnableAlign;
  FHintWindow.Invalidate;
  FHintWindow.Update;
end;

procedure THoverTooltipPlugin.StartTimer;
begin
  if not FIdleTimer.Enabled then
    FIdleTimer.AutoEnabled:=True;
end;

procedure THoverTooltipPlugin.RequestHintInfo(CursorPos: TPoint);
var
  HoverInfoParams: THoverInfoParams;
  FileId: String;
begin
  if Assigned(TGlobalFileList.Instance.GetFocusedFile) then
    FileId := TGlobalFileList.Instance.GetFocusedFile.UniqueId
  else
    Exit;

  FLogInstance.Warn('Request hint info');
  FInfoRequested:=True;

  HoverInfoParams := THoverInfoParams.Create;
  HoverInfoParams.FileId:=FileId;
  HoverInfoParams.Position:=CursorPos;
  FHint := '';
  TGlobalRequestDispatcher.Instance.SendRequest('RequestTooltipInfo', 'HoverTooltipPlugin', HoverInfoParams);
end;

procedure THoverTooltipPlugin.StartTimer(Sender: TObject);
begin
  Application.AddOnUserInputHandler(@DoOnUserInputCancelTimer);
  FIdleTimer.AutoEnabled:=false;
end;

procedure THoverTooltipPlugin.StopTimer;
begin
  FIdleTimer.Enabled := False;
end;

procedure THoverTooltipPlugin.HideHint;
begin
  if Assigned(FHintWindow) then
    begin
    FLogInstance.Warn('Hide hintwindow');
    FHintWindow.Visible:=False;
    end;
  Application.RemoveOnUserInputHandler(@DoOnUserInput);
end;

function THoverTooltipPlugin.MousePixelsToCursor(APixelPoint: TPoint): TPoint;
begin
  APixelPoint := (Application.MouseControl as TSynEdit).ScreenToClient(APixelPoint);
  Result := (Application.MouseControl as TSynEdit).PixelsToLogicalPos(APixelPoint);
end;

constructor THoverTooltipPlugin.Create();
begin
  FSendId:='HoverTooltipPlugin';
  FIdleTimer:=TIdleTimer.Create(nil);
  FIdleTimer.Enabled:=False;
  FIdleTimer.AutoStartEvent:=itaOnIdle;
  FIdleTimer.AutoEndEvent:=itaOnUserInput;
  FIdleTimer.OnStartTimer:=@StartTimer;
  FIdleTimer.OnTimer:=@IdleTimer;
end;

destructor THoverTooltipPlugin.Destroy;
begin
  FIdleTimer.Free;
  inherited Destroy;
end;

procedure THoverTooltipPlugin.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
var
  OpnFile: TOpenFile;
  SynEdt: TSynEdit;
  ClientXY: TPoint;
  NewBounds: TRect;
  Ps: TPoint;
begin
  Finished:=False;
  if AnEvent.Name='Startup' then
    begin
    FLogInstance := TGlobalLogFactory.Instance.InitializeLogger('tooltipplugin');
    FLogInstance.SetLevel(DEBUG);
    end
  else if AnEvent.Name='MouseMove' then
    begin
    OpnFile := TGlobalFileList.Instance.GetFocusedFile;
    SynEdt := OpnFile.SynEdit;
    Ps := SynEdt.PixelsToLogicalPos(Events.TMouseMoveEvent(AnEvent).CursorPos);
    FLogInstance.Warn('Mouse move position: ' + IntToStr(Ps.X) + ':' + IntToStr(Ps.Y));

    if Ps <> FPreviousRowColumn then
      begin
      FPreviousRowColumn := ps;
      FLogInstance.Warn('moved, cancek');
      CancelTimer;
      end;

    StartTimer;
    end
  else if AnEvent.Name='RequestTooltipInfoResponse' then
    begin
    OnHoverInfoCompleted(AnEvent.Data as THoverInfoRequestResponse)
    end;
end;

initialization
  TGlobalPluginList.Instance.Add(THoverTooltipPlugin.Create);

end.

