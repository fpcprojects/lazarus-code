unit Actions;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Singleton,
  ActnList,
  Events,
  Callbacks,
  CustApp,
  Forms,
  Generics.Collections;

type

  { TActionExecutor }

  TActionExecutorState = (Init, Executing, Background, Failed, Success);
  TActionExecutor = class(ICallbackDispatcher)
  private
    FDestroying: Boolean;
    FDispatcher: TCallbackDispatcher;
    FParams: TObject;
    FState: TActionExecutorState;
  protected
    procedure TriggerDoneCallbacks;
    procedure SetFailed;
    procedure SetSuccess;
    property Params: TObject read FParams;
    property Dispatcher: TCallbackDispatcher read FDispatcher implements ICallbackDispatcher;
  public
    constructor Create(AParams: TObject); virtual;
    destructor Destroy; override;
    procedure HandleEvent(AnEvent: TEvent; out EventFinished: Boolean); virtual;
    procedure Execute; virtual; abstract;
    property State:TActionExecutorState read FState;
    property Destroying: Boolean read FDestroying;
  end;
  TActionExecutorClass = class of TActionExecutor;

  TCustActionList = specialize TObjectList<TActionExecutor>;

  { TActionRegistration }

  TActionRegistration = class
  private
    FDescription: string;
    FName: string;
    FMenuName: string;
    FActionClass: TActionExecutorClass;
  public
    constructor Create(AName, ADescription, AMenuName: string; AnActionClass: TActionExecutorClass);
    property Name: string read FName;
    property Description: string read FDescription;
    property MenuName: string read FMenuName;
  end;
  TActionRegistrationList = specialize TObjectList<TActionRegistration>;

  TActionCompletedCallback = procedure(Sender: TActionExecutor; CallerData: PtrInt);

  { TActionFactory }

  TActionFactory = class
  private
    FRegistrationList: TActionRegistrationList;
    FRunningActionList: TCustActionList;
    FRemoveActionList: TCustActionList;
    FActionList: TActionList;
    function GetActionCount: Integer;
    function GetRegisteredAction(i: Integer): TActionRegistration;
    procedure UpdateLCLActionList;
    function GetLCLActionList: TActionList;
  protected
    function FindName(AName: string): TActionRegistration;
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure RegisterAction(AName, ADescription, AMenuName: string; AnActionClass: TActionExecutorClass);
    procedure CleanupOldActions;
    procedure RunAction(AName: string; AParams: TObject = nil; ACallback: TCallback = nil; ACallerData: PtrInt = 0);
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean);
    property RegisteredActionCount: Integer read GetActionCount;
    property RegisteredAction[i: Integer]: TActionRegistration read GetRegisteredAction;
  end;
  TGlobalActionFactory = specialize TSingleton<TActionFactory>;

  { TActionParams }

  TActionParams = class
  private
    FUniqueId: string;
  public
    constructor Create(AnUniqueId: string); virtual;
    property UniqueId: string read FUniqueId write FUniqueId;
  end;

  { TLazarusCodeAction }

  TLazarusCodeAction = class(TCustomAction)
  private
    FActionExecutorClass: TActionExecutorClass;
    FCallback: TCallback;
    FCallerData: PtrInt;
    FParams: TObject;
    procedure DoOnExecute(Sender: TObject);
  protected
    property Params: TObject read FParams;
    property Callback: TCallback read FCallback;
    property CallerData: PtrInt read FCallerData;
    procedure SetTempActionData(AParams: TObject; ACallBack: TCallback; ACallerData: PtrInt);
    procedure ClearTempActionData;
  public
    constructor Create(AOwner: TComponent; AnActionExecutorClass: TActionExecutorClass); virtual;
    function Execute: Boolean; override;
    property ActionExecutorClass: TActionExecutorClass read FActionExecutorClass;
  end;


implementation

{ TActionExecutor }

procedure TActionExecutor.TriggerDoneCallbacks;
begin
  FDispatcher.TriggerCallbacks(Self);
end;

procedure TActionExecutor.SetFailed;
begin
  FState:=TActionExecutorState.Failed;
  TriggerDoneCallbacks;
end;

procedure TActionExecutor.SetSuccess;
begin
  FState:=TActionExecutorState.Success;
  TriggerDoneCallbacks;
end;

constructor TActionExecutor.Create(AParams: TObject);
begin
  FParams:=AParams;
  FDispatcher := TCallbackDispatcher.Create;
end;

destructor TActionExecutor.Destroy;
begin
  FDispatcher.Free;
  FDispatcher := nil;
  FParams.Free;
  inherited;
end;

procedure TActionExecutor.HandleEvent(AnEvent: TEvent; out EventFinished: Boolean);
begin
  EventFinished:=False;
end;

{ TActionRegistration }

constructor TActionRegistration.Create(AName, ADescription, AMenuName: string; AnActionClass: TActionExecutorClass);
begin
  FDescription:=ADescription;
  FName:=AName;
  FMenuName:=AMenuName;
  FActionClass:=AnActionClass;
end;

{ TActionList }

procedure TActionFactory.UpdateLCLActionList;
begin
  //
end;

function TActionFactory.GetLCLActionList: TActionList;
begin

end;

function TActionFactory.FindName(AName: string): TActionRegistration;
var
  i: Integer;
begin
  for i := 0 to FRegistrationList.Count -1 do
    if FRegistrationList.Items[i].Name=AName then
      begin
      Result := FRegistrationList.Items[i];
      Exit;
      end;
  Result := Nil;
end;

function TActionFactory.GetActionCount: Integer;
begin
  Result := FRegistrationList.Count;
end;

function TActionFactory.GetRegisteredAction(i: Integer): TActionRegistration;
begin
  Result := FRegistrationList[i];
end;

constructor TActionFactory.Create;
begin
  FRegistrationList := TActionRegistrationList.Create;
  FRunningActionList := TCustActionList.Create;
  FActionList := TActionList.Create(nil);
  FRemoveActionList := TCustActionList.Create;
end;

destructor TActionFactory.Destroy;
begin
  FRegistrationList.Free;
  FRunningActionList.Free;
  FActionList.Free;
  FRemoveActionList.Free;
  inherited Destroy;
end;

procedure TActionFactory.RegisterAction(AName, ADescription, AMenuName: string; AnActionClass: TActionExecutorClass);
var
  Registration: TActionRegistration;
  Act: TLazarusCodeAction;
  Actt: TContainedAction;
begin
  Registration := TActionRegistration.Create(AName, ADescription, AMenuName, AnActionClass);
  FRegistrationList.Add(Registration);
  Act := TLazarusCodeAction.Create(FActionList, AnActionClass);
  Act.Name:=AName;
  Act.Category:=AMenuName;
  Act.Caption:=ADescription;
  Act.ActionList:=FActionList;
end;

procedure TActionFactory.CleanupOldActions;
begin
  FRemoveActionList.Free;
  FRemoveActionList := TCustActionList.Create;
end;

procedure TActionFactory.RunAction(AName: string; AParams: TObject; ACallback: TCallback; ACallerData: PtrInt);
var
  Registration: TActionRegistration;
  Action: TActionExecutor;
  IsFinished: Boolean;
  Act: TContainedAction;
begin
  //Registration := FindName(AName);
  //Action := Registration.FActionClass.Create(AParams);
  //try
  //  (Action as ICallbackDispatcher).RegisterCallback(ACallback, ACallerData);
  //  FRunningActionList.Add(Action);
  //  Action.Execute;
  //  if (Action.State in [TActionExecutorState.Failed, TActionExecutorState.Success]) and not Action.Destroying then
  //    begin
  //    Action.FDestroying:=True;
  //    FRunningActionList.Remove(Action);
  //    end;
  //  Action := nil;
  //finally
  //  Action.Free;
  //end;
  Act:=FActionList.ActionByName(AName);
  if not Assigned(Act) then
    raise Exception.CreateFmt('Action named [%s] not found', [AName]);
  if (AParams <> nil) or (ACallback <> nil) then
    (Act as TLazarusCodeAction).SetTempActionData(AParams, ACallback, ACallerData);
  try
    //FForm.ExecuteAction(Act);
    //FActionList.ExecuteAction(Act);
    Act.Execute;
//    CustomApplication.ExecuteAction(Act);
  finally
    (Act as TLazarusCodeAction).ClearTempActionData;
  end;
end;

procedure TActionFactory.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
var
  i: Integer;
  ActionFinished: Boolean;
  //RemoveActionList: TCustActionList;
  Action: TActionExecutor;
begin
  //RemoveActionList:=Nil;
  //try
    Finished := False;
    for i := FRunningActionList.Count-1 downto 0 do
      begin
      Action := FRunningActionList[i];
      Action.HandleEvent(AnEvent, Finished);
      //if (Action.State in [TActionExecutorState.Failed, TActionExecutorState.Success]) and not Action.Destroying then
      //  FRunningActionList.Delete(i);
      //if (Action.State in [TActionExecutorState.Failed, TActionExecutorState.Success]) and not Action.Destroying then
      //  begin
      //  Action.FDestroying:=True;
      //  //if not Assigned(RemoveActionList) then
      //  //  RemoveActionList := TCustActionList.Create;
      //  FRunningActionList.Extract(Action);
      //  FRemoveActionList.Add(Action);
      //  end;
      if Finished then
        Break;
      end;

    //if Assigned(RemoveActionList) then for i := 0 to RemoveActionList.Count -1 do
    //  begin
    //  // The objects are freed when the RemoveActionList is freed.
    //  FRunningActionList.Extract(RemoveActionList.Items[i]);
    //  end;
  //finally
  //  RemoveActionList.Free;
  //end;
end;

{ TActionParams }

constructor TActionParams.Create(AnUniqueId: string);
begin
  FUniqueId:=AnUniqueId;
end;

{ TLazarusCodeAction }

procedure TLazarusCodeAction.DoOnExecute(Sender: TObject);
var
  Runner: TActionExecutor;
begin
  Runner := FActionExecutorClass.Create(Params);
  try
    (Runner as ICallbackDispatcher).RegisterCallback(Callback, CallerData);
    TGlobalActionFactory.Instance.FRunningActionList.Add(Runner);
    //FRunningActionList.Add(Action);
    Runner.Execute;
    //if (Action.State in [TActionExecutorState.Failed, TActionExecutorState.Success]) and not Action.Destroying then
    //  begin
    //  Action.FDestroying:=True;
    //  FRunningActionList.Remove(Action);
    //  end;
    Runner := nil;
  finally
    Runner.Free;
  end;
end;

procedure TLazarusCodeAction.SetTempActionData(AParams: TObject;ACallBack: TCallback; ACallerData: PtrInt);
begin
  FParams:=AParams;
  FCallback:=ACallBack;
  FCallerData:=ACallerData;
end;

procedure TLazarusCodeAction.ClearTempActionData;
begin
  FParams := nil;
  FCallback := nil;
  FCallerData := 0;
end;

constructor TLazarusCodeAction.Create(AOwner: TComponent; AnActionExecutorClass: TActionExecutorClass);
begin
  inherited Create(AOwner);
  FActionExecutorClass:=AnActionExecutorClass;
  OnExecute:=@DoOnExecute;
end;

function TLazarusCodeAction.Execute: Boolean;
var
  Registration: TActionRegistration;
  Action: TActionExecutor;
  IsFinished: Boolean;
  Act: TContainedAction;
begin
  inherited Execute;   //set onexcute laat die het afhandelen...
  //OnExecute:=;
  //Registration := FindName(AName);
  //Action := Registration.FActionClass.Create(AParams);
  //try
  //  (Action as ICallbackDispatcher).RegisterCallback(ACallback, ACallerData);
  //  FRunningActionList.Add(Action);
  //  Action.Execute;
  //  if (Action.State in [TActionExecutorState.Failed, TActionExecutorState.Success]) and not Action.Destroying then
  //    begin
  //    Action.FDestroying:=True;
  //    FRunningActionList.Remove(Action);
  //    end;
  //  Action := nil;
  //finally
  //  Action.Free;
  //end;
end;

end.

