unit Events;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  System.UITypes,
  Singleton;
type

  { TCustomEvent }

  TCustomEvent = class
  private
    FData: TObject;
    FName: string;
    FSender: string;
  public
    constructor Create(AName, ASender: string; AData: TObject = nil); virtual;
    destructor Destroy; override;
    property Name: string read FName;
    property Sender: string read FSender;
    property Data: TObject read FData;
  end;

  { TEvent }

  TEvent = class(TCustomEvent)
  public
    procedure Send;
  end;
  TEventClass = class of TEvent;

   { TMouseMoveEvent }

   TMouseMoveEvent = class(TEvent)
   private
     FCursorPos: TPoint;
     FShift: TShiftState;
   public
     property CursorPos: TPoint read FCursorPos write FCursorPos;
     property Shift: TShiftState read FShift write FShift;
   end;

  { TResponseEvent }

  TResponseEvent = class(TEvent)
  private
    FInResponseTo: string;
    FSuccesful: boolean;
  public
    property InResponseTo: string read FInResponseTo write FInResponseTo;
    property Succesful: boolean read FSuccesful write FSuccesful;
  end;

  TCustomEventHandler = procedure (AnEvent: TCustomEvent; out Finished: Boolean) of object;

  TMethodList= specialize TList<TCustomEventHandler>;

  { TCustomEventCaller }

  TCustomEventCaller = class
  private
    FEventHandler: TMethodList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure CallEvent(AnEvent: TCustomEvent; out Finished: Boolean);
    procedure AddHandler(AMethod: TCustomEventHandler);
    procedure RemoveHandler(AMethod: TCustomEventHandler);
  end;

  { TCustomEventDispatcher }

  THandlerDictionary = specialize TObjectDictionary<string, TCustomEventCaller>;

  TCustomEventDispatcher = class
  protected
    FHandlerDictionary: THandlerDictionary;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddHandler(AnEventName: string; AnEventHandler: TCustomEventHandler);
    procedure RemoveHandler(AnEventName: string; AnEventHandler: TCustomEventHandler);
  end;

  { TEventDispatcher }

  TEventHandler = procedure (AnEvent: TEvent; out Finished: Boolean) of object;

  TEventDispatcher = class(TCustomEventDispatcher)
  public
    function CreateEvent(AName, ASender: string; AnEventClass: TEventClass; AData: TObject = nil): TEvent;
    procedure SendEvent(AName, ASender: string; AData: TObject=nil); overload;
    procedure SendEvent(AnEvent: TEvent); overload;

    procedure AddEventHandler(AnEventName: string; AnEventHandler: TEventHandler);
    procedure RemoveEventHandler(AnEventName: string; AnEventHandler: TEventHandler);
  end;
  TGlobalEventDispatcher = specialize TSingleton<TEventDispatcher>;

  { TDlgResponseEvent }

  TDlgResponseEvent = class(TResponseEvent)
  private
    FDlgResult: TModalResult;
  public
    property DlgResult: TModalResult read FDlgResult write FDlgResult;
  end;

implementation

uses
  MainForm,
  Actions,
  FilesFrame,
  OpenFile,
  plugins;

{ TCustomEvent }

constructor TCustomEvent.Create(AName, ASender: string; AData: TObject);
begin
  FName:=AName;
  FSender:=ASender;
  FData:=AData;
end;

destructor TCustomEvent.Destroy;
begin
  FData.Free;
  inherited Destroy;
end;

{ TEvent }

procedure TEvent.Send;
begin
  TGlobalEventDispatcher.Instance.SendEvent(Self);
end;

{ TCustomEventCaller }

constructor TCustomEventCaller.Create;
begin
  FEventHandler := TMethodList.Create;
end;

destructor TCustomEventCaller.Destroy;
begin
  FEventHandler.Free;
  inherited Destroy;
end;

procedure TCustomEventCaller.CallEvent(AnEvent: TCustomEvent; out Finished: Boolean);
var
  i: Integer;
begin
  for i := 0 to FEventHandler.Count -1 do
    begin
    FEventHandler[i](AnEvent, Finished);
    if Finished then
      Exit;
    end;
  Finished:=False;
end;

procedure TCustomEventCaller.AddHandler(AMethod: TCustomEventHandler);
begin
  FEventHandler.Add(AMethod);
end;

procedure TCustomEventCaller.RemoveHandler(AMethod: TCustomEventHandler);
begin
  FEventHandler.Remove(AMethod);
end;

{ TEventDispatcher }

constructor TCustomEventDispatcher.Create;
begin
  FHandlerDictionary := THandlerDictionary.Create([doOwnsValues]);
end;

destructor TCustomEventDispatcher.Destroy;
begin
  FHandlerDictionary.Free;
  inherited Destroy;
end;

procedure TCustomEventDispatcher.AddHandler(AnEventName: string; AnEventHandler: TCustomEventHandler);
var
  EventCaller: TCustomEventCaller;
begin
  if not FHandlerDictionary.TryGetValue(AnEventName, EventCaller) then
    begin
    EventCaller := TCustomEventCaller.Create;
    FHandlerDictionary.Add(AnEventName, EventCaller);
    end;
  EventCaller.AddHandler(AnEventHandler);
end;

procedure TCustomEventDispatcher.RemoveHandler(AnEventName: string; AnEventHandler: TCustomEventHandler);
var
  EventCaller: TCustomEventCaller;
begin
  if FHandlerDictionary.TryGetValue(AnEventName, EventCaller) then
    begin
    EventCaller.AddHandler(AnEventHandler);
    end;
end;


function TEventDispatcher.CreateEvent(AName, ASender: string; AnEventClass: TEventClass; AData: TObject): TEvent;
begin
  Result := AnEventClass.Create(AName, ASender, AData);
end;

procedure TEventDispatcher.SendEvent(AName, ASender: string; AData: TObject);
begin
  SendEvent(CreateEvent(AName, ASender, TEvent, AData));
end;

procedure TEventDispatcher.SendEvent(AnEvent: TEvent);
var
  GlobalFileList: TOpenFileList;
  GlobalActionList: TActionFactory;
  IsFinished: Boolean;
  i: Integer;
  Handler: TCustomEventCaller;
begin
  IsFinished:=False;
  try
    //GlobalFileList := TGlobalFileList.Instance;
    //for i := 0 to GlobalFileList.Count -1 do
    //  begin
    //  GlobalFileList.Items[i].HandleRequest(ARequest, IsFinished);
    //  if IsFinished then
    //    Break;
    //  end;
    //
    if FHandlerDictionary.TryGetValue(AnEvent.Name, Handler) then
      begin
      Handler.CallEvent(AnEvent, IsFinished);
      end;

    if not IsFinished then
      MainFrm.HandleEvent(AnEvent);

    if not IsFinished then
      begin
      GlobalActionList := TGlobalActionFactory.Instance;
      GlobalActionList.HandleEvent(AnEvent, IsFinished);
      end;

    if not IsFinished then
      begin
      TGlobalPluginList.Instance.HandleEvent(AnEvent, IsFinished);
      end;

    if not IsFinished then
      begin
      TFilesFrame.Instance.HandleEvent(AnEvent, IsFinished);
      end;

  finally
    AnEvent.Free;
  end;
end;

procedure TEventDispatcher.AddEventHandler(AnEventName: string; AnEventHandler: TEventHandler);
begin
  inherited AddHandler(AnEventName, TCustomEventHandler(AnEventHandler));
end;

procedure TEventDispatcher.RemoveEventHandler(AnEventName: string; AnEventHandler: TEventHandler);
begin
  // In some cases the dispatcher is already freed (order of class-destructors is
  // unpredictable)
  if Assigned(self) then
    inherited RemoveHandler(AnEventName, TCustomEventHandler(AnEventHandler));
end;

end.

