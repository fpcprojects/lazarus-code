unit RunTaskAction;

{$mode ObjFPC}{$H+}

interface

uses
  // rtl
  Classes,
  SysUtils,
  Generics.Collections,
  fpjson,
  // Cerialization
  csJSONRttiStreamHelper,
  csModel,
  csGenericCollectionsDescriber,
  // LazUtils
  LazFileUtils,
  // LCL
  ComCtrls,
  // LazarusCode
  Requests,
  Actions,
  plugins,
  Shell,
  Tabs,
  OpenFolder;

type

  { TRunTaskAction }

  TRunTaskAction = class(TActionExecutor)
  public
    procedure Execute; override;
  end;

implementation

type

  { TTaskOptions }

  TTaskOptions = class
  private
    FCwd: string;
  published
    property Cwd: string read FCwd write FCwd;
  end;

  { TTask }

  TTask = class
  private
    FArgs: TStrings;
    FCommand: string;
    FLabel: string;
    FOptions: TTaskOptions;
    FType: string;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property &Type: string read FType write FType;
    property Command: string read FCommand write FCommand;
    property &Label: string read FLabel write FLabel;
    property Args: TStrings read FArgs;
    property Options: TTaskOptions read FOptions;
  end;
  TTaskList = specialize TObjectList<TTask>;

  { TTasks }

  TTasks = class
  private
    FTasks: TTaskList;
    FVersion: string;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property Version: string read FVersion write FVersion;
    property Tasks: TTaskList read FTasks;
  end;

constructor TTasks.Create;
begin
  FTasks := TTaskList.Create;
end;

destructor TTasks.Destroy;
begin
  FTasks.Free;
  inherited Destroy;
end;

constructor TTask.Create;
begin
  FArgs:=TStringList.Create;
  FOptions:=TTaskOptions.Create;
end;

destructor TTask.Destroy;
begin
  FArgs.Free;
  FOptions.Free;
  inherited Destroy;
end;

{ TRunTaskAction }

procedure TRunTaskAction.Execute;
var
  Folder: String;
  TaskConfigurationFilename: RawByteString;
  Tasks: TTasks;
  FS: TStringStream;
  RunProcessData: TRunProcessData;
  Serializer: TJSONRttiStreamClass;
  Task: TTask;
begin
  Folder := ExpandFileNameUTF8(TGlobalOpenFolder.Instance.OpenFolderName);
  TaskConfigurationFilename := ConcatPaths([Folder, '.vscode', 'tasks.json']);
  if FileExists(TaskConfigurationFilename) then
    begin
    FS := TStringStream.Create();
    try
      FS.LoadFromFile(TaskConfigurationFilename);
      Tasks := TTasks.Create;
      try
        Serializer := TJSONRttiStreamClass.Create;
        try
          Serializer.Describer.Flags:=[tcsdfCreateClassInstances];
          Serializer.Describer.DefaultImportNameStyle:=tcsinsLowerCaseFirstChar;
          Serializer.JSONStringToObject(FS.DataString, Tasks);
        finally
          Serializer.Free;
        end;

        if Tasks.Tasks.Count > 0 then
          begin
          Task := Tasks.Tasks[0];
          RunProcessData := TRunProcessData.Create(Task.Command);
          RunProcessData.Parameters.Assign(Task.Args);
          RunProcessData.Cwd:=Task.Options.Cwd;
          TGlobalRequestDispatcher.Instance.SendRequest('RunProcess', 'OpenTerminalAction', RunProcessData);
          end;
      finally
        Tasks.Free;
      end;
    finally
      FS.Free;
    end;
    end;
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('RunTask', 'Run task', 'Tasks', TRunTaskAction);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(specialize TcsGenericCollectionTListObjectDescriber<TTask>, 1);
end.

