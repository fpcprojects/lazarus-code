unit CloseAllAction;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Actions,
  Events,
  LCLType,
  CloseFileAction,
  OpenFile,
  System.UITypes,
  Requests,
  Shortcuts, Callbacks;

type

  { TCloseFileAction }

  TCloseFileAction = class(TActionExecutor, ICallbackListener)
  private
    FCallbackListener: TCallbackListener;
    FOutstandingTasksUniqueIdarr: array of Integer;
  private
    procedure OnCloseFileCompleted(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
    property CallbackListener: TCallbackListener read FCallbackListener implements ICallbackListener;
  public
    constructor Create(AParams: TObject); override;
    destructor Destroy; override;
    procedure Execute; override;
  end;


implementation

{ TCloseFileAction }

procedure TCloseFileAction.OnCloseFileCompleted(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
var
  i: Integer;
begin
  for i := High(FOutstandingTasksUniqueIdarr) downto 0 do
    begin
    if (Sender as TActionExecutor).State = Failed then
      SetFailed;
    if FOutstandingTasksUniqueIdarr[i] = CallerData then
      Delete(FOutstandingTasksUniqueIdarr, i, 1);
    end;
  if (Length(FOutstandingTasksUniqueIdarr) = 0) and (State<>Failed) then
    SetSuccess;
  ForgetMe:=True;
end;

constructor TCloseFileAction.Create(AParams: TObject);
begin
  inherited Create(AParams);
  FCallbackListener := TCallbackListener.Create;
end;

destructor TCloseFileAction.Destroy;
begin
  FCallbackListener.Free;
  inherited Destroy;
end;

procedure TCloseFileAction.Execute;
var
  Data: TCloseFileParams;
  OpnFiles: TOpenFileList;
  i: Integer;
begin
  OpnFiles := TGlobalFileList.Instance;
  if OpnFiles.Count=0 then
    begin
    SetSuccess;
    Exit;
    end;
  // We have to add all the files to FOutstandingTasksUniqueIdarr before we
  // start closing them. Or else we could get into the situation that in
  // OnCloseFileCompleted it is assumed that all files are already closed, which
  // may not be the case.
  for i := 0 to OpnFiles.Count-1 do
    System.Insert(OpnFiles.Items[i].UniqueInt, FOutstandingTasksUniqueIdarr, 0);
  for i := High(FOutstandingTasksUniqueIdarr) downto 0 do
    begin
    Data := TCloseFileParams.Create;
    Data.UniqueInt:=FOutstandingTasksUniqueIdarr[i];
    TGlobalActionFactory.Instance.RunAction('CloseFile', Data, @OnCloseFileCompleted, Data.UniqueInt);
    end;
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('CloseAllFiles', 'Close all', 'File', TCloseFileAction);
end.


