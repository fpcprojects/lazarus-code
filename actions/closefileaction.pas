unit CloseFileAction;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Actions,
  Events,
  LCLType,
  OpenFile,
  System.UITypes,
  Requests,
  Shortcuts;

type

  { TCloseFileParams }

  TCloseFileParams = class
  private
    FUniqueInt: Integer;
  public
    property UniqueInt: Integer read FUniqueInt write FUniqueInt;
  end;

  { TCloseFileAction }

  TCloseFileAction = class(TActionExecutor)
  private type
    TCloseFileActionState = (Start, QuerySaveChanges, Saving, Closing);
  private
    FCloseState: TCloseFileActionState;
    FSendId: string;
  public
    procedure Execute; override;
    procedure HandleEvent(AnEvent: TEvent; out EventFinished: Boolean); override;
  end;


implementation

{ TCloseFileAction }

procedure TCloseFileAction.Execute;
var
  OpnFile: TOpenFile;
begin
  if Assigned(Params) then
    begin
    OpnFile := TGlobalFileList.Instance.FindByUniqueInt((Params as TCloseFileParams).UniqueInt);
    if not Assigned(OpnFile) then
      begin
      SetSuccess;
      Exit;
      end;
    end
  else
    OpnFile := TGlobalFileList.Instance.GetFocusedFile;
  if Assigned(OpnFile) then
    begin
    FSendId := OpnFile.UniqueId;
    if OpnFile.Modified then
      begin
      FCloseState:=QuerySaveChanges;
      TGlobalRequestDispatcher.Instance.SendRequest('SaveFileOrCancel', FSendId);
      end
    else
      begin
      FCloseState:=Closing;
      TGlobalRequestDispatcher.Instance.SendRequest('CloseWindow', FSendId);
      end;
    end;
end;

procedure TCloseFileAction.HandleEvent(AnEvent: TEvent; out EventFinished: Boolean);
var
  ResponseEvent: TResponseEvent;
  OpnFile: TOpenFile;
begin
  EventFinished:=false;
  if (AnEvent is TResponseEvent) and (TResponseEvent(AnEvent).InResponseTo=FSendId) then
    begin
    ResponseEvent := TResponseEvent(AnEvent);
    EventFinished:=True;

    case FCloseState of
      QuerySaveChanges:
        begin
        if not ResponseEvent.Succesful then
          begin
          SetFailed;
          Exit;
          end;

        if AnEvent.Name='DlgResult' then
          case (AnEvent as TDlgResponseEvent).DlgResult of
            mrYes:
              begin
              FCloseState:=Saving;
              OpnFile:= TGlobalFileList.Instance.FindByUniqueId(FSendId);
              if OpnFile.Modified then
                OpnFile.SaveToFile();
              FCloseState:=Closing;
              TGlobalRequestDispatcher.Instance.SendRequest('CloseWindow', FSendId);
              end;
            mrNo:
              begin
              FCloseState:=Closing;
              TGlobalRequestDispatcher.Instance.SendRequest('CloseWindow', FSendId);
              end
            else
              begin
              SetFailed;
              Exit;
              end;
          end;
        end;
      Closing:
        begin
        if ResponseEvent.Succesful then
          begin
          OpnFile:= TGlobalFileList.Instance.FindByUniqueId(FSendId);
          if OpnFile.IsShallow then
            TGlobalFileList.Instance.ResetShallow;
          TGlobalEventDispatcher.Instance.SendEvent('ClosedFile', OpnFile.UniqueId);
          TGlobalEventDispatcher.Instance.SendEvent('ClosedWindow', OpnFile.UniqueId);
          TGlobalFileList.Instance.Remove(OpnFile);
          SetSuccess;
          end
        else
          SetFailed;
        end;
    end;
    end;

  if AnEvent is TResponseEvent then
    begin
    ResponseEvent := TResponseEvent(AnEvent);
    if ResponseEvent.InResponseTo=FSendId then
      begin
      end;
    end;
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('CloseFile', 'Close window', 'File', TCloseFileAction);
  TGlobalShortcuts.Instance.RegisterShortcut(scCtrl+VK_W, 'CloseFile');
end.

