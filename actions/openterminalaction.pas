unit OpenTerminalAction;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Actions,
  plugins,
  Shell,
  Tabs,
  ComCtrls,
  // LazarusCode
  Requests;

type

  { TOpenTerminalAction }

  TOpenTerminalAction = class(TActionExecutor)
  public
    procedure Execute; override;
  end;

implementation

{ TCloseFileAction }

procedure TOpenTerminalAction.Execute;
var
  ProcParams: TStringList;
  RunProcessData: TRunProcessData;
begin
  RunProcessData := TRunProcessData.Create('/bin/bash');
  RunProcessData.Parameters.Add('-i');
  TGlobalRequestDispatcher.Instance.SendRequest('RunProcess', 'OpenTerminalAction', RunProcessData);
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('OpenTerminal', 'Open terminal', 'File', TOpenTerminalAction);
end.

