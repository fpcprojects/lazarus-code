unit OpenFolderAction;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Events,
  Requests,
  Actions,
  OpenFolder,
  Shortcuts;

type

  { TOpenFolderParams }

  TOpenFolderParams = class
  private
    FFullPathName: string;
  public
    constructor Create(AFullPathName: string);
    property FullPathName: string read FFullPathName write FFullPathName;
  end;

  { TOpenFolderAction }

  TOpenFolderAction = class(TActionExecutor)
  private
    FSendId: string;
    procedure OnCloseAllCompleted(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
  public
    procedure Execute; override;
    procedure HandleEvent(AnEvent: TEvent; out EventFinished: Boolean); override;
    destructor Destroy; override;
  end;

implementation

uses
  OpenFileAction;

{ TOpenFolderParams }

constructor TOpenFolderParams.Create(AFullPathName: string);
begin
  FFullPathName:=AFullPathName;
end;


{ TOpenFolderAction }

procedure TOpenFolderAction.OnCloseAllCompleted(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
begin
  if Assigned(Params) then
    begin
    TGlobalOpenFolder.Instance.SelectFolder((Params as TOpenFolderParams).FullPathName);
    ForgetMe:=True;
    SetSuccess;
    end
  else
    begin
    TGlobalRequestDispatcher.Instance.SendRequest('OpenFolderDialog', FSendId);
    ForgetMe:=False;
    end;
end;

procedure TOpenFolderAction.Execute;
begin
  FSendId := 'OpenFolder' + IntToStr(Random(999999));
  TGlobalActionFactory.Instance.RunAction('CloseAllFiles', nil, @OnCloseAllCompleted);
end;

procedure TOpenFolderAction.HandleEvent(AnEvent: TEvent; out EventFinished: Boolean);
var
  OpenFolderEvent: TOpenFileSelectedEvent;
  //OpnFile: TOpenFile;
begin
  EventFinished:=false;

  if (AnEvent is TOpenFileSelectedEvent) and (TOpenFileSelectedEvent(AnEvent).InResponseTo=FSendId) then
    begin
    OpenFolderEvent := TOpenFileSelectedEvent(AnEvent);
    if OpenFolderEvent.InResponseTo=FSendId then
      begin
      if OpenFolderEvent.Succesful then
        begin
        TGlobalOpenFolder.Instance.SelectFolder(OpenFolderEvent.FileName);
        //OpnFile:= TGlobalFileList.Instance.FindByFilename(OpenFileEvent.FileName);
        //if Assigned(OpnFile) then
        //  begin
        //  TGlobalRequestDispatcher.Instance.SendRequest('SetFocus', OpnFile.UniqueId);
        //  end
        //else
        //  begin
        //  OpnFile:=TGlobalFileList.Instance.NewFile;
        //  OpnFile.LoadFromFile(OpenFileEvent.FileName);
        //  end;
        SetSuccess;
        end
      else
        SetFailed;
      end;
    end;
end;

destructor TOpenFolderAction.Destroy;
begin
  inherited Destroy;
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('OpenFolder', 'Open folder', 'File', TOpenFolderAction);
end.

