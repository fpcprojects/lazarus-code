unit LanguageServerAction;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Actions,
  Events,
  LCLType,
  CloseFileAction,
  OpenFile,
  System.UITypes,
  Requests,
  LangServer,
  Shortcuts, Callbacks;

type

  { TLanguageServerAction }

  TLanguageServerAction = class(TActionExecutor)
  public
    constructor Create(AParams: TObject); override;
    destructor Destroy; override;
    procedure Execute; override;
  end;


implementation

{ TLanguageServerAction }

constructor TLanguageServerAction.Create(AParams: TObject);
begin
  inherited Create(AParams);
end;

destructor TLanguageServerAction.Destroy;
begin
  inherited Destroy;
end;

procedure TLanguageServerAction.Execute;
begin
end;

initialization
  //TGlobalActionFactory.Instance.RegisterAction('CallLanguageServer', 'Language Server', 'File', TLanguageServerAction);
end.


