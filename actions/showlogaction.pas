unit ShowLogAction;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Actions,
  // LazarusCode
  Plugins,
  Requests;

type

  { TOpenTerminalAction }

  TShowLogAction = class(TActionExecutor)
  public
    procedure Execute; override;
  end;

implementation

{ TShowLogAction }

procedure TShowLogAction.Execute;
begin
  TGlobalRequestDispatcher.Instance.SendRequest('ShowLogs', 'ShowLogAction');
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('ShowLog', 'Show logs', 'File', TShowLogAction);
end.
