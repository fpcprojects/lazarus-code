unit OpenFileAction;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Actions,
  Events,
  LCLType,
  Shortcuts,
  OpenFile,
  Requests;

type

  { TOpenFileParams }

  TOpenFileParams = class
  private
    FFullFileName: string;
    FShallow: Boolean;
  public
    constructor Create(AFillFileName: string; AShallow: Boolean);
    property Shallow: Boolean read FShallow write FShallow;
    property FullFileName: string read FFullFileName write FFullFileName;
  end;

  { TOpenFileAction }

  TOpenFileAction = class(TActionExecutor)
  private
    FSendId: string;
  protected
    procedure OpenFile(FullFileName: string; Shallow: Boolean);
  public
    procedure Execute; override;
    procedure HandleEvent(AnEvent: TEvent; out EventFinished: Boolean); override;
  end;

  { TOpenFileSelectedEvent }

  TOpenFileSelectedEvent = class(TResponseEvent)
  private
    FFileName: string;
  public
    property FileName: string read FFileName write FFileName;
  end;


implementation

{ TOpenFileParams }

constructor TOpenFileParams.Create(AFillFileName: string; AShallow: Boolean);
begin
  FFullFileName:=AFillFileName;
  FShallow:=AShallow;
end;

{ TOpenFileAction }

procedure TOpenFileAction.OpenFile(FullFileName: string; Shallow: Boolean);
var
  OpnFile: TOpenFile;
begin
  if FullFileName='' then
    raise Exception.Create('Cannot open a file without a name');
  if not FileExists(FullFileName) then
    raise Exception.CreateFmt('Cannot open file: file not found [%s]', [FullFileName]);
  OpnFile:= TGlobalFileList.Instance.FindByFilename(FullFileName);
  if not Assigned(OpnFile) then
    begin
    if Shallow then
      begin
      OpnFile := TGlobalFileList.Instance.ObtainShallowFile;
      TGlobalEventDispatcher.Instance.SendEvent('ClosedFile', OpnFile.UniqueId);
      end
    else
      OpnFile:=TGlobalFileList.Instance.NewFile;
    OpnFile.LoadFromFile(FullFileName);
    end;
  TGlobalRequestDispatcher.Instance.SendRequest('SetFocus', OpnFile.UniqueId);
  SetSuccess;
end;

procedure TOpenFileAction.Execute;
begin
  if Assigned(Params) then
    begin
    OpenFile((Params as TOpenFileParams).FullFileName, (Params as TOpenFileParams).Shallow);
    end
  else
    begin
    FSendId := 'OpenFileAct'+IntToStr(Random(999999));
    TGlobalRequestDispatcher.Instance.SendRequest('OpenFileDialog', FSendId);
    end;
end;

procedure TOpenFileAction.HandleEvent(AnEvent: TEvent; out EventFinished: Boolean);
var
  OpenFileEvent: TOpenFileSelectedEvent;
begin
  EventFinished:=false;

  if AnEvent is TOpenFileSelectedEvent then
    begin
    OpenFileEvent := TOpenFileSelectedEvent(AnEvent);
    if OpenFileEvent.InResponseTo=FSendId then
      begin
      if OpenFileEvent.Succesful then
        begin
        OpenFile(OpenFileEvent.FileName, False);
        end
      else
        SetFailed;
      end;
    end;
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('OpenFile', 'Open file', 'File', TOpenFileAction);
  TGlobalShortcuts.Instance.RegisterShortcut(scCtrl+VK_O, 'OpenFile');
end.

