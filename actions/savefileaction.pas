unit SaveFileAction;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  LCLType,
  Actions,
  Events,
  OpenFile,
  Shortcuts;

type

  { TSaveFileAction }

  TSaveFileAction = class(TActionExecutor)
  public
    procedure Execute; override;
  end;

implementation

{ TSaveFileAction }

procedure TSaveFileAction.Execute;
var
  OpnFile: TOpenFile;
begin
  OpnFile := TGlobalFileList.Instance.GetFocusedFile;
  if OpnFile.Modified then
    OpnFile.SaveToFile();
  SetSuccess;
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('SaveFile', 'Save file', 'File', TSaveFileAction);
  TGlobalShortcuts.Instance.RegisterShortcut(scCtrl+VK_S, 'SaveFile');
end.

