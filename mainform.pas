unit MainForm;

// https://remixicon.com/

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Controls,
  FilesFrame,
  ffi.manager,
  AnchorDocking,
  Graphics,
  OpenFile,
  LangServer,
  Dialogs,
  ExtCtrls,
  ComCtrls,
  Menus,
  ActnList,
  LCLType, StdCtrls,
  AnchorDockPanel,
  CloseFileAction,
  SynEditTypes,
  SynEdit, synhighlighterunixshellscript, ExtendedNotebook, laz.VirtualTrees,
  Events,
  ChangesSyneditPlugin,
  Requests,
  Shortcuts,
  Actions,
  Shell,
  SynEditKeyCmds, SynCompletion, SynHighlighterPo,
  Tabs;

type

  { TMainFrm }

  TMainFrm = class(TForm)
    ActionList: TActionList;
    ExtendedNotebook: TExtendedNotebook;
    ImageList: TImageList;
    FileMenu: TMenuItem;
    EditMenu: TMenuItem;
    OpenDialog: TOpenDialog;
    MainMenu: TMainMenu;
    LeftPanel: TPanel;
    PageControl1: TPageControl;
    PanelRight: TPanel;
    SelectDirectoryDialog: TSelectDirectoryDialog;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    SynUNIXShellScriptSyn1: TSynUNIXShellScriptSyn;
    ToolBar1: TToolBar;
    ExplorerTB: TToolButton;
    procedure ExplorerTBClick(Sender: TObject);
    procedure ExtendedNotebookChange(Sender: TObject);
    procedure ExtendedNotebookCloseTabClicked(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenFileActExecute(Sender: TObject);
    procedure PageControl1CloseTabClicked(Sender: TObject);
    procedure TabControlChange(Sender: TObject);
    procedure ToolBar1Click(Sender: TObject);
    procedure OpenFile(AFileName: string);
  private
    FFocusedUniqueInt: Integer;
    FCurrentFrame: TFrame;
    FCloseQueryRandomId: PtrInt;
    FInClose: Boolean;
    FStarted: Boolean;
    procedure DoOnIdle(Sender: TObject; var Done: Boolean);
    procedure DoSynEditClick(Sender: TObject);
    procedure DoSynEditMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure OnCloseQueryCallback(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
    procedure OnRegisteredActionClick(Sender: TObject);
    function FindTabSheet(AnUniqueId: Integer): TTabSheet;
    procedure SEStatusChange(Sender: TObject; Changes: TSynStatusChanges);
  public
    procedure HandleRequest(ARequest: TRequest; out Finished: Boolean);
    procedure HandleEvent(AnEvent: TEvent);
  end;

var
  MainFrm: TMainFrm;

implementation

uses
  OpenFileAction,
  plugins;
{$R *.lfm}

{ TMainFrm }

procedure TMainFrm.ToolBar1Click(Sender: TObject);
begin

end;

procedure TMainFrm.OpenFile(AFileName: string);
var
  OpnFile: TOpenFile;
begin
  //OpnFile:=TGlobalFileList.Instance.NewFile;
  //OpnFile.LoadFromFile(AFileName);
  //ExtendedNotebook.TabIndex := ExtendedNotebook.Tabs.Add(OpnFile.Filename);
  //OpnFile.CoupledPageIndex:=TabControl.TabToPageIndex(TabControl.TabIndex);
  //TabControlChange(nil);
end;

procedure TMainFrm.OnRegisteredActionClick(Sender: TObject);
var
  ActName: String;
begin
  ActName := TGlobalActionFactory.Instance.RegisteredAction[(Sender as TMenuItem).Tag].Name;

  TGlobalActionFactory.Instance.RunAction(ActName);
end;

procedure TMainFrm.OnCloseQueryCallback(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
begin
  ForgetMe := false; // (sender as TActionExecutor).State in [Failed, Success];
  if ((sender as TActionExecutor).State = Success) and (CallerData=FCloseQueryRandomId) then
    begin
    FInClose := True;
    Self.Close;
    FInClose := False;
    end;
end;

procedure TMainFrm.DoOnIdle(Sender: TObject; var Done: Boolean);
begin
  TGlobalActionFactory.Instance.CleanupOldActions;
end;

procedure TMainFrm.DoSynEditClick(Sender: TObject);
begin
  TGlobalEventDispatcher.Instance.SendEvent(TEvent.Create('MouseClick', 'SynEdit'));
end;

procedure TMainFrm.DoSynEditMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  MouseEvent: TMouseMoveEvent;
begin
  MouseEvent := TGlobalEventDispatcher.Instance.CreateEvent('MouseMove', 'SynEdit', TMouseMoveEvent) as TMouseMoveEvent;
  MouseEvent.CursorPos := Point(X, Y);
  MouseEvent.Shift := Shift;
  TGlobalEventDispatcher.Instance.SendEvent(MouseEvent);
end;

function TMainFrm.FindTabSheet(AnUniqueId: Integer): TTabSheet;
var
  i: Integer;
begin
  for i := 0 to ExtendedNotebook.PageCount -1 do
    begin
    if ExtendedNotebook.Pages[i].Tag=AnUniqueId then
      begin
      Result := ExtendedNotebook.Pages[i];
      Exit;
      end;
    end;
  Result := Nil;
end;

procedure TMainFrm.SEStatusChange(Sender: TObject; Changes: TSynStatusChanges);
var
  OpnFile: TOpenFile;
begin
  if scModified in Changes then
    begin
    OpnFile := TGlobalFileList.Instance.FindByUniqueInt((Sender as TControl).Parent.Tag);
    OpnFile.SetModified;
    end;
end;

procedure TMainFrm.HandleRequest(ARequest: TRequest; out Finished: Boolean);
var
  EventDispatcher: TEventDispatcher;
  ResponseFileEvent: TOpenFileSelectedEvent;
  ResponseEvent: TResponseEvent;
  OpnFile: TOpenFile;
  TS: TTabSheet;
  DlgResponse: TModalResult;
  DlgResponseEvent: TDlgResponseEvent;
begin
  EventDispatcher := TGlobalEventDispatcher.Instance;

  if ARequest.Name='OpenFileDialog' then
    begin
    ARequest.Handle;
    if OpenDialog.Execute then
      begin
      ResponseFileEvent := EventDispatcher.CreateEvent('FileSelected', 'Main', TOpenFileSelectedEvent) as TOpenFileSelectedEvent;
      ResponseFileEvent.InResponseTo:=ARequest.Sender;

      if FileExists(OpenDialog.FileName) then
        begin
        ResponseFileEvent.Succesful:=True;
        ResponseFileEvent.FileName:=OpenDialog.FileName;
        end
      else
        begin
        ResponseFileEvent.Succesful:=False;
        end;
      EventDispatcher.SendEvent(ResponseFileEvent);
      end;
    ARequest.SetHandled;
    end
  else if ARequest.Name='OpenFolderDialog' then
    begin
    ARequest.Handle;
    if SelectDirectoryDialog.Execute then
      begin
      ResponseFileEvent := EventDispatcher.CreateEvent('FolderSelected', 'Main', TOpenFileSelectedEvent) as TOpenFileSelectedEvent;
      ResponseFileEvent.InResponseTo:=ARequest.Sender;

      if DirectoryExists(SelectDirectoryDialog.FileName) then
        begin
        ResponseFileEvent.Succesful:=True;
        ResponseFileEvent.FileName:=SelectDirectoryDialog.FileName;
        end
      else
        begin
        ResponseFileEvent.Succesful:=False;
        end;
      EventDispatcher.SendEvent(ResponseFileEvent);
      end;
    ARequest.SetHandled;
    end
  else if ARequest.Name='SetFocus' then
    begin
    OpnFile := TGlobalFileList.Instance.FindByUniqueId(ARequest.Sender);
    if Assigned(OpnFile) then
      begin
      TS:=FindTabSheet(OpnFile.UniqueInt);
      if Assigned(TS) then
        ExtendedNotebook.TabIndex:=TS.TabIndex;
      TGlobalFileList.Instance.SetFocusedFile(OpnFile.UniqueId);
      end;
    end
  else if ARequest.Name='CloseWindow' then
    begin
    OpnFile := TGlobalFileList.Instance.FindByUniqueId(ARequest.Sender);
    if Assigned(OpnFile) then
      begin
      TS:=FindTabSheet(OpnFile.UniqueInt);
      TS.Free;
      OpnFile.SynEdit:=nil;
      if ExtendedNotebook.TabIndex>-1 then
        begin
        OpnFile := TGlobalFileList.Instance.FindByUniqueInt(ExtendedNotebook.Pages[ExtendedNotebook.TabIndex].Tag);
        TGlobalFileList.Instance.SetFocusedFile(OpnFile.UniqueId);
        end;
      ResponseEvent := EventDispatcher.CreateEvent('WindowClosed', 'Main', TResponseEvent) as TResponseEvent;
      ResponseEvent.InResponseTo:=ARequest.Sender;
      ResponseEvent.Succesful:=True;
      EventDispatcher.SendEvent(ResponseEvent);
      end;
    end
  else if ARequest.Name='SaveFileOrCancel' then
    begin
    OpnFile := TGlobalFileList.Instance.FindByUniqueId(ARequest.Sender);
    if Assigned(OpnFile) then
      begin
      DlgResponse := QuestionDlg('File is modified', 'Do you want to save the changes you made to ['+ExtractFileName(OpnFile.Filename)+']?', mtConfirmation, [mrYes, mrNo, mrCancel], 0);
      DlgResponseEvent := EventDispatcher.CreateEvent('DlgResult', 'Main', TDlgResponseEvent) as TDlgResponseEvent;
      DlgResponseEvent.InResponseTo:=ARequest.Sender;
      DlgResponseEvent.DlgResult:=DlgResponse;
      DlgResponseEvent.Succesful:=True;
      EventDispatcher.SendEvent(DlgResponseEvent);
      end;
    end;
end;

procedure TMainFrm.HandleEvent(AnEvent: TEvent);
var
  OpnFile: TOpenFile;
  TS: TTabSheet;
  SE: TSynEdit;
  FChangesPlugin: TChangesSyneditPlugin;
begin
  if AnEvent.Name='NewFile' then
    begin
    OpnFile:=TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
    if Assigned(OpnFile) then
      begin
      TS := ExtendedNotebook.AddTabSheet;
      TS.Caption:=OpnFile.Caption;
      //OpnFile.CoupledPageIndex:=TS.PageIndex;
      TS.Tag:=OpnFile.UniqueInt;
      SE := TSynEdit.Create(MainFrm);
      SE.OnMouseMove:=@DoSynEditMouseMove;
      SE.OnClick:=@DoSynEditClick;

      OpnFile.SynEdit:=SE;
      SE.Align:=alClient;
      TS.InsertControl(SE);
      (TS.Controls[0] as TSynEdit).RegisterStatusChangedHandler(@SEStatusChange, [scModified]);
      ExtendedNotebook.TabIndex:=TS.TabIndex;

      ExtendedNotebookChange(nil);

      TGlobalEventDispatcher.Instance.SendEvent('NewEditor', OpnFile.UniqueId, nil);
      //TabControlChange(nil);
      end;
    end
  else if AnEvent.Name='LoadedFile' then
    begin
    OpnFile:=TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
    if Assigned(OpnFile) then
      begin
      TS := FindTabSheet(OpnFile.UniqueInt);
      TS.Caption:=OpnFile.Caption;
      if OpnFile.IsShallow then
        TS.Font.Style:=[fsItalic];
      TabControlChange(nil);
      end;
    end
  else if AnEvent.Name='Unshallow' then
    begin
    OpnFile:=TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
    if Assigned(OpnFile) then
      begin
      TS := FindTabSheet(OpnFile.UniqueInt);
      if Assigned(TS) then
        TS.Font.Style:=[];
      end;
    end;
end;

procedure TMainFrm.OpenFileActExecute(Sender: TObject);
begin
  if OpenDialog.Execute then
  begin
    if FileExists(OpenDialog.FileName) then
      OpenFile(OpenDialog.FileName)
  end;
end;

procedure TMainFrm.PageControl1CloseTabClicked(Sender: TObject);
begin
  TGlobalEventDispatcher.Instance.SendEvent('CloseTabRequested',  IntToStr((Sender as TTabSheet).Tag));
end;

procedure TMainFrm.TabControlChange(Sender: TObject);
begin
  if ExtendedNotebook.PageCount>0 then
    begin
    ExtendedNotebook.Visible:=True;
    end
  else
    begin
    ExtendedNotebook.Visible:=False;
    end;
end;

procedure TMainFrm.FormCreate(Sender: TObject);
var
  ActionFactory: TActionFactory;
  i: Integer;
  ActionClass: TActionRegistration;
  Item, MenuItem: TMenuItem;
begin
//  DockMaster.MakeDockSite(AnchorDockPanel1,[akTop],admrpChild);
//  DockMaster.MakeDockPanel(AnchorDockPanel1, admrpNone);
  Application.OnIdle := @DoOnIdle;

  ActionFactory := TGlobalActionFactory.Instance;
  for i := 0 to ActionFactory.RegisteredActionCount -1 do
    begin
    ActionClass := ActionFactory.RegisteredAction[i];
    if ActionClass.MenuName<>'' then
      begin
      Item := TMenuItem.Create(Menu);
      Item.Name := ActionClass.Name;
      Item.Caption := ActionClass.Description;
      Item.Tag:=i;
      Item.OnClick:=@OnRegisteredActionClick;
      MenuItem:=Menu.Items.Find(ActionClass.MenuName);
      if not Assigned(MenuItem) then
        begin
        MenuItem := TMenuItem.Create(Menu);
        MenuItem.Caption:=ActionClass.MenuName;
        Menu.Items.Add(MenuItem);
        end;
      Item.ShortCut:=TGlobalShortcuts.Instance.ShortcutForAction(ActionClass.Name);
      MenuItem.add(Item);
      end;

    end;
  TGlobalTabFactory.Instance.BindToPageControl(PageControl1);
end;

procedure TMainFrm.FormShow(Sender: TObject);
begin
  if not assigned(FCurrentFrame) then
    ExplorerTBClick(nil);
  if not FStarted then
    begin
    TGlobalEventDispatcher.Instance.SendEvent('Startup', 'Main');
    FStarted:=True;
    end;
end;

procedure TMainFrm.ExplorerTBClick(Sender: TObject);
begin
  DockMaster.HeaderStyle:='Lines';
  FCurrentFrame := TFilesFrame.Instance;
  FCurrentFrame.parent := LeftPanel;
  FCurrentFrame.Align:=alClient;
end;

procedure TMainFrm.ExtendedNotebookChange(Sender: TObject);
var
  Unique: Integer;
  OpnFile: TOpenFile;
begin
  Unique := ExtendedNotebook.Pages[ExtendedNotebook.TabIndex].Tag;
  if Unique<>FFocusedUniqueInt then
    begin
    OpnFile := TGlobalFileList.Instance.FindByUniqueInt(Unique);
    if Assigned(OpnFile) then
      TGlobalFileList.Instance.SetFocusedFile(OpnFile.UniqueId);
    FFocusedUniqueInt:=Unique;
    end;

end;

procedure TMainFrm.ExtendedNotebookCloseTabClicked(Sender: TObject);
var
  Data: TCloseFileParams;
begin
  Data := TCloseFileParams.Create;
  Data.UniqueInt:=(sender as TTabSheet).Tag;
  TGlobalActionFactory.Instance.RunAction('CloseFile', Data);
end;

procedure TMainFrm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if FInClose then
    CanClose:=True
  else
    begin
    FCloseQueryRandomId:=Random(999999);
    TGlobalActionFactory.Instance.RunAction('CloseAllFiles', nil, @OnCloseQueryCallback, FCloseQueryRandomId);
    CanClose:=False;
    end;
end;

end.

