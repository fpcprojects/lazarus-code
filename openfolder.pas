unit OpenFolder;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Singleton,
  events,
  Requests;

type

  { TOpenFolder }

  TOpenFolder = class
  private
    FOpenFolderName: string;
  public
    procedure SelectFolder(AFolderName: string);
    property OpenFolderName: string read FOpenFolderName;
  end;
  TGlobalOpenFolder = specialize TSingleton<TOpenFolder>;

implementation



{ TOpenFolder }

procedure TOpenFolder.SelectFolder(AFolderName: string);
begin
  FOpenFolderName:=AFolderName;
  TGlobalEventDispatcher.Instance.SendEvent('OpenFolderChanged', 'OpenFolder');
end;

end.

