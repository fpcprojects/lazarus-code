unit FilesFrame;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Events,
  Controls,
  Buttons,
  AnchorDockPanel,
  FilesExplorer,
  OpenFiles,
  AnchorDocking;

type

  { TFilesFrame }

  TFilesFrame = class(TFrame)
    AnchorDockPanel: TAnchorDockPanel;
  private
    class var FInstance: TFilesFrame;
  protected
    procedure AfterConstruction; override;

  public
    procedure Init;
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean);
    class function Instance: TFilesFrame;
    class destructor Destroy;
  end;

implementation

{$R *.lfm}

{ TFilesFrame }

procedure TFilesFrame.AfterConstruction;
begin
  inherited AfterConstruction;
  DockMaster.MakeDockPanel(AnchorDockPanel, admrpChild);

  DockMaster.MakeDockable(OpenFileForm,true,true);
  DockMaster.ManualDock(DockMaster.GetAnchorSite(OpenFileForm), AnchorDockPanel, alTop);

  DockMaster.MakeDockable(FileExplorer,true,true);
  DockMaster.ManualDock(DockMaster.GetAnchorSite(FileExplorer), AnchorDockPanel, alBottom);

  OpenFileForm.Update;
end;

procedure TFilesFrame.Init;
begin
  //DockMaster.MakeDockPanel(AnchorDockPanel, admrpChild);
end;

procedure TFilesFrame.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
begin
  OpenFileForm.HandleEvent(AnEvent, Finished);
  FilesExplorer.FileExplorer.HandleEvent(AnEvent, Finished);
end;

class function TFilesFrame.Instance: TFilesFrame;
begin
  if not Assigned(FInstance) then
    FInstance := TFilesFrame.Create(nil);
  Result := FInstance;
end;

class destructor TFilesFrame.Destroy;
begin
  FInstance.Free;
end;

end.

