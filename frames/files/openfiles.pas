unit OpenFiles;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Controls,
  Graphics,
  Events,
  Dialogs,
  GraphType,
  OpenFile,
  FileUtil,
  OpenFolder,
  Requests,
  laz.VirtualTrees;

type

  { TOpenFileForm }

  TOpenFileForm = class(TForm)
    LazVirtualStringTree: TLazVirtualStringTree;
    procedure LazVirtualStringTreeAddToSelection(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure LazVirtualStringTreeFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure LazVirtualStringTreeGetNodeDataSize(Sender: TBaseVirtualTree; var NodeDataSize: Integer);
    procedure LazVirtualStringTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: String);
    procedure LazVirtualStringTreeInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
    procedure LazVirtualStringTreePaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType);
  private
    FFocusedUniqueId: string;
    FProcessingReceivedFocus: Boolean;
    FSettingFocus: Boolean;
    function FindNode(UniqueId: string): PVirtualNode;
  public
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean);
    procedure Update;
  end;

var
  OpenFileForm: TOpenFileForm;

implementation

type
  TNodeData = record
    UniqueId: string;
  end;

{$R *.lfm}

{ TOpenFileForm }

procedure TOpenFileForm.LazVirtualStringTreeGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: String);
var
  OpnFiles: TOpenFileList;
  OpenFolderName: string;
begin
  OpnFiles := TGlobalFileList.Instance;
  if Node^.Index<(OpnFiles.Count) then
    begin
    if TextType=ttNormal then
      CellText:=ExtractFileName(OpnFiles[Node^.Index].Filename)
    else
      begin
      // Show the name of the directory
      CellText:=ExtractFileDir(OpnFiles[Node^.Index].Filename);
      OpenFolderName := TGlobalOpenFolder.Instance.OpenFolderName;
      if (OpenFolderName<>'') and (FileIsInPath(OpnFiles[Node^.Index].Filename, OpenFolderName)) then
        // When a folder has been opened, show the path relative to this folder,
        // except when the file is outside the folder.
        CellText := ExcludeTrailingPathDelimiter(ExtractFilePath(ExtractRelativePath(IncludeTrailingPathDelimiter(OpenFolderName), OpnFiles[Node^.Index].Filename)));
      end;
    end
  else
    CellText:='invalid';
end;

procedure TOpenFileForm.LazVirtualStringTreeInitNode(Sender: TBaseVirtualTree;
  ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
  OpnFiles: TOpenFileList;
  NodeData: Pointer;
begin
  OpnFiles := TGlobalFileList.Instance;
  NodeData := Sender.GetNodeData(Node);
  if Node^.Index < OpnFiles.Count then
    begin
    TNodeData(NodeData^).UniqueId := OpnFiles[Node^.Index].UniqueId;
    if TNodeData(NodeData^).UniqueId = FFocusedUniqueId then
      begin
      LazVirtualStringTree.ClearSelection;
      LazVirtualStringTree.AddToSelection(Node);
      end;
    end
  else
    TNodeData(NodeData^).UniqueId := '';
end;

procedure TOpenFileForm.LazVirtualStringTreePaintText(
  Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType);
begin
  if TextType=ttStatic then
    TargetCanvas.Font.Size:=10;
end;

function TOpenFileForm.FindNode(UniqueId: string): PVirtualNode;
var
  NodeData: Pointer;
begin
  Result := LazVirtualStringTree.GetFirst();
  if Assigned(Result) then
    begin
    NodeData := LazVirtualStringTree.GetNodeData(Result);
    while Assigned(Result) and (TNodeData(NodeData^).UniqueId<>UniqueId) do
      begin
      Result := LazVirtualStringTree.GetNext(Result);
      NodeData := LazVirtualStringTree.GetNodeData(Result);
      end;
    end;
end;

procedure TOpenFileForm.LazVirtualStringTreeFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  NodeData: Pointer;
begin
  NodeData := Sender.GetNodeData(Node);
  TNodeData(NodeData^).UniqueId := '';
end;

procedure TOpenFileForm.LazVirtualStringTreeAddToSelection(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  NodeData: Pointer;
begin
  if Assigned(Node) and not FProcessingReceivedFocus then
    begin
    if Sender.SelectedCount=0 then
      begin
      FSettingFocus:=true;
      try
        NodeData := LazVirtualStringTree.GetNodeData(Node);
        TGlobalRequestDispatcher.Instance.SendRequest('SetFocus', TNodeData(NodeData^).UniqueId);
      finally
        FSettingFocus:=False;
      end;
      end;
    end;
end;

procedure TOpenFileForm.LazVirtualStringTreeGetNodeDataSize(Sender: TBaseVirtualTree; var NodeDataSize: Integer);
begin
  NodeDataSize := SizeOf(TNodeData);
end;

procedure TOpenFileForm.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
var
  Node: PVirtualNode;
  NodeData: Pointer;
begin
  if (AnEvent.Name='NewFile') or (AnEvent.Name='LoadedFile') then
    begin
    Update;
    end
  else if (AnEvent.Name='ClosedWindow') then
    begin
    LazVirtualStringTree.BeginUpdate;

    Node := FindNode(AnEvent.Sender);
    if Assigned(Node) then
       LazVirtualStringTree.DeleteNode(Node);
    LazVirtualStringTree.RootNodeCount:=TGlobalFileList.Instance.Count;

    LazVirtualStringTree.EndUpdate;
    end
  else if (AnEvent.Name='ReceivedFocus') then
    begin
    if FSettingFocus then
      Exit;
    FProcessingReceivedFocus := True;
    try
      LazVirtualStringTree.ClearSelection;
      Node := FindNode(AnEvent.Sender);
      if Assigned(Node) then
         LazVirtualStringTree.AddToSelection(Node);
      FFocusedUniqueId:=AnEvent.Sender;
    finally
      FProcessingReceivedFocus:=False;
    end;
    end;
end;

procedure TOpenFileForm.Update;
var
  OpnFiles: TOpenFileList;
begin
  LazVirtualStringTree.BeginUpdate;
  OpnFiles := TGlobalFileList.Instance;
  LazVirtualStringTree.RootNodeCount:=OpnFiles.Count;
  LazVirtualStringTree.EndUpdate;
end;

end.

