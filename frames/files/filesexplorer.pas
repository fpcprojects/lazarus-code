unit FilesExplorer;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  Events,
  OpenFolder,
  ExtCtrls,
  ShellCtrls,
  ComCtrls,
  Actions,
  OpenFile,
  OpenFileAction;

type

  { TFileExplorer }

  TFileExplorer = class(TForm)
    ShellTreeView: TShellTreeView;
    procedure ShellTreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure ShellTreeViewDblClick(Sender: TObject);
  private

  public
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean);

  end;

var
  FileExplorer: TFileExplorer;

implementation

{$R *.lfm}

{ TFileExplorer }

procedure TFileExplorer.ShellTreeViewChange(Sender: TObject; Node: TTreeNode);
var
  Data: TOpenFileParams;
  FN: String;
begin
  // This event is also called when the selection is cleared. In that case Node
  // is nil and there is no need to open another file.
  if Assigned(Node) then
    begin
    FN := ShellTreeView.GetPathFromNode(Node);
    if DirectoryExists(FN) then
      Exit;
    Data := TOpenFileParams.Create(FN, True);
    TGlobalActionFactory.Instance.RunAction('OpenFile', Data, nil );
    end;
end;

procedure TFileExplorer.ShellTreeViewDblClick(Sender: TObject);
var
  FN: String;
begin
  if TGlobalFileList.Instance.GetFocusedFile.IsShallow then
    begin
    // A doubleclick should only unshallow, if the actual selected file
    // is shallow.
    // This is nog the case, for example, when a directory is selected
    // (Question is: should a directory be selectable at all?)
    if Assigned(ShellTreeView.Selected) then
      begin
      FN := ShellTreeView.GetPathFromNode(ShellTreeView.Selected);
      if FN = TGlobalFileList.Instance.GetFocusedFile.Filename then
        TGlobalFileList.Instance.ResetShallow;
      end;
    end;
end;

procedure TFileExplorer.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
//var
//  Node: PVirtualNode;
//  NodeData: Pointer;
var
  FocusedFile: TOpenFile;
  Node: TTreeNode;
  RelName, RelText: String;
begin
  if (AnEvent.Name='OpenFolderChanged') then
    begin
    ShellTreeView.Root:=TGlobalOpenFolder.Instance.OpenFolderName;
    ShellTreeView.Visible:=True;
    end
  //else if (AnEvent.Name='ClosedWindow') then
  //  begin
  //  LazVirtualStringTree.BeginUpdate;
  //
  //  Node := FindNode(AnEvent.Sender);
  //  if Assigned(Node) then
  //     LazVirtualStringTree.DeleteNode(Node);
  //  LazVirtualStringTree.RootNodeCount:=TGlobalFileList.Instance.Count;
  //
  //  LazVirtualStringTree.EndUpdate;
  //  end
  else if (AnEvent.Name='ReceivedFocus') then
    begin
    FocusedFile := TGlobalFileList.Instance.FindByUniqueId(AnEvent.Sender);
    if Assigned(FocusedFile) then
      begin
      RelName := ExtractRelativePath(IncludeTrailingPathDelimiter(TGlobalOpenFolder.Instance.OpenFolderName), FocusedFile.Filename);
      Node := ShellTreeView.Items.GetFirstNode;
      for RelText in RelName.Split(DirectorySeparator) do
        begin
        if Assigned(Node) then
          Node := Node.FindNode(RelText);
        end;
      if Assigned(Node) then
        ShellTreeView.Selected:=Node
      else
        ShellTreeView.ClearSelection();
      end;
    end;
end;

end.

