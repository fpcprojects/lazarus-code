unit LangServer;

{$mode ObjFPC}{$H+}{$M+}

interface

uses
  // rtl/fcl
  generics.collections,
  Classes,
  SysUtils,
  fpjsonrpc,
  fprpcrtti,
  fprpcclient,
  math,
  URIParser,
  ssockets,
  fpjson,
  jsonparser,
  Contnrs,
  // Cerialization
  csJSONRttiStreamHelper,
  csModel,
  // LCL
  LazFileUtils,
  // LazarusCode
  Actions,
  Requests,
  OpenFile,
  annotations,
  HoverToolTip,
  OpenFolder,
  Events,
  plugins,
  PluginData,
  LangServerModel,
  sessions,
  ChangesSyneditPlugin,
  HoverJumpToPlugin,
  CompletionPlugin;

type

  { THoverInfoParams }

  THoverInfoParams = class
  private
    FFileId: string;
    FPosition: TPoint;
  public
    property Position: TPoint read FPosition write FPosition;
    property FileId: string read FFileId write FFileId;
  end;


  { TLanguageServerMessageEvent }

  TLanguageServerMessageEvent = class(TEvent)
  private
    FIsError: Boolean;
    FRes: TJSONData;
  public
    constructor Create(AName, ASender: string; ARes: TJSONData; AIsError: Boolean);
    destructor Destroy; override;
    property Res: TJSONData read FRes write FRes;
    property IsError: Boolean read FIsError write FIsError;
  end;

  { TLanguageServerResponseEvent }

  TLanguageServerResponseEvent = class(TLanguageServerMessageEvent)
  private
    FRequestId: string;
  public
    constructor Create(AName, ASender: string; ARequestId: string; ARes: TJSONData; AIsError: Boolean);
    property RequestId: String read FRequestId write FRequestId;
  end;

  { TLanguageServerNotificationEvent }

  TLanguageServerNotificationEvent = class(TLanguageServerMessageEvent)
  private
    FMethod: String;
  public
    constructor Create(AName, ASender: string; AMethod: string; ARes: TJSONData; AIsError: Boolean);
    property Method: String read FMethod write FMethod;
  end;

  { TLangServerClient }

  TLanguageServerInitStatus = (isNone, isProcessing, isFailed, isSucceeded, isShuttingDown, isReconnect);
  TRpcResponse = (rResponse, rNotification, rErrorResponse, rErrorNotification);

  { TLanguageServerData }

  TLanguageServerData = class
  private
    FIsMarkedAsOpen: Boolean;
  public
    // Did we send a didOpen message to the server to claim ownership?
    property IsMarkedAsOpen: Boolean read FIsMarkedAsOpen write FIsMarkedAsOpen;
  end;
  TLanguageServerDataDictionary = specialize TPluginDataDictionary<TLanguageServerData>;

  TOutstandingRequestDataDictionary = specialize TObjectDictionary<string, TRequest>;


  { TLanguageServer }

  TLanguageServer = class(TPlugin)
  private
    FInitSucceeded: Boolean;
    FInitializationStatus: TLanguageServerInitStatus;
    FInputSocket: TSocketStream;
    FOutputSocket: TSocketStream;
    FRequestId: Integer;
    FSerializer: TJSONRttiStreamClass;
    FQueue: TObjectQueue;
    FFolder: string;
    FInitializeResult: TInitializeResult;

    function GetInitializeResult: TInitializeResult;
    procedure OnHoverInfoCompleted(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
    function ParseRPCResponse(DataStr: string; out Identifier: string; out Res: TJSONData): TRpcResponse;
    procedure InitDone(Sender: TObject);
    procedure ListenDone(Sender: TObject);
    Procedure ProcessMessage(Sender : TThread; Const status : String);
    procedure InitInThread;
    procedure ListenInThread(ReportStatus : TThreadReportStatus);
  protected
    FPluginData: TLanguageServerDataDictionary;
    FOutstandingServerCallsForRequests: TOutstandingRequestDataDictionary;
    // Keep a list of files that were 'opened' during the connection phase. So
    // an open-notificatio could be sent once the connection is made.
    FFilesToOpenAfterConnect: TStringList;
    function CreateRPCRequest(const aMethodName: String; IsNotification: Boolean; ARequestId: string = ''): TJSONObject;
    function MakeServerCallAndReleaseParams(AMethod: string; ARequestId: string; AParams: TObject; AForced: Boolean= false): Integer; overload;
    function MakeServerCall(AMethod: string; ARequestId: string; AParams: TJSONData; AForced: Boolean= false): Integer; overload;
    function AwaitMessage: TBytes;
    procedure DoRequest(ARequest: TJSONObject);
    procedure FinalizeDisconnect;
    procedure NotifyDidOpenTextDocument(FileUniqueId: string);
    procedure NotifyChangeTextDocument(FileUniqueId: string; Change: TLinesEditedData);
    procedure NotifyDidCloseTextDocument(FileUniqueId: string);
    procedure HandleServerNotification(ANotification: TLanguageServerNotificationEvent);

    procedure OpentextDocumentWhenNecessary(FileUniqueId: string);
    procedure ClosetextDocumentWhenNecessary(FileUniqueId: string);

    procedure HandleLinesEdited(AnEvent: TEvent; out Finished: Boolean);
    procedure HandleLanguageIdChanged(AnEvent: TEvent; out Finished: Boolean);

    procedure HandleCompletionRequest(ARequest: TRequest; out Finished: Boolean); virtual;
    procedure HandleCompletionLangServerResponse(AResponseEvent: TLanguageServerResponseEvent; ARequest: TRequest);

    procedure PerformPositionInfoRequest(ARequest: TRequest; AInfoParams: TTextDocumentPositionParams; AMethod: string; APositionInfoData: TPositionInfoData; AnOpnFile: TOpenFile); virtual;

    procedure HandleDeclarationInfoRequest(ARequest: TRequest; out Finished: Boolean); virtual;
    procedure HandleDeclarationInfoLangServerResponse(AResponseEvent: TLanguageServerResponseEvent; ARequest: TRequest);

    procedure HandleDefinitionInfoRequest(ARequest: TRequest; out Finished: Boolean); virtual;
    procedure HandleDefinitionInfoLangServerResponse(AResponseEvent: TLanguageServerResponseEvent; ARequest: TRequest);

    property InitializeResult: TInitializeResult read GetInitializeResult;
  public
    constructor Create;
    destructor Destroy; override;
    procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean); override;
    procedure HandleRequest(ARequest: TRequest; out Finished: Boolean); override;
    function EnforceConnected: Boolean;
    procedure InitiateDisconnect;
    procedure Init;

    function DoesSupportLanguageId(ALanguageId: string): Boolean; virtual;

    class function FilenameToUri(const AFilename: string): TDocumentUri;
    class function UriToFilename(const AnUri: TDocumentUri): string;
  end;

  { THoverInfoAction }

  THoverInfoAction = class(TActionExecutor)
  private
    FHint: string;
    FRequestId: String;
    FPosition: TPoint;
  public
    procedure Execute; override;
    procedure HandleEvent(AnEvent: TEvent; out EventFinished: Boolean); override;
    property Position: TPoint read FPosition;
    property Hint: string read FHint;
  end;

  { TResponseHoverInfoEvent }

  TResponseHoverInfoEvent = class(TEvent)
  private
    FHint: string;
    FPosition: TPoint;
    FRequestId: Integer;
  public
    property RequestId: Integer read FRequestId write FRequestId;
    property Hint: string read FHint write FHint;
    property Position: TPoint read FPosition write FPosition;
  end;

  { TLangServerRequestData }

  TLangServerRequestData = class
  private
    FLanguageId: string;
    FMethod: string;
    FParams: TObject;
    FRequestId: string;
  public
    constructor Create(AMethod: string; ALanguageId: string; ARequestId: string; AParams: TObject);
    property Method: string read FMethod;
    property LanguageId: string read FLanguageId;
    property Params: TObject read FParams;
    property RequestId: string read FRequestId;
  end;

implementation

{ TLanguageServerClient }

function TLanguageServer.ParseRPCResponse(DataStr: string; out Identifier: string; out Res: TJSONData): TRpcResponse;
var
  JSObject: TJSONObject;
  IdElement: TJSONData;
begin
  JSObject := GetJSON(DataStr) as TJSONObject;
  try
    IdElement := JSObject.Find('id');
    if not Assigned(IdElement) then
      begin
      // Notification
      Result := rNotification;
      Identifier := JSObject.Get('method', '');
      Res := JSObject.Extract('params');
      if not Assigned(Res) then
        begin
        Res := JSObject.Extract('error');
        Result := rErrorNotification;
        end;
      Exit;
      end
    else if IdElement.JSONType=jtString then
      Identifier := JSObject.Get('id', '')
    else
      Identifier := IntToStr(JSObject.Get('id', -1));
    Res := JSObject.Extract('result');
    if not Assigned(Res) then
      begin
      Res := JSObject.Extract('error');
      Result := rErrorResponse;
      end
    else
      Result := rResponse;
  finally
    JSObject.Free;
  end;
end;

procedure TLanguageServer.OnHoverInfoCompleted(Sender: TObject; CallerData: PtrInt; out ForgetMe: Boolean);
var
  Action: THoverInfoAction;
begin
  Action := Sender as THoverInfoAction;
  if Action.Hint <> '' then
    TGlobalEventDispatcher.Instance.SendEvent('RequestTooltipInfoResponse', 'LanguageServer', THoverInfoRequestResponse.Create(Action.Hint, Action.Position));
end;

function TLanguageServer.GetInitializeResult: TInitializeResult;
begin
  if FInitializationStatus <> isSucceeded then
    raise Exception.create('It is not possible to retrieve the initialization-result as long as the language server is not initialized.');
  Result := FInitializeResult;
end;

procedure TLanguageServer.HandleDeclarationInfoRequest(ARequest: TRequest; out Finished: Boolean);
var
  OpnFile: TOpenFile;
  PositionInfoData: TPositionInfoData;
begin
  PositionInfoData := ARequest.Data as TPositionInfoData;

  OpnFile:=TGlobalFileList.Instance.FindByUniqueId(PositionInfoData.UniqueFileId);
  if DoesSupportLanguageId(OpnFile.LanguageId) and Assigned(OpnFile) then
    begin
    if EnforceConnected and FInitializeResult.Capabilities.DeclarationProvider then
      PerformPositionInfoRequest(ARequest, TDeclarationParams.Create, 'textDocument/declaration', PositionInfoData, OpnFile);
    end;
end;

procedure TLanguageServer.HandleDeclarationInfoLangServerResponse(AResponseEvent: TLanguageServerResponseEvent; ARequest: TRequest);
var
  j: string;
  Resp: TCompletionDataResponse;
  JD: TJSONData;
  LangRespItems: TJSONData;
  i: Integer;
begin
  //if not AResponseEvent.IsError then
  //  begin
  //  j := AResponseEvent.FRes.AsJSON;
  //  JD := AResponseEvent.FRes.FindPath('items');
  //  if Assigned(JD) and (JD.JSONType=jtArray) then
  //    begin
  //    LangRespItems := JD as TJSONArray;
  //    for i := 0 to LangRespItems.Count -1 do
  //      begin
  //      Resp := TCompletionDataResponse.Create;
  //      Resp.ResponseLabel := (LangRespItems.Items[i] as TJSONObject).Get('insertText');
  //      (ARequest.Data as TCompletionData).Response.Add(Resp);
  //      end;
  //    end;
  //  end;
  //
  ARequest.SetHandled;
end;

procedure TLanguageServer.HandleDefinitionInfoRequest(ARequest: TRequest; out Finished: Boolean);
var
  OpnFile: TOpenFile;
  PositionInfoData: TPositionInfoData;
begin
  PositionInfoData := ARequest.Data as TPositionInfoData;

  OpnFile:=TGlobalFileList.Instance.FindByUniqueId(PositionInfoData.UniqueFileId);
  if DoesSupportLanguageId(OpnFile.LanguageId) and Assigned(OpnFile) then
    begin
    if EnforceConnected and FInitializeResult.Capabilities.DefinitionProvider then
      PerformPositionInfoRequest(ARequest, TDefinitionParams.Create, 'textDocument/definition', PositionInfoData, OpnFile);
    end;
end;

procedure TLanguageServer.HandleDefinitionInfoLangServerResponse(AResponseEvent: TLanguageServerResponseEvent; ARequest: TRequest);
var
  j: string;
  Resp: TCompletionDataResponse;
  JD: TJSONData;
  LangRespItems: TJSONData;
  i: Integer;
  ll: TLocationList;
  Reference: TReference;
begin
  if not AResponseEvent.IsError then
    begin
    j := AResponseEvent.FRes.AsJSON;

    ll := TLocationList.Create;
    FSerializer.JSONToObject(AResponseEvent.FRes, ll);

    for i := 0 to ll.Count -1 do
      begin
      Reference := TReference.Create;
      Reference.StartPos.Create(ll[i].Range.Start.Character+1, ll[i].Range.Start.Line+1);
      Reference.EndPos.Create(ll[i].Range.&End.Character+1, ll[i].Range.&End.Line+1);
      Reference.Filename := UriToFilename(ll[i].Uri);
      (ARequest.Data as TPositionInfoData).Response.Add(Reference);
      end;
    end;

  ARequest.SetHandled;
end;

procedure TLanguageServer.HandleLanguageIdChanged(AnEvent: TEvent; out Finished: Boolean);
var
  EventData: TLanguageIdChangedData;
begin
  EventData := AnEvent.Data as TLanguageIdChangedData;

  // Check if the file was already opened (with the old languageid) - when this is
  // the case, close it.
  ClosetextDocumentWhenNecessary(AnEvent.Sender);

  if DoesSupportLanguageId(EventData.NewLanguageId) then
    begin
    // Notify the server that we opened the file
    EnforceConnected;

    if FInitializationStatus=isSucceeded then
      OpentextDocumentWhenNecessary(AnEvent.Sender)
    else
      begin
      // We cannot send the open notification right now, because we cannot check
      // the server capabilities as long as wer're not connected. So cache the
      // files.
      FFilesToOpenAfterConnect.Add(AnEvent.Sender);
      end;
    end;
end;

procedure TLanguageServer.HandleCompletionRequest(ARequest: TRequest; out Finished: Boolean);
var
  CompletionData: TCompletionData;
  OpnFile: TOpenFile;
  CompletionParams: TCompletionParams;
  UniqueLangServerRequestId: string;
begin
  CompletionData := ARequest.Data as TCompletionData;

  OpnFile:=TGlobalFileList.Instance.FindByUniqueId(CompletionData.UniqueFileId);
  if Assigned(OpnFile) then
    begin
    if DoesSupportLanguageId(OpnFile.LanguageId) and EnforceConnected then
      begin
      UniqueLangServerRequestId := 'TDC'+IntToStr(Random(999999));
      FOutstandingServerCallsForRequests.Add(UniqueLangServerRequestId, ARequest);
      ARequest.Handle;

      CompletionParams := TCompletionParams.Create;
      CompletionParams.Context.TriggerKind:=Invoked;
      CompletionParams.TextDocument.Uri:=FilenameToUri(OpnFile.Filename);
      CompletionParams.Position.Line:=CompletionData.SourceStart.Y -1;
      CompletionParams.Position.Character:=CompletionData.SourceStart.X -1;

      MakeServerCallAndReleaseParams('textDocument/completion', UniqueLangServerRequestId, CompletionParams);
      end;
    end;
end;

procedure TLanguageServer.HandleCompletionLangServerResponse(AResponseEvent: TLanguageServerResponseEvent; ARequest: TRequest);
var
  j: string;
  Resp: TCompletionDataResponse;
  JD: TJSONData;
  LangRespItems: TJSONData;
  i: Integer;
begin
  j := AResponseEvent.FRes.AsJSON;
  JD := AResponseEvent.FRes.FindPath('items');
  if Assigned(JD) and (JD.JSONType=jtArray) then
    begin
    LangRespItems := JD as TJSONArray;
    for i := 0 to LangRespItems.Count -1 do
      begin
      Resp := TCompletionDataResponse.Create;
      Resp.ResponseLabel := (LangRespItems.Items[i] as TJSONObject).Get('insertText');
      (ARequest.Data as TCompletionData).Response.Add(Resp);
      end;
    end;

  ARequest.SetHandled;
end;

procedure TLanguageServer.PerformPositionInfoRequest(ARequest: TRequest; AInfoParams: TTextDocumentPositionParams; AMethod: string; APositionInfoData: TPositionInfoData; AnOpnFile: TOpenFile);
var
  UniqueLangServerRequestId: string;
begin
  UniqueLangServerRequestId := 'IR'+IntToStr(Random(999999));
  FOutstandingServerCallsForRequests.Add(UniqueLangServerRequestId, ARequest);
  ARequest.Handle;

  AInfoParams.Position.Line := APositionInfoData.Position.Y-1;
  AInfoParams.Position.Character := APositionInfoData.Position.X-1;
  AInfoParams.TextDocument.Uri:=FilenameToUri(AnOpnFile.Filename);

  MakeServerCallAndReleaseParams(AMethod, UniqueLangServerRequestId, AInfoParams);
end;

procedure TLanguageServer.InitDone(Sender : TObject);
var
  Request: TJSONObject;
  i: Integer;
  OpenFiles: TOpenFileList;
begin
  if FInitSucceeded then
    begin
    FInitializationStatus := isSucceeded;

    // Send open-notifications for all files that were opened during the connect
    for i := 0 to FFilesToOpenAfterConnect.Count -1 do
      begin
      OpentextDocumentWhenNecessary(FFilesToOpenAfterConnect[i]);
      end;
    FFilesToOpenAfterConnect.Clear;

    // All server calls were queued during the initialization, send them now.
    Request := FQueue.Pop as TJSONObject;
    while Assigned(Request) do
      begin
      DoRequest(Request);
      Request.Free;
      Request := FQueue.Pop as TJSONObject;
      end;

    TThread.ExecuteInThread(@ListenInThread, @ProcessMessage, @ListenDone);
    end
  else
    begin
    FInitializationStatus := isFailed;

    // Remove any pending requests
    repeat
    Request := FQueue.Pop as TJSONObject;
    until not Assigned(Request);
    end;
end;

procedure TLanguageServer.ListenDone(Sender: TObject);
begin
  FInitializationStatus:=isNone;
end;

procedure TLanguageServer.ProcessMessage(Sender: TThread; const status: String);
var
  Identifier: String;
  Res: TJSONData;
begin
  case ParseRPCResponse(status, Identifier, Res) of
    rResponse:
      TGlobalEventDispatcher.Instance.SendEvent(TLanguageServerResponseEvent.Create('LangServerResponse', 'LangServer', Identifier, Res, False));
    rErrorResponse:
      TGlobalEventDispatcher.Instance.SendEvent(TLanguageServerResponseEvent.Create('LangServerResponse', 'LangServer', Identifier, Res, True));
    rNotification:
      TGlobalEventDispatcher.Instance.SendEvent(TLanguageServerNotificationEvent.Create('LangServerNotification', 'LangServer', Identifier, Res, False));
    rErrorNotification:
      TGlobalEventDispatcher.Instance.SendEvent(TLanguageServerNotificationEvent.Create('LangServerNotification', 'LangServer', Identifier, Res, True));
  end;
end;

Const
  CRLF = #13#10;

{ TLanguageServer }

function TLanguageServer.CreateRPCRequest(const aMethodName: String; IsNotification: Boolean; ARequestId: string): TJSONObject;
begin
  Result := TJSONObject.Create;
  try
    Result.Add('method', aMethodName);
    //Result.Add('classname', aClassName);
    Result.Add('jsonrpc','2.0');
    // In case of notification, do not send an ID
    if Not (IsNotification {and (rcoNotifications in Options)})  then
      begin
      if ARequestId='' then
        begin
        inc(FRequestID);
        Result.Add('id', FRequestID);
        end
      else
        Result.Add('id', ARequestID);
      end;
  except
    Result.Free;
    Raise;
  end;
end;

function TLanguageServer.MakeServerCallAndReleaseParams(AMethod: string; ARequestId: string; AParams: TObject; AForced: Boolean): Integer;
var
  JS: TJSONData;
begin
  JS := FSerializer.ObjectToJSON(AParams);
  try
    MakeServerCall(AMethod, ARequestId, JS, AForced);
  finally
    AParams.Free;
  end;
  AParams := nil;
end;

function TLanguageServer.AwaitMessage(): TBytes;

var
  ContentSize: Integer;

  procedure ParseLine(Line: AnsiString);
  begin
    Line := Trim(Line);
    if SameText('Content-Length: ', Copy(Line, 1, 16)) then
      ContentSize:=StrToInt(Copy(Line, 17, MaxInt));
  end;

var
  Buf: array[1..1023] of Byte;
  s: AnsiString;
  BytesRead: Integer;
  Line: AnsiString;
  PosCrLf: SizeInt;
begin
  Line := '';
  ContentSize:=0;
  s := '';
  repeat
  PosCrLf := Pos(CRLF, s);
  if PosCrLf > 0 then
    begin
    Line := Copy(s, 1, PosCrLf-1);
    s := Copy(s, PosCrLf+2, MaxInt);
    ParseLine(Line);
    end
  else
    begin
    if FInputSocket.Closed then
      Exit([]);
    BytesRead := FInputSocket.Read(Buf, SizeOf(Buf));
    if BytesRead=0 then
      Exit([]);
    s := s + TEncoding.ASCII.GetAnsiString(@Buf[1], 0, BytesRead);
    end;
  until (Line='') and (ContentSize>0);
  Result := TEncoding.ASCII.GetAnsiBytes(s);
  BytesRead:=Length(Result);
  if BytesRead < ContentSize then
    begin
    SetLength(Result, ContentSize);
    FInputSocket.ReadBuffer(Result, BytesRead, ContentSize-BytesRead);
    end;
end;

function TLanguageServer.MakeServerCall(AMethod: string; ARequestId: string; AParams: TJSONData; AForced: Boolean): Integer;
var
  Request: TJSONObject;
begin
  if FInitializationStatus = isNone then
    raise Exception.Create('The Language Server is not active and cannot be called.');
  Request := CreateRPCRequest(AMethod, (ARequestId=''), ARequestId);
  try
    Result := Request.Get('id', -1);
    if Assigned(AParams) then
      Request.Add('params', AParams);

    // When the connection with the language-server is not active yet, queue
    // all calls. Except for the calls related to the initialization of the
    // connection itself. (AForced=true)
    if not AForced and (FInitializationStatus in [isProcessing, isReconnect]) then
      begin
      FQueue.Push(Request);
      Request:=nil;
      end
    else
      DoRequest(Request);
 finally
   Request.Free;
 end;
end;

procedure TLanguageServer.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
var
  JS: TJSONData;
  RequestData: TLangServerRequestData;
  Request: TRequest;
begin
  inherited HandleEvent(AnEvent, Finished);
  if (AnEvent.Name = 'LangServerRequest') then
    begin
    RequestData := AnEvent.Data as TLangServerRequestData;
    if DoesSupportLanguageId(RequestData.LanguageId) then
      begin
      if EnforceConnected then
        begin
        JS := FSerializer.ObjectToJSON(RequestData.Params);
        MakeServerCall(RequestData.Method, RequestData.RequestId, JS);
        end;
      end;
    end
  else if (AnEvent.Name = 'OpenFolderChanged') then
    begin
    // To open a new folder, a new session has to be started.
    if FInitializationStatus in [isSucceeded, isProcessing] then
      InitiateDisconnect;
    end
  else if (AnEvent.Name = 'LangServerResponse') then
    begin
    if (AnEvent as TLanguageServerResponseEvent).RequestId='shutd' then
      begin
      FinalizeDisconnect;
      end
    else if FOutstandingServerCallsForRequests.TryGetValue((AnEvent as TLanguageServerResponseEvent).RequestId, Request) then
      begin
      if Request.Name='Completion' then
        HandleCompletionLangServerResponse(TLanguageServerResponseEvent(AnEvent), Request)
      else if Request.Name='DeclarationInfo' then
        HandleDeclarationInfoLangServerResponse(TLanguageServerResponseEvent(AnEvent), Request)
      else if Request.Name='DefinitionInfo' then
        HandleDefinitionInfoLangServerResponse(TLanguageServerResponseEvent(AnEvent), Request);
      end;
    end
  else if (AnEvent.Name = 'LangServerNotification') then
    begin
    HandleServerNotification(AnEvent as TLanguageServerNotificationEvent)
    end;
end;

procedure TLanguageServer.HandleRequest(ARequest: TRequest; out Finished: Boolean);
var
  HoverInfoParams, ReqHoverInfoParams: THoverInfoParams;
  CompletionData: TCompletionData;
  CompletionParams: TCompletionParams;
  OpnFile: TOpenFile;
begin
  inherited HandleRequest(ARequest, Finished);
  if ARequest.Name='RequestTooltipInfo' then
    begin
    ReqHoverInfoParams := ARequest.Data as THoverInfoParams;
    HoverInfoParams := THoverInfoParams.Create;
    HoverInfoParams.FileId:=ReqHoverInfoParams.FileId;
    HoverInfoParams.Position:=ReqHoverInfoParams.Position;
    TGlobalActionFactory.Instance.RunAction('HoverInfo', HoverInfoParams, @OnHoverInfoCompleted);
    end;
end;

function TLanguageServer.EnforceConnected: Boolean;
begin
  case FInitializationStatus of
    isNone :
      begin
      Init;
      Result := True;
      end;
    isProcessing, isSucceeded :
      begin
      Result := True;
      end;
    isShuttingDown, isReconnect :
      begin
      // When a shutdown is in progress, set status to isReconnect so that
      // after the shutdown the connection is immediately restarted
      FInitializationStatus:=isReconnect;
      Result := True;
      end;
    isFailed :
      begin
      Result := False;
      end;
  end;
end;

procedure TLanguageServer.InitiateDisconnect;
begin
  MakeServerCall('shutdown', 'shutd', nil);
  FInitializationStatus:=isShuttingDown;
end;

procedure TLanguageServer.Init;
begin
  FInputSocket := TInetSocket.Create('localhost', 4002, TSocketHandler.Create);
  FOutputSocket := FInputSocket;
  FInitializationStatus := isProcessing;
  FQueue := TObjectQueue.Create;

  FFolder := ExpandFileNameUTF8(TGlobalOpenFolder.Instance.OpenFolderName);

  TThread.ExecuteInThread(@InitInThread, @InitDone);
end;

function TLanguageServer.DoesSupportLanguageId(ALanguageId: string): Boolean;
begin
  Result := False;
end;

class function TLanguageServer.FilenameToUri(const AFilename: string): TDocumentUri;
begin
  Result := 'file://' + AFilename;
end;

class function TLanguageServer.UriToFilename(const AnUri: TDocumentUri): string;
begin
  if pos('file://', AnUri) = 1 then
    Result := copy(AnUri, 8, length(AnUri))
  else
    Result := AnUri;
end;

procedure TLanguageServer.InitInThread;
var
  InitializeParams: TInitializeParams;
  Serializer: TJSONRttiStreamClass;
  Parms, ResponseResult: TJSONData;
  ResponseBytes: TBytes;
  ss: UnicodeString;
  resp: TInitializeResult;
  Id: string;
begin
  if FInputSocket is TInetSocket then
    TInetSocket(FInputSocket).Connect;

  Serializer := TJSONRttiStreamClass.Create;

  InitializeParams := TInitializeParams.Create;
  InitializeParams.RootUri := TLanguageServer.FilenameToUri(FFolder);
  MakeServerCallAndReleaseParams('initialize', 'init1', InitializeParams, true);
  ResponseBytes := AwaitMessage;
  ParseRPCResponse(TEncoding.UTF8.GetString(ResponseBytes), Id, ResponseResult);

  FInitializeResult := TInitializeResult.Create;
  FSerializer.JSONToObject(ResponseResult, FInitializeResult);

  MakeServerCallAndReleaseParams('initialized', 'init2', TInitializedParams.Create, true);
  ResponseBytes := AwaitMessage;

  FInitSucceeded := True;
end;

procedure TLanguageServer.ListenInThread(ReportStatus: TThreadReportStatus);
var
  Msg: TBytes;
begin
  repeat
  Msg := AwaitMessage;
  if Msg <> nil then
    ReportStatus(TEncoding.ASCII.GetString(Msg));
  until Msg = nil;
end;

procedure TLanguageServer.DoRequest(ARequest: TJSONObject);
var
  RequestStr: AnsiString;
  Message, s: string;
  Buf: AnsiString;
  ResponseBytes: TBytes;
begin
  RequestStr:=ARequest.AsJSON;
  Message:='Content-Length: '+IntToStr(Length(RequestStr))+CRLF+CRLF+RequestStr;
  FOutputSocket.WriteBuffer(Message[1], Length(Message));
end;

procedure TLanguageServer.FinalizeDisconnect;
begin
  MakeServerCall('exit', '', nil, True);
  FInputSocket.Close;
  if FOutputSocket<>FInputSocket then
    FOutputSocket.Close;
end;

procedure TLanguageServer.NotifyDidOpenTextDocument(FileUniqueId: string);
var
  OpnFile: TOpenFile;
  Params: TDidOpenTextDocumentParams;
begin
  OpnFile:=TGlobalFileList.Instance.FindByUniqueId(FileUniqueId);
  MakeServerCallAndReleaseParams('textDocument/didOpen', '', TDidOpenTextDocumentParams.Create(
    TTextDocumentItem.Create(
      FilenameToUri(OpnFile.Filename),
      OpnFile.LanguageId,
      OpnFile.Version,
      OpnFile.SynEdit.Text)));
end;

procedure TLanguageServer.NotifyChangeTextDocument(FileUniqueId: string; Change: TLinesEditedData);
var
  OpnFile: TOpenFile;
  Params: TDidChangeTextDocumentParams;
  ContentChange: TTextDocumentContentChangeEventIncremental;
  i: Integer;
begin
  OpnFile:=TGlobalFileList.Instance.FindByUniqueId(FileUniqueId);
  if Assigned(OpnFile) then
    begin
    // Todo: Check wheter to use full or incremental.
    Params := TDidChangeTextDocumentParams.Create(
      FilenameToUri(OpnFile.Filename),
      OpnFile.Version);

    OpnFile.Version := OpnFile.Version + 1;
    ContentChange := TTextDocumentContentChangeEventIncremental.Create;
    if Change.LineBrkCnt < 0 then
      begin
      // Make sure that these combinations of LineBrkCnt and actual content
      // do not happen
      Assert(Change.Count=0);
      Assert(Change.Text='');

      // LineBrkCnt=-1: Combine two lines. LineBrkCnt < -1: remove lines
      ContentChange.Range.Start.Line := Change.LinePos-1;
      ContentChange.Range.Start.Character := Change.BytePos-1;
      ContentChange.Range.&End.Line := Change.LinePos-1-Change.LineBrkCnt;
      ContentChange.Range.&End.Character := 0;
      ContentChange.Text := '';
      end
    else if Change.LineBrkCnt > 0 then
      begin
      // Make sure that these combinations of LineBrkCnt and actual content
      // do not happen
      Assert(Change.Count=0);
      Assert(Change.Text='');

      // LineBrkCnt=1: Split line. LineBrkCnt>1: insert lines
      ContentChange.Range.Start.Line := Change.LinePos-1;
      ContentChange.Range.Start.Character := Change.BytePos-1;
      ContentChange.Range.&End.Line := Change.LinePos-1;
      ContentChange.Range.&End.Character := Change.BytePos-1;
      for i := 0 to Change.LineBrkCnt -1 do
        ContentChange.Text := ContentChange.Text + LineEnding;
      end
    else if Change.Count < 0 then
      begin
      // Delete:
      ContentChange.Range.Start.Line := Change.LinePos-1;
      ContentChange.Range.Start.Character := Change.BytePos-1;
      ContentChange.Range.&End.Line := Change.LinePos-1;
      ContentChange.Range.&End.Character := Change.BytePos-1-change.Count;
      ContentChange.Text := Change.Text;
      end
    else
      begin
      // Insert:
      ContentChange.Range.Start.Line := Change.LinePos-1;
      ContentChange.Range.Start.Character := Change.BytePos-1;
      ContentChange.Range.&End.Line := Change.LinePos-1;
      ContentChange.Range.&End.Character := Change.BytePos-1;
      ContentChange.Text := Change.Text;
      end;

    Params.ContentChanges.Add(ContentChange);

    MakeServerCallAndReleaseParams('textDocument/didChange', '', Params);
    end;
end;

procedure TLanguageServer.NotifyDidCloseTextDocument(FileUniqueId: string);
var
  OpnFile: TOpenFile;
begin
  OpnFile:=TGlobalFileList.Instance.FindByUniqueId(FileUniqueId);
  if not OpnFile.IsShallow then
    begin
    MakeServerCallAndReleaseParams('textDocument/didClose', '', TDidCloseTextDocumentParams.Create(
      TTextDocumentIdentifier.Create(FilenameToUri(OpnFile.Filename))));
    end;
end;

procedure TLanguageServer.HandleServerNotification(ANotification: TLanguageServerNotificationEvent);

  function RangeToPoint(Element: TJSONObject): TPoint;
  var
    Line, Character: Integer;
  begin
   Line := Element.Get('line', -1);
   Character := Element.Get('character', -1);
   Result := Point(Character, Line);
  end;

var
  Dta: TFileAnnotations;
  an: TAnnotation;
  Diagnostics: TJSONArray;
  Diagnostic, Range, Start, EndElement, Resp: TJSONObject;
  StartPoint, EndPoint: TPoint;
  i: Integer;
  FileName, Message: TJSONStringType;
  CurrFile: TOpenFile;
begin
  if ANotification.Method='textDocument/publishDiagnostics' then
    begin
    Resp := (ANotification.Res as TJSONObject);

    Dta := TFileAnnotations.Create;
    FileName := UriToFilename(Resp.Get('uri', ''));
    CurrFile := TGlobalFileList.Instance.FindByFilename(FileName);
    if not Assigned(CurrFile) then
      Exit;
    dta.UniqueFileId:=CurrFile.UniqueId;

    Diagnostics := Resp.Get('diagnostics', TJSONArray(nil));
    for i := 0 to Diagnostics.Count -1 do
      begin
      Diagnostic := Diagnostics.Items[i] as TJSONObject;
      Range := Diagnostic.Get('range', TJSONObject(nil));
      Message := Diagnostic.Get('message', '');
      Start := Range.Get('start', TJSONObject(nil));
      StartPoint := RangeToPoint(Start);
      EndElement := Range.Get('end', TJSONObject(nil));
      EndPoint := RangeToPoint(EndElement);

      an:=TAnnotation.Create;
      an.PosFrom:=StartPoint.X+1;
      an.PosTill:=EndPoint.X+1;
      an.LineNr:=StartPoint.Y;
      an.Text:=Message;
      an.Level:=alInfo;
      dta.AnnotationList.Add(an);
      end;
    TGlobalEventDispatcher.Instance.SendEvent('NewAnnotations', 'LangServer', dta);
    end;
end;

procedure TLanguageServer.OpentextDocumentWhenNecessary(FileUniqueId: string);
var
  PluginData: TLanguageServerData;
begin
  if Assigned(InitializeResult.Capabilities.TextDocumentSync) and (InitializeResult.Capabilities.TextDocumentSync.OpenClose) then
    begin
    PluginData := FPluginData.GetData(FileUniqueId);
    NotifyDidOpenTextDocument(FileUniqueId);
    PluginData.IsMarkedAsOpen:=True;
    end;
end;

procedure TLanguageServer.ClosetextDocumentWhenNecessary(FileUniqueId: string);
var
  PluginData: TLanguageServerData;
  i: Integer;
begin
  if FPluginData.TryGetData(FileUniqueId, PluginData) and PluginData.IsMarkedAsOpen then
    begin
    i := FFilesToOpenAfterConnect.IndexOf(FileUniqueId);
    if i > 0 then
      begin
      // File was enlisted to be opened, but it actually enver was. So remove
      // from the list, do not send a close.
      FFilesToOpenAfterConnect.Delete(i);
      end
    else
      begin
      NotifyDidCloseTextDocument(FileUniqueId);
      PluginData.IsMarkedAsOpen:=False;
      end;
    end;
end;

procedure TLanguageServer.HandleLinesEdited(AnEvent: TEvent; out Finished: Boolean);
var
  LinesEdited: TLinesEditedData;
  PluginData: TLanguageServerData;
begin
  if FPluginData.TryGetData(AnEvent.Sender, PluginData) and PluginData.IsMarkedAsOpen then
    begin
    LinesEdited := AnEvent.Data as TLinesEditedData;
    NotifyChangeTextDocument(AnEvent.Sender, LinesEdited);
    end;
end;

constructor TLanguageServer.Create;
begin
  FPluginData := TLanguageServerDataDictionary.Create;
  FOutstandingServerCallsForRequests := TOutstandingRequestDataDictionary.Create;
  FSerializer := TJSONRttiStreamClass.Create;
  FSerializer.DescriptionStore.Describer.DefaultExportNameStyle := tcsensLowerCaseFirstChar;

  FSerializer.Describer.Flags:=[tcsdfCreateClassInstances];
  FSerializer.Describer.DefaultExportNameStyle:=tcsensLowerCaseFirstChar;
  FSerializer.Describer.DefaultImportNameStyle:=tcsinsLowerCaseFirstChar;

  TGlobalEventDispatcher.Instance.AddEventHandler('LinesEdited', @HandleLinesEdited);
  TGlobalEventDispatcher.Instance.AddEventHandler('LanguageIdChanged', @HandleLanguageIdChanged);

  TGlobalRequestDispatcher.Instance.AddRequestHandler('Completion', @HandleCompletionRequest);
  TGlobalRequestDispatcher.Instance.AddRequestHandler('DeclarationInfo', @HandleDeclarationInfoRequest);
  TGlobalRequestDispatcher.Instance.AddRequestHandler('DefinitionInfo', @HandleDefinitionInfoRequest);

  FFilesToOpenAfterConnect := TStringList.Create;
  FFilesToOpenAfterConnect.Sorted:=True;
  FFilesToOpenAfterConnect.Duplicates:=dupIgnore;
end;

destructor TLanguageServer.Destroy;
begin
  TGlobalEventDispatcher.Instance.RemoveEventHandler('LinesEdited', @HandleLinesEdited);
  TGlobalEventDispatcher.Instance.RemoveEventHandler('LanguageIdChanged', @HandleLanguageIdChanged);

  TGlobalRequestDispatcher.Instance.RemoveRequestHandler('Completion', @HandleCompletionRequest);
  TGlobalRequestDispatcher.Instance.RemoveRequestHandler('DeclarationInfo', @HandleDeclarationInfoRequest);
  TGlobalRequestDispatcher.Instance.RemoveRequestHandler('DefinitionInfo', @HandleDefinitionInfoRequest);
  FInputSocket.Free;
  if FOutputSocket<>FInputSocket then
    FOutputSocket.Free;

  FSerializer.Free;
  FPluginData.Free;
  FQueue.Free;
  FOutstandingServerCallsForRequests.Free;
  FFilesToOpenAfterConnect.Free;
  inherited Destroy;
end;

{ THoverInfoAction }

procedure THoverInfoAction.Execute;
var
  HoverInfoParams: THoverInfoParams;
  HoverParams: THoverParams;
  OpnFile: TOpenFile;
begin
  HoverInfoParams := Params as THoverInfoParams;
  FPosition := HoverInfoParams.Position;

  OpnFile:=TGlobalFileList.Instance.FindByUniqueId(HoverInfoParams.FileId);
  if Assigned(OpnFile) then
    begin
    HoverParams := THoverParams.Create;
    HoverParams.TextDocument.Uri:= TLanguageServer.FilenameToUri(OpnFile.Filename);

    HoverParams.Position.Line:=Max(HoverInfoParams.Position.Y,1)-1;
    HoverParams.Position.Character:=Max(HoverInfoParams.Position.X,1)-1;

    FRequestId := 'HIA'+IntToStr(Random(999999));

    TGlobalEventDispatcher.Instance.SendEvent('LangServerRequest', 'HoverInfoAction', TLangServerRequestData.Create('textDocument/hover', OpnFile.LanguageId, FRequestId, HoverParams));
    end;
end;

procedure THoverInfoAction.HandleEvent(AnEvent: TEvent; out EventFinished: Boolean);

  function ItemToStr(AnItem: TJSONData): string;
  begin
    if AnItem.JSONType=jtString then
      FHint := FHint + AnItem.AsString
    else
      FHint := FHint + AnItem.AsJSON;
  end;

var
  Response: TLanguageServerResponseEvent;
  Contents, Item: TJSONData;
  i: Integer;
begin
  inherited HandleEvent(AnEvent, EventFinished);
  if (AnEvent.Name='LangServerResponse') then
    begin
    Response := AnEvent as TLanguageServerResponseEvent;
    if Response.RequestId = FRequestId then
      begin
      Contents := (Response.Res as TJSONObject).Find('contents');
      if Contents.JSONType=jtArray then
        begin
        for i := 0 to Contents.Count -1 do
          begin
          Item := Contents.Items[i];
          if FHint<>'' then
            FHint:=FHint+CRLF;
          FHint:=FHint+ItemToStr(item);
          end;
        end
      else
        begin
        if FHint<>'' then
          FHint:=FHint+CRLF;
        FHint := FHint + ItemToStr(Contents);
        end;
      SetSuccess;
      EventFinished:=True;
      end;
    end;
end;

{ TLanguageServerMessageEvent }

constructor TLanguageServerMessageEvent.Create(AName, ASender: string; ARes: TJSONData; AIsError: Boolean);
begin
  inherited Create(AName, ASender);
  FIsError:=AIsError;
  FRes := ARes;
end;

destructor TLanguageServerMessageEvent.Destroy;
begin
  FRes.Free;
  inherited Destroy;
end;

{ TLanguageServerResponseEvent }

constructor TLanguageServerResponseEvent.Create(AName, ASender: string; ARequestId: string; ARes: TJSONData; AIsError: Boolean);
begin
  inherited Create(AName, ASender, ARes, AIsError);
  FRequestId := ARequestId;
end;

{ TLanguageServerNotificationEvent }

constructor TLanguageServerNotificationEvent.Create(AName, ASender: string; AMethod: string; ARes: TJSONData; AIsError: Boolean);
begin
  inherited Create(AName, ASender, ARes, AIsError);
  FMethod := AMethod;
end;

{ TLangServerRequestData }

constructor TLangServerRequestData.Create(AMethod: string; ALanguageId: string; ARequestId: string; AParams: TObject);
begin
  FMethod:=AMethod;
  FLanguageId:=ALanguageId;
  FParams:=AParams;
  FRequestId:=ARequestId;
end;

initialization
  TGlobalActionFactory.Instance.RegisterAction('HoverInfo', 'Retrieve info', 'Language server', THoverInfoAction);
end.

