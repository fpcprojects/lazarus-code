unit Tabs;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  ComCtrls,
  Singleton;

type

  { TTabFactory }

  TTabFactory = class
  private
    FPageControl: TPageControl;
  public
    procedure BindToPageControl(APageControl: TPageControl);
    function GetTab: TTabSheet;
  end;
  TGlobalTabFactory = specialize TSingleton<TTabFactory>;

implementation

{ TTabFactory }

procedure TTabFactory.BindToPageControl(APageControl: TPageControl);
begin
  FPageControl := APageControl;
end;

function TTabFactory.GetTab: TTabSheet;
begin
  if Assigned(FPageControl) then
    Result := FPageControl.AddTabSheet
  else
    raise Exception.Create('No pagecontrol available. Failed to add tab');
end;

end.

