unit Plugins;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  Singleton,
  Generics.Collections,
  Events,
  Requests;

type

    { TPlugin }

    TPlugin = class
    private
    public
      procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean); virtual;
      procedure HandleRequest(ARequest: TRequest; out Finished: Boolean); virtual;
      constructor Create; virtual;
      destructor Destroy; override;
    end;
    TCustPluginList = specialize TObjectList<TPlugin>;

    { TOpenFileList }

    { TPluginList }

    TPluginList = class(TCustPluginList)
    protected
    public
      procedure HandleEvent(AnEvent: TEvent; out Finished: Boolean);
    end;
    TGlobalPluginList = specialize TSingleton<TPluginList>;

implementation

{ TPlugin }

procedure TPlugin.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
begin
  Finished:=False;
end;

procedure TPlugin.HandleRequest(ARequest: TRequest; out Finished: Boolean);
begin
  Finished:=False;
end;

constructor TPlugin.Create;
begin

end;

destructor TPlugin.Destroy;
begin
  inherited Destroy;
end;


{ TPluginList }

procedure TPluginList.HandleEvent(AnEvent: TEvent; out Finished: Boolean);
var
  i: Integer;
begin
  for i := 0 to Count -1 do
    begin
    Items[i].HandleEvent(AnEvent, Finished);
    if Finished then
      Break;
    end;
end;

end.

