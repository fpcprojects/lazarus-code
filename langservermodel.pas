unit LangServerModel;

{$mode ObjFPC}{$H+}{$M+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  csModel,
  csGenericCollectionsDescriber,
  Events;

type

  TProgressToken = Integer;
  TUri = string;
  TDocumentUri = string;

  { TWorkDoneProgressParams }

  TWorkDoneProgressParams = class
  private
    FWorkDoneToken: TProgressToken;
  published
    property WorkDoneToken: TProgressToken read FWorkDoneToken write FWorkDoneToken;
  end;


  { TWorkspaceFolder }

  TWorkspaceFolder = class
  private
    FName: string;
    Furi: TURI;
  published
    property Uri: TURI read Furi write Furi;
    property Name: string read FName write FName;
  end;
  TWorkspaceFolderArray = array of TWorkspaceFolder;


  { TWorkSpaceCapabilities }

  TWorkSpaceCapabilities = class
  private
    FWorkspaceFolders: boolean;
  published
    property WorkspaceFolders: boolean read FWorkspaceFolders write FWorkspaceFolders;
  end;

  { TClientCapabilities }

  TClientCapabilities = class
  private
    FWorkSpace: TWorkSpaceCapabilities;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property WorkSpace: TWorkSpaceCapabilities read FWorkSpace write FWorkSpace;
  end;

  { TServerInfo }

  TServerInfo = class
  private
    FName: string;
    FVersion: string;
  published
    property Name: string read FName write FName;
    property Version: string read FVersion write FVersion;
  end;

  TTextDocumentSyncKind = (
    None = 0,
    Full = 1,
    Incremental = 2);

  { TTextDocumentSyncOptions }

  TTextDocumentSyncOptions = class
  private
    FChange: TTextDocumentSyncKind;
    FOpenClose: Boolean;
  published
    property OpenClose: Boolean read FOpenClose write FOpenClose;
    property Change: TTextDocumentSyncKind read FChange write FChange;
  end;

  { TServerCapabilities }

  TServerCapabilities = class
  private
    FDeclarationProvider: Boolean;
    FDefinitionProvider: Boolean;
    FTextDocumentSync: TTextDocumentSyncOptions;
  published
    property TextDocumentSync: TTextDocumentSyncOptions read FTextDocumentSync write FTextDocumentSync;
    property DeclarationProvider: Boolean read FDeclarationProvider write FDeclarationProvider;
    property DefinitionProvider: Boolean read FDefinitionProvider write FDefinitionProvider;
  end;

  { TInitializeResult }

  TInitializeResult = class
  private
    FCapabilities: TServerCapabilities;
    FServerInfo: TServerInfo;
  public
    destructor Destroy; override;
  published
    property Capabilities: TServerCapabilities read FCapabilities write FCapabilities;
    property ServerInfo: TServerInfo read FServerInfo write FServerInfo;
  end;

  { TInitializeParams }

  TInitializeParams = class
  private
    FCapabilities: TClientCapabilities;
    FProcessId: integer;
    FRootUri: string;
    FWorkspaceFolders: TWorkspaceFolderArray;
  public
    constructor Create;
  published
    property ProcessId: integer read FProcessId write FProcessId;
    property Capabilities: TClientCapabilities read FCapabilities write FCapabilities;
    property RootUri: string read FRootUri write FRootUri;
    property WorkspaceFolders: TWorkspaceFolderArray read FWorkspaceFolders write FWorkspaceFolders;
  end;

  TInitializedParams = class
  end;

  { TPosition }

  TPosition = class
  private
    FCharacter: LongWord;
    FLine: LongWord;
  published
    property Line: LongWord read FLine write FLine;
    property Character: LongWord read FCharacter write FCharacter;
  end;

  { TTextDocumentIdentifier }

  TTextDocumentIdentifier = class
  private
    FUri: TDocumentUri;
  public
    constructor Create(AnUri: TDocumentUri = '');
  published
    property Uri: TDocumentUri read FUri write FUri;
  end;

  { TTextDocumentPositionParams }

  TTextDocumentPositionParams = class
  private
    FPosition: TPosition;
    FTextDocument: TTextDocumentIdentifier;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property TextDocument: TTextDocumentIdentifier read FTextDocument write FTextDocument;
    property Position: TPosition read FPosition write FPosition;
  end;

  THoverParams = class(TTextDocumentPositionParams)

  end;

  TDefinitionParams = class(TTextDocumentPositionParams)

  end;

  TDeclarationParams = class(TTextDocumentPositionParams)

  end;

  TLangServerEvent = class(TEvent)

  end;

  { TTextDocumentItem }

  TTextDocumentItem = class
  private
    FLanguageId: string;
    FText: string;
    FUri: TDocumentUri;
    FVersion: Integer;
  public
    constructor Create(AnUri: TDocumentUri; ALanguageId: string; AVersion: Integer; AText: string);
  published
    property Uri: TDocumentUri read FUri write FUri;
    property LanguageId: string read FLanguageId write FLanguageId;
    property Version: Integer read FVersion write FVersion;
    property Text: string read FText write FText;
  end;

  { TDidOpenTextDocumentParams }

  TDidOpenTextDocumentParams = class
  private
    FTextDocument: TTextDocumentItem;
  public
    constructor Create(ATextDocument: TTextDocumentItem);
    destructor Destroy; override;
  published
    property TextDocument: TTextDocumentItem read FTextDocument write FTextDocument;
  end;

  { TDidCloseTextDocumentParams }

  TDidCloseTextDocumentParams = class
  private
    FTextDocument: TTextDocumentIdentifier;
  public
    constructor Create(ATextDocument: TTextDocumentIdentifier);
    destructor Destroy; override;
  published
    property TextDocument: TTextDocumentIdentifier read FTextDocument write FTextDocument;
  end;

  TTextDocumentContentChangeEvent = class
  published

  end;

  { TextDocumentContentChangeEventFull }

  TTextDocumentContentChangeEventFull = class(TTextDocumentContentChangeEvent)
  private
    FText: string;
  published
    property Text: string read FText write FText;
  end;

  { TRange }

  TRange = class
  private
    FEnd: TPosition;
    FStart: TPosition;
  public
    constructor Create;
  published
    property Start: TPosition read FStart;
    property &End: TPosition read FEnd;
  end;

  { TTextDocumentContentChangeEventIncremental }

  TTextDocumentContentChangeEventIncremental = class(TTextDocumentContentChangeEvent)
  private
    FRange: TRange;
    FText: string;
  public
    constructor Create;
  published
    property Range: TRange read FRange;
    property Text: string read FText write FText;
  end;

  TTextDocumentContentChangeEventIncrementalObjectList = specialize TObjectList<TTextDocumentContentChangeEventIncremental>;

  { TVersionedTextDocumentIdentifier }

  TVersionedTextDocumentIdentifier = class(TTextDocumentIdentifier)
  private
    FVersion: Integer;
  public
    constructor Create(AnUri: TUri; AVersion: integer);
  published
    property Version: Integer read FVersion write FVersion;
  end;

  { TDidChangeTextDocumentParams }

  TDidChangeTextDocumentParams = class
  private
    FContentChanges: TTextDocumentContentChangeEventIncrementalObjectList;
    FTextDocument: TVersionedTextDocumentIdentifier;
  public
    constructor Create(ADocumentUri: TUri; AVersion: integer);
  published
    property TextDocument: TVersionedTextDocumentIdentifier read FTextDocument;
    property ContentChanges: TTextDocumentContentChangeEventIncrementalObjectList read FContentChanges;
  end;

  TCompletionTriggerKind = (Invoked = 1, TriggerCharacter = 2, TriggerForIncompleteCompletions = 3);

  { TCompletionContext }

  TCompletionContext = class
  private
    FTriggerCharacter: string;
    FTriggerKind: TCompletionTriggerKind;
  public
    property TriggerKind: TCompletionTriggerKind read FTriggerKind write FTriggerKind;
  published
    property TriggerCharacter: string read FTriggerCharacter write FTriggerCharacter;
  end;

  { TCompletionParams }

  TCompletionParams = class(TTextDocumentPositionParams)
  private
    FContext: TCompletionContext;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property Context: TCompletionContext read FContext write FContext;
  end;

  { TLocation }

  TLocation = class
  private
    FRange: TRange;
    FUri: TDocumentUri;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property Uri: TDocumentUri read FUri write FUri;
    property Range: TRange read FRange;
  end;
  TLocationList = specialize TObjectList<TLocation>;


implementation

{ TClientCapabilities }

constructor TClientCapabilities.Create;
begin
  FWorkSpace := TWorkSpaceCapabilities.Create;
  //FWorkSpace.WorkspaceFolders:=True;
end;

destructor TClientCapabilities.Destroy;
begin
  FWorkSpace.Free;
  inherited Destroy;
end;

{ TInitializeResult }

destructor TInitializeResult.Destroy;
begin
  FServerInfo.Free;
  FCapabilities.Free;
  inherited Destroy;
end;

{ TInitializeParams }

constructor TInitializeParams.Create;
begin
  FCapabilities := TClientCapabilities.Create;
end;

{ TTextDocumentIdentifier }

constructor TTextDocumentIdentifier.Create(AnUri: TDocumentUri);
begin
  FUri:=AnUri;
end;

{ TTextDocumentPositionParams }

constructor TTextDocumentPositionParams.Create;
begin
  FTextDocument := TTextDocumentIdentifier.Create;
  FPosition := TPosition.Create;
end;

destructor TTextDocumentPositionParams.Destroy;
begin
  FPosition.Free;
  inherited Destroy;
end;

{ TTextDocumentItem }

constructor TTextDocumentItem.Create(AnUri: TDocumentUri; ALanguageId: string; AVersion: Integer; AText: string);
begin
  FUri:=AnUri;
  FLanguageId:=ALanguageId;
  FVersion:=AVersion;
  FText:=AText;
end;

{ TDidOpenTextDocumentParams }

constructor TDidOpenTextDocumentParams.Create(ATextDocument: TTextDocumentItem);
begin
  FTextDocument := ATextDocument;
end;

destructor TDidOpenTextDocumentParams.Destroy;
begin
  FTextDocument.Free;
  inherited Destroy;
end;

{ TDidCloseTextDocumentParams }

constructor TDidCloseTextDocumentParams.Create(ATextDocument: TTextDocumentIdentifier);
begin
  FTextDocument := ATextDocument;
end;

destructor TDidCloseTextDocumentParams.Destroy;
begin
  FTextDocument.Free;
  inherited Destroy;
end;

{ TRange }

constructor TRange.Create;
begin
  FStart := TPosition.Create;
  FEnd := TPosition.Create;
end;

{ TTextDocumentContentChangeEventIncremental }

constructor TTextDocumentContentChangeEventIncremental.Create;
begin
  FRange := TRange.Create;
end;

{ TVersionedTextDocumentIdentifier }

constructor TVersionedTextDocumentIdentifier.Create(AnUri: TUri; AVersion: integer);
begin
  FUri := AnUri;
  FVersion := AVersion;
end;

{ TDidChangeTextDocumentParams }

constructor TDidChangeTextDocumentParams.Create(ADocumentUri: TUri; AVersion: integer);
begin
  FTextDocument := TVersionedTextDocumentIdentifier.Create(ADocumentUri, AVersion);
  FContentChanges := TTextDocumentContentChangeEventIncrementalObjectList.Create;
end;

{ TCompletionParams }

constructor TCompletionParams.Create;
begin
  inherited Create;
  FContext := TCompletionContext.Create;
end;

destructor TCompletionParams.Destroy;
begin
  FContext.Free;
  inherited Destroy;
end;

{ TLocation }

constructor TLocation.Create;
begin
  FRange:=TRange.Create;
end;

destructor TLocation.Destroy;
begin
  FRange.Free;
  inherited Destroy;
end;

initialization
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(specialize TcsGenericCollectionTListObjectDescriber<TTextDocumentContentChangeEventIncremental>, 1);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(specialize TcsGenericCollectionTListObjectDescriber<TLocation>, 1);
end.

